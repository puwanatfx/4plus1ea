/*
22-MAR-2017 - start grails EA after failed with 4plus1 and grails1 ,grails2 EA
version 3.02 - ABS pip cond
version 3.05 - ABS pip cond +  bollinger band + Pip Pressure stop
version 3.05 - ABS pip cond +  bollinger band(Enable/Disable Input)
  EURUSD , cool + No Bollinger Band + Pip Pressure stop
  GUBUSD , cool + Bollinger Band + Pip Pressure stop
version 3.06 - success with win rate , + reduce stoploss from -25 to -20
             - Add ABS(Diff) < TickTotal
             - Add Diff != 0
version 3.07 - add support buy3/sell3 algo
             - add close when signalLast3SecHasZeroTick();

  
*/

//+------------------------------------------------------------------+..
//|                                                    grailsEA3.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Grails EA , Agility FX Trading."
#property link "http://www.grailsEA.com"
#property version "3.07"
#property strict

#include ".\Include\Account.mqh"
#include ".\Include\SingleOrder.mqh"
#include ".\Include\DenFxUtil.mqh"
#include ".\Include\DualOrderManager.mqh"
#include ".\Include\WebAPI.mqh"
#include ".\Include\VPAMonitor.mqh"






WebAPI api;
VPAMonitor vpamon;
SingleOrder order;
int http_status = 0;
input int magic_number=30703;
input int profit_pip = 30;
input int stoploss_pip = -20;
input int ABS_pip_per_sec = 12;
input bool bollinger_enable = false;
bool trailingActivated = false;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {

   // Show Logo


  ChartSetInteger(0, CHART_COLOR_CHART_UP, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CHART_DOWN, 0, 255);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, 0, 255);
  ChartSetInteger(0, CHART_SHOW_VOLUMES, 0, 1);
  ChartSetInteger(0, CHART_MODE, 0, 1);
  ChartSetInteger(0, CHART_SCALE, 3);
  ChartSetSymbolPeriod(0,NULL, PERIOD_M1);
  
  EventSetTimer(1);


  if (AccountCompany() == "Trading Point Of Financial Instruments Ltd") {
      // add 13 pip
    SingleOrder::s_high_spread += 13;
    
  } else if (AccountCompany() == "Tickmill Ltd") {
      // Do not thing
  
  } else {
    MessageBox("Unknown Broker name " + AccountCompany(), "Error",MB_ICONINFORMATION);
    ExpertRemove();
  }
  SingleOrder::setMagicNo(magic_number);
  
  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
  //--- destroy timer
  EventKillTimer();
 // order.ClosePending();

}


//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+


void OnTick() {
   
   if (IsTesting()){
   
      if (order.isNoneOrder()) {
         order.BuyNow("TESTMODE");
         //Print("ActualProfit",order.getActualProfit());
         //order.CloseNow();
         //ExpertRemove();
         trailingActivated = false;
         int trailingstep = 5;
         order.setRtStopLossPip(profit_pip-trailingstep*2);
         order.setRtTakeProfitPip(profit_pip);
         order.setRtTrailingStepPip(trailingstep);
      }
   
   
   }

   vpamon.collectOnTick();
   vpamon.CommentInfo(magic_number);
   
   
   if(order.isOpenedOrder()){
      int pip = order.profitPip() ;
      //Print("PIP =",pip);
      
      if(order.isProfitOverRtTakeProfit()){
         order.trailingRtStopLoss();
         trailingActivated = true;
         Print("trailing stop loss is activated , pip = ",pip);
      }
      if( trailingActivated && order.isProfitLowerRtStopLoss()){
        order.CloseNow();
        return;
      }
      
      if(order.profitPip() < stoploss_pip){
         Print("Close order due to profit=",pip," < stoploss_pip=",stoploss_pip);
         order.CloseNow();
         return;
      }

   }
   

}

bool IsMarketOpened(){
   return MarketInfo(Symbol(),MODE_TRADEALLOWED)== 1;
}

bool BollingerBandSingal(){

   if (!bollinger_enable) return true;
  
   double upper_price = iBands(NULL,PERIOD_M1,4,1,0,PRICE_MEDIAN,MODE_UPPER,0);
   double lower_price = iBands(NULL,PERIOD_M1,4,1,0,PRICE_MEDIAN,MODE_LOWER,0);
   
   if (Bid > upper_price  || Ask  < lower_price ){
      return true;
   }
   return false;
}

bool CondTradeAllowed(){

   return (  
            IsMarketOpened() && !SingleOrder::HighSpread() 
            && vpamon.getAvgTick1M() > 2.50  
            && vpamon.getLastTickTotal() > vpamon.getAvgTick1M() 
            && vpamon.getLastABSPip() >=  ABS_pip_per_sec
            && order.isNoneOrder()
            && BollingerBandSingal()
            && vpamon.getLastDiff() != 0
            && MathAbs(vpamon.getLastDiff()) <= vpamon.getLastTickTotal()
           
         );
}


//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
int sec=0;



void OnTimer() {
   
   vpamon.processTickDataWhenTimer();
   vpamon.CommentInfo(magic_number);
   
   if(order.isBuyOrder() || order.isSellOrder()){
      if (vpamon.signalLast3SecHasZeroTick() && order.getActualProfit() > 0  ){
         Print("Close order due to signalLast3SecHasZeroTick()");
         order.CloseNow();
      }
      if ( !trailingActivated && order.getActualProfit() > 0 &&
         vpamon.getLastTickTotal()  > vpamon.getAvgTick1M() &&
         vpamon.getLastABSPip() > vpamon.getAvgTick1M() &&
         vpamon.getLastDiff() == 0 )
      {
         // adding check 2 side Pressure equal signal
         Print("Close order due to Tick Pressure Cond");
         order.CloseNow();
         
      }
   }


   if (!CondTradeAllowed())
      return;   

 
   
   string output;
   int res = api.get("http://localhost/EA/signal?magic="+IntegerToString(magic_number),output);
   
   
   if(res> 200 && res <= 299 ){
      http_status = res;
      Print( "http status return =",res ,",output=",output);
      if(res > 200){ 
      
         int count = (res-200)/10;
         int direction = (res-200)%10;  
         string comment  = StringFormat("S=%d.%d,V=%.2f,B=%d,F=%d,T=%d",count,direction,vpamon.getAvgTick1M(),vpamon.getLastABSPip(),vpamon.getLastDiff(),vpamon.getLastTickTotal());
         if (direction == 1) order.BuyNow(comment);
         if (direction == 2) order.SellNow(comment);
         trailingActivated = false;
         int trailingstep = 5;
         order.setRtStopLossPip(profit_pip-trailingstep*2);
         order.setRtTakeProfitPip(profit_pip);
         order.setRtTrailingStepPip(trailingstep);
         return;
      }
 
      
   }
  

   
   
   // Must set Pending order here
  
   
}
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester() {
  //---
  double ret = 0.0;
  //---

 
  return (ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long& lparam, const double& dparam,
                  const string& sparam) {
  //---
}
//+------------------------------------------------------------------+
