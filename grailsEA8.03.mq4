/*
22-MAR-2017 - start grails EA after failed with 4plus1 and grails1 ,grails2 EA
version 3.02 - ABS pip cond
version 3.05 - ABS pip cond +  bollinger band + Pip Pressure stop
version 3.05 - ABS pip cond +  bollinger band(Enable/Disable Input)
  EURUSD , cool + No Bollinger Band + Pip Pressure stop
  GUBUSD , cool + Bollinger Band + Pip Pressure stop
version 3.06 - success with win rate , + reduce stoploss from -25 to -20
             - Add ABS(Diff) < TickTotal
             - Add Diff != 0
version 3.07 - add support buy3/sell3 algo
             - add close when signalLast3SecHasZeroTick();
version 3.08 - Change ABS(Diff) > TickTotal
version 3.09 - adding Dual Order
             - add Auto Resize Lot
        3.09.1 - Fix bug volume always 0.01
        3.09.2 - Add Handle when T/P was reached
        3.09.3 - if (count > 3) return / no open order
version 3.10
   - grails add return 22X
   - input Enable_AUTO_resize_lot
=== BIG CHANGE
version 4.01 move to new stategy
version 4.02
   +not care server directly , care only Bid price
   +add close when profit > 0 , avg_tick < 2

2017-06-20 - version 4.03
    + implement direction condition and pip between 2 price signal
2017-07-04 - version 4.03.02
    + add vpa.lastDiff() as condition too
    + add auto lot
2017-07-13 - version 4.03.03
    + add ABS{vpamon.getLastDiff()) > 1

2017-07-19 - version 4.03.04
    + check sig 2 previous sig time , rather than 1 previous sig time
2017-07-204 - version 4.03.05
    + When order is open if sig , then close order
2017-07-31 - version 4.03.06
    + time between order = 8 hours

---------------------------------------------
Big change
---------------------------------------------
2017-08-21 - version 5.01.01
   + change new stategy
   + 1) using Hedge Order Manager
   + 2) using BollingerBandSingal

2017-08-30 - done
2017-08-30 - version 50102
  +apply hedge trailing
2017-09-02 - version 50103
   + apply callVol();
2017-09-05 - version 50103
  + add HedgeOrderManager::isTooDifferenceUpperLower() , not test yet !
2017-09-13 - version 50103
   + Improve cal lot
 ========= Start 5.02
2017-08-21 - version 5.02.01 // seems go
  + re comment and message
  + add low AvergePip , >= 1  and cond < 2.5


2017-10-16 - version 5.02.02
   + when reach max order , close when profit = -5%
2017-10-17 - version 5.02.03
   + swap direction
2017-10-18 - version 5.02.04
   + g_max_loss_pip


==== Version 6.01
2017-10-23 - verison  6.01.01
2017-10-25 - version 6.01.02
+ no open order when ( LastABS != 0 && LastDiff != 0)
2017-10-25 - version 6.0.1.03
+ add sleep 10 min ,if previous order get loss
2017-10-30 - version 6.0.1.04
+ order.profitPip() > ((-1)*stoploss_pip/3)
+ set g_sleep_sec as twice =  60*10*2


==== Version 6.02
2017-11-02 - version 6.02.01
Buy & Sell consider with diffPip too

2017-11-06 - version 6.02.02
StopLoss same as get profit
Sleep 5 min if get loss

2017-11-07 - version 6.02.03
add close order when trailingActivated= true and avgTick < g_avg_tick_close_order

2017-11-07 - version 6.02.04
add CalVol() and Weekly Timer condition
2017-11-07 - version 6.02.05
not all to sell when F == 0
x4/3 of volumn when buy
x1 of volumn when sell


2017-11-07 - version 6.02.06
Sleep 1 hour when loss

2017-11-14 - version 6.03.01
sleep as 5 min when loss
add change open order mode when consec_loss >=3

2017-11-14 - version 6.03.01 (same version )
fix cal Rt_stop_loss from profit_pip
2017-11-15 - version 6.03.02
close order ,when HighSpread , actualProfit > 0
2017-11-16 - version 6.03.03
 + input g_open_order_mode
 + improve comment bug

2017-11-16 - version 6.04.01
 + getProfitPipOfThisWeek
 2017-11-19 - version 6.05.01
 + NormalLot()
 + AutoModeSelect Based on last 7 orders


2017-12-03 - version 7.01.01
+ 3 mode
   - Buy Upper
   - Reverse
   - Sell Lower

2018-01-16 - version 8.01.01
+ change Alog by have 3 line of move average to validate reverse trend
+ will start coding soon
+ if Price too high as > 2 x length of Bolliger Band Then No Open  <-- Inportant !!

2018-01-16 - version 8.01.03
+ Swap Direction

2018-01-19 - version 8.01.04
+ BollingerBandSingal - make sure price not go too far

2018-01-19 - version 8.01.05
+ BollingerBandSingal  - cal too-High price from 80% of shift = 1
+ add iBands in Close Order Condition

2018-01-19 - version 8.01.06
+ Ordery Type (Buy/Sell) by Grails Direction

2018-01-23 - version 8.01.07
+ add openMode cal base on order history()

2018-01-24 - version 8.01.08
+ remove cond close with iBand

2018-01-25 - version 8.01.09
+ check only last 2 orders for getOpenMode)()
+ if F == R count , then random

2018-01-25 - version 8.01.10
+ close now - no RT trailing stop loss

2018-01-25 - version 8.01.11
+ Close now - with RT trailing stop loss
+ Close now - no care Tick on dynamic cond

2018-01-25 - version 8.01.12
+ CloseNow when opentime > 0.58 hour

==============

2018-01-25 - version 8.02.01
+ copy way from 5.03

2018-02-14 - version 8.03.01

2018-02-19 - version 8.03.03
 + consider direct + diff in same way
 + Remove Hedge Stategy

 2018-02-19 - version 8.03.04
 + BollingerBandSingal - make sure price not go too far
 + OrderSwap()

 2018-02-25 - version 8.03.05
 + Input Buffer Pip
 + DayEndGrap

 2018-02-27 - version 8.03.06
 + Ignore Open Order  | Diff | > TickTotal
 + Ignore Open Order - TickTotal+1 < AvgTick
 + CalVol() after 1 min

 2018-02-25 - version 8.03.07
 + OpenedOrderConde - isDayEndGap() , then Close


----------------

 2018-03-17 - version 8.03.8
 +Consider why low profit while  80304 05 06 is cool
 so remove too much condition out
 + Seems nothing to remove all conditio of 80304-05-06 looks good
 + OpenedOrderCond - isWeekEndGap(), then Close


2018-06-03 - version 8.03.9
+ swap direction

2018-06-12 - version 8.03.10
+ g_trade_vol = calVol(0) , if it's newweek
+ print Pressure when pressure cond is good to print too

*/


//+------------------------------------------------------------------+..
//|                                                    grailsEA3.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Grails EA , Agility FX Trading."
#property link "http://www.grailsEA.com"
#property version "8.0310"
#property strict

#include ".\Include\Account.mqh"
#include ".\Include\HedgeOrderManager.mqh"
#include ".\Include\DenFxUtil.mqh"
#include ".\Include\WebAPI.mqh"
#include ".\Include\VPAMonitor.mqh"
#include ".\Include\WeekTimer.mqh"
#include ".\Include\GrailsAutoLot.mqh"
#include ".\Include\MVAMonitor.mqh"




WebAPI api;
VPAMonitor vpamon;
WeekTimer weektime;
MVAMonitor mvamon;

SingleOrder order;

input int magic_number = 80310;
input int profit_pip = 100;
input int stoploss_pip = -100;
input bool bollinger_enable = true;
//input int trailingstep = 10;
input int max_pip_of_week = 999999999;
input double max_lot = 15;
input double lot_trade = 0.00;
input int buffer_pip = 2000;



bool g_lot_trade_read = false;


//BollingerBandSingal
double g_main_price;
double g_max_order_count ;
// Avg Tick
double g_avg_tick_close_order = 2;
double g_trade_vol = 0;
bool trailingActivated = false;
int g_sleep_sec = 0;
bool g_order_opened = false;



//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+


int OnInit() {

  // Show Logo


  ChartSetInteger(0, CHART_COLOR_CHART_UP, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CHART_DOWN, 0, 255);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, 0, 255);
  ChartSetInteger(0, CHART_SHOW_VOLUMES, 0, 1);
  ChartSetInteger(0, CHART_MODE, 0, 1);
  ChartSetInteger(0, CHART_SCALE, 0);
  ChartSetSymbolPeriod(0, NULL, PERIOD_M1);

  EventSetTimer(1);


  if (AccountCompany() == "Trading Point Of Financial Instruments Ltd") {
    // add 13 pip
    SingleOrder::s_high_spread += 13;

  } else if (AccountCompany() != "Tickmill Ltd") {
    // Do not thing
    MessageBox("Unknown Broker name " + AccountCompany(), "Error", MB_ICONINFORMATION);
    //ExpertRemove();

  }

  SingleOrder::setMagicNo(magic_number);

  if (Symbol() != "EURUSD") {
    Print("Error : Symbol is not EURUSD");
    ExpertRemove();
  }

  g_trade_vol = calVol();
  Print("g_trade_vol =", g_trade_vol);
  getProfitPipOfThisWeek();



  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
  //--- destroy timer
  EventKillTimer();
// order.ClosePending();

}

int getProfitPipOfThisWeek(bool fcomment = true) {

  double  profitPip = 0;
  datetime thisMonday = weektime.findThisMonday();
  double allProfit = 0;

  for (int i = OrdersHistoryTotal() - 1; i >= 0; i--) {

    if (OrderSelect(i, SELECT_BY_POS, MODE_HISTORY)) {

      if (OrderOpenTime() > thisMonday && OrderType() != 6 /*type balance*/ ) {


        double actualProfit = RoundDown2Digit(OrderProfit()) + RoundDown2Digit(OrderCommission()) + RoundDown2Digit(OrderSwap());
        allProfit = allProfit + actualProfit;
        continue;
      }

      break;
    }
  }
  profitPip = allProfit / g_trade_vol;
  if (fcomment) Print("getProfitPipOfThisWeek() = ", MathCeil(profitPip) , ",thisMonday is =", thisMonday);
  return (int) MathCeil(profitPip);
}



void PreformOrderWhenTesting() {
  if (IsTesting()) {



  }

}




void OnTick() {

  vpamon.collectOnTick();
  vpamon.CommentInfo(magic_number);

  PreformOrderWhenTesting();
  if (order.isOpenedOrder()) {
    int pip = order.profitPip() ;
    // set sleep = 1 hour
    g_sleep_sec = 60 * 5;
    //g_sleep_sec = pip < 0 ? 60 * 5 : 60 * 5;

    MonitorOpenedOrderDynamicCond();
    return ;

    if (order.isProfitOverRtTakeProfit()) {
      order.trailingRtStopLoss();
      trailingActivated = true;
      Print("trailing stop loss is activated , pip = ", pip, ",rt_stoploss = ", order.getRtStopLoss(), ",rt_profit=", order.getRtTakProfit());
    }

    MonitorOpenedOrderDynamicCond();

    if ( trailingActivated && order.isProfitLowerRtStopLoss()) {
      order.CloseNow();
      return;
    }

    if (order.profitPip() < stoploss_pip) {

      Print("Close order due to profit=", pip, " < stoploss_pip=", stoploss_pip);
      order.CloseNow();
      return;

    }



    return;
  } // if (order.isOpenedOrder())


}



bool BollingerBandSingal(double &main_price) {

  if (!bollinger_enable) return true;

  double upper_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_UPPER, 0);
  double lower_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_LOWER, 0);
  main_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_MAIN, 0);

  // start - check price not go too far from divation

  double upper_price01 = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_UPPER, 1);
  double lower_price01 = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_LOWER, 1);
  double main_price01 = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_MAIN, 1);

  //if (Bid > upper_price && Bid < (upper_price + (upper_price01 - main_price01) * 0.8) ) { // make sure price not go too far
  if ( Bid >= (upper_price + (upper_price01 - main_price01) * 0.8) ) { // make sure price not go too far
    return false;
  }

  //if (lower_price > Ask && Ask > (lower_price - (main_price01 - lower_price01) * 0.8) ) { // make sure price not go too far

  if (Ask <= (lower_price - (main_price01 - lower_price01) * 0.8) ) { // make sure price not go too far
    return false;
  }

  // end -  check price not go too far from divation


  if (upper_price >= Bid   && Ask >= lower_price)
    return false;

  // add mvamon.getEMA(200) on BollingerBand Range
  if (upper_price >= mvamon.getEMA(200) && mvamon.getEMA(200) >= lower_price )
    return false;

  return true;


}

bool CondTradeWithPressureOnly() {

  g_avg_tick_close_order = 2 ;

  if (vpamon.getAvgTick1M() < 2.50 && vpamon.hasLowerAvg50PerCenterBefore()) {
    g_avg_tick_close_order = vpamon.getAvgTick1M() * 0.8;
  }

  return ( vpamon.getAvgTick1M() >= 2.50 || ( vpamon.hasLowerAvg50PerCenterBefore() &&  vpamon.getAvgTick1M() > 1));
}
bool CondTradeAllowed() {

  if (!order.isNoneOrder()) return  false; // if order is opne , return false
  //## if order is close() , then add sleep if previous order have negative profit

  return (
           IsMarketOpened() && !SingleOrder::HalfHighSpread()
           && BollingerBandSingal(g_main_price)
           //&& ( vpamon.getLastABSPip() != 0 ||  vpamon.getLastDiff() != 0)
           && !weektime.isWeekEndGap()
           && !weektime.isDayEndGap()
           && g_sleep_sec <= 0

           //&& (TimeCurrent() - last_opened_order_time ) > 60 * 60 * 8 // 8 hours
         );
}


//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
int sec = 0;
void MonitorOpenedOrderDynamicCond() { // MonitorOpenedOrderDynamicCond


  if (order.isNoneOrder()) return;

  //if ( trailingActivated && order.profitPip() > ((-1)*stoploss_pip/3) && order.getActualProfit() > 0 && vpamon.getAvgTick1M() < g_avg_tick_close_order) {

  //if ( trailingActivated &&  vpamon.getAvgTick1M() < g_avg_tick_close_order) {
  //  Print("Close order due to trailingActivated=true > 0, AvgTick < ", g_avg_tick_close_order);
  //  order.CloseNow();
  //}

  if (SingleOrder::HighSpread() && order.getActualProfit() > 0) {
    Print("Close order due to Highspread");
    order.CloseNow();
    //g_consec_loss_count =0;
  }

  if (weektime.isDayEndGap() && order.getActualProfit() > 0) {
    Print("Close order due to isDayEndGap()");
    order.CloseNow();
    //g_consec_loss_count =0;
  }
  if (weektime.isWeekEndGap() && order.getActualProfit() > 0) {
    Print("Close order due to isWeekEndGap()");
    order.CloseNow();

  }

  // if (order.getOpenTimeSec() > 2088){// 0.58 hour
  //     Print("Close order due to open time > 0.58 hour or 2088 sec");
  //     order.CloseNow();
  //     return;
  //   }

}

double calVol() {

  if (lot_trade != 0  && g_lot_trade_read == false) {
    Print("calVol():input lot_trade =", lot_trade);
    //g_lot_trade_read = true;
    return RoundDown2Digit(lot_trade);
  }


  double usd_margin_usd4_1_miro_lot = Ask * 1000 / AccountLeverage();
  Print("calVol():usd_margin_usd4_1_miro_lot = ", usd_margin_usd4_1_miro_lot, ", plus ", buffer_pip, " pip/ ", buffer_pip / 100, " USD=", usd_margin_usd4_1_miro_lot + buffer_pip / 100);

  if ( AccountBalance() < (usd_margin_usd4_1_miro_lot  + buffer_pip / 100) ) {
    Print("calVol():Not Enough Money to trade with this EA", ",Balance < ", usd_margin_usd4_1_miro_lot  + 2);
    ExpertRemove();
  }
  if ( AccountBalance() < ( usd_margin_usd4_1_miro_lot + buffer_pip / 100 )) { // && AccountBalance() > ( usd_margin_usd4_1_miro_lot + 5 ) ) {
    Print("calVol():AccountBalance very low , force to traingVol = 0.01");
    return 0.01;
  }

  double tradingVol = AccountBalance() / (usd_margin_usd4_1_miro_lot + buffer_pip / 100) * 0.01;

  //tradingVol = RoundDown2Digit(tradingVol, 2);

  Print("calVol():Trading Volume = ", tradingVol, ",NormalLot to ", RoundDown2Digit(tradingVol));

  if (tradingVol > max_lot ) {
    tradingVol = RoundDown2Digit(max_lot);
    Print("calVol():Force Trading Volume = max_lot", max_lot);
  }

  return RoundDown2Digit(tradingVol);

}

string getComment(void) {

  int direction = (api.getHttpStatus() - 200) % 10;
  /* direction
   *  1 - Price is rasing up trend - so buy
   *  2 - Price is going down trend - so sell
   */
  string comment  = StringFormat("D%dV%.2fB%dF%dT%d", direction, vpamon.getAvgTick1M(), vpamon.getLastABSPip(), vpamon.getLastDiff(), vpamon.getLastTickTotal());
  return comment;
}

bool isProfitWeekOverMax() {
  if (getProfitPipOfThisWeek(true) >= max_pip_of_week ) {
    Print("Ignore Trading - getProfitPipOfThisWeek() > ", max_pip_of_week);
    return true;
  }
  return false;

}




void PrintVPAOrderDetail() {
  if (api.getHttpStatus() > 200 && api.getHttpStatus() <= 299) {
    Print("pip=", order.profitPip() , ",http=", api.getHttpStatus(), ",Bid =", Bid, ",Ask=", Ask, "-", getComment());
  }

}

void PrintIfSleepMode() {

  if (order.isNoneOrder()) {

    if (g_sleep_sec > 0) {
      
      if (g_sleep_sec % 60 == 0) {
        Print("Sleep for ", g_sleep_sec / 60, " min.");
      }
      
      g_sleep_sec--;

      if (g_sleep_sec == 0) {
        Print("Sleep for ", g_sleep_sec / 60, " min.");
        calVol();
      }

    }

  }// is NoneOrder()



}

void OnTimer() {




  vpamon.processTickDataWhenTimer();
  vpamon.CommentInfo(magic_number);

  MonitorOpenedOrderDynamicCond();

  if (order.isNoneOrder() && g_order_opened ) {
    g_order_opened = false;
    ShowLastOrderInfo();
  }

  PrintIfSleepMode();

  if (order.isOpenedOrder()) {

    api.get("http://localhost/EA/signal?magic=" + IntegerToString(magic_number));
    PrintVPAOrderDetail();
    return;
  }
  if (!CondTradeWithPressureOnly()) {
    // not even pass pressure condition
    return;
  }

  // Print out pressure because pressure is good to show
  api.get("http://localhost/EA/signal?magic=" + IntegerToString(magic_number));
  PrintVPAOrderDetail();
  if (api.getHttpStatus() <= 200 || api.getHttpStatus() > 299) {
    // return if signal is not good
    return;
  }

  if (!CondTradeAllowed()) {
    // if not good cond, then return/reject
    return;
  }

  // all good to open Order here





  if (weektime.isNewWeek()) {
    g_trade_vol = calVol();
  }


  if (Bid > g_main_price
      && (mvamon.getEMA(25) < mvamon.getEMA(50) && mvamon.getEMA(50) < mvamon.getEMA(200))
     )
  {

    OpenOrderNow();

  }

  if (Bid < g_main_price
      && (mvamon.getEMA(25) > mvamon.getEMA(50) && mvamon.getEMA(50) > mvamon.getEMA(200))
     )
  {
    OpenOrderNow();
  }


  if (order.isOpenedOrder()) {
    g_order_opened = true;
    int l_profit_pip = profit_pip + (int)(4 * Ask);
    order.modifyOpenedOrder(stoploss_pip , profit_pip + 6);

  }


  return;


}


void OpenOrderNow() {

  int direction = (api.getHttpStatus() - 200) % 10;

  if (MathAbs(vpamon.getLastDiff()) > vpamon.getLastTickTotal()) {
    g_sleep_sec = 60 * 5;
    Print("###Ignore Open Order - |DiffPip| > TickTotal");
    return;
  }

  if (vpamon.getLastTickTotal() + 1 < vpamon.getAvgTick1M()) {
    g_sleep_sec = 60 * 5;
    Print("###Ignore Open Order - TickTotal+1 < AvgTick");
    return;
  }

  if (direction == 1 && vpamon.getLastDiff() > 0 ) {
    order.setVolume(g_trade_vol);
    //order.BuyNow(getComment());
    order.SellNow(getComment());
  }

  if (direction == 2 && vpamon.getLastDiff() < 0) {
    order.setVolume(g_trade_vol);
    //order.SellNow(getComment());
    order.BuyNow(getComment());
  }

  if (order.isNoneOrder()) {
    g_sleep_sec = 60 * 5;
    Print("###Ignore Open Order - Diff conflict with direction. or Diff == 0");
    //PrintVPAOrderDetail();
  }

}

//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester() {
  //---
  double ret = 0.0;
  //---


  return (ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long & lparam, const double & dparam,
                  const string & sparam) {
  //---
}
//+------------------------------------------------------------------+
