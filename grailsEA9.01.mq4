/*

---------------

2018-04-17 - version 9.01
Re new way

#90102 - is good for 100 pip range
2018-08-27
- add max_order_age_min as 15 min
2018-08-28
- close opened order when actualProfit > | Commission | to prevent lost

2018-09-02
- Probably final version for me , add auto calVol()

2018-09-10 # magic 90107
- time out for pending >= 15 min bar
- no time out for opened order

2018-09-12 # magic 90108
- signal_count as 2 (input)
- time out for pending > 15 min bar

2018-09-13 # magic 90109
-Add sleep_after_loss_min = 30;

2018-09-14
- Add Email when Delete pending order

2018-09-17 # magic 90110
- Add Comment to Pending Order

2018-09-17 # magic 90111
- change rang as 95

2018-09-30 # magic 90112
- Change comment way of Grails9algo
*/


//+------------------------------------------------------------------+..
//|                                                    grailsEA3.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Grails EA , Agility FX Trading."
#property link "http://www.grailsEA.com"
#property version "9.0112"
#property strict

#include ".\Include\Account.mqh"
#include ".\Include\HedgeOrderManager.mqh"
#include ".\Include\DenFxUtil.mqh"
#include ".\Include\WebAPI.mqh"
#include ".\Include\VPAMonitor.mqh"
#include ".\Include\WeekTimer.mqh"
#include ".\Include\GrailsAutoLot.mqh"
#include ".\Include\MVAMonitor.mqh"
#include ".\Include\Grails9algo.mqh"
//#include ".\Include\Drawer.mqh"


input int magic_number = 90112;
input bool bollinger_enable = true;
input int max_pip_of_week = 999999999;
input double max_lot = 20 ;
input int timout_pending_min = 15;
input double user_lot_trade = 0.00;
input int buffer_pip = 2000;
input int g9_max_sec = 60;
input int g9_signal_count = 2;
input int sleep_after_loss_min = 30;
input int g9_pip_range = 90;

WebAPI api;
VPAMonitor vpamon;
WeekTimer weektime;
MVAMonitor mvamon;
Grails9algo g9alog(g9_max_sec);
//Drawer drawer;

SingleOrder order;






//BollingerBandSingal
double g_main_price;
double g_max_order_count ;
// Avg Tick
double g_avg_tick_close_order = 2;
double g_trade_vol = 0;
int g_sleep_sec = 0;
bool g_order_opened = false;



//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+


int OnInit() {

  // Show Logo
  ChartSetInteger(0, CHART_COLOR_CHART_UP, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CHART_DOWN, 0, 255);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, 0, 255);
  ChartSetInteger(0, CHART_SHOW_VOLUMES, 0, 1);
  ChartSetInteger(0, CHART_MODE, 0, 1);
  ChartSetInteger(0, CHART_SCALE, 0);
  ChartSetSymbolPeriod(0, NULL, PERIOD_M1);

  EventSetTimer(1);


  if (AccountCompany() == "Trading Point Of Financial Instruments Ltd") {
    // add 13 pip
    SingleOrder::s_high_spread += 13;

  } else if (AccountCompany() != "Tickmill Ltd") {
    // Do not thing
    MessageBox("Unknown Broker name " + AccountCompany(), "Error", MB_ICONINFORMATION);
    //ExpertRemove();

  }

  SingleOrder::setMagicNo(magic_number);

  if (Symbol() != "EURUSD") {
    Print("Error : Symbol is not EURUSD");
    ExpertRemove();
  }

  g_trade_vol = calVol();
  Print("g_trade_vol =", g_trade_vol);
  getProfitPipOfThisWeek();
  SendMail("grailsEA starts ","magic="+StringFormat("%d",magic_number));

  //drawer.drawNow();

  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
  //--- destroy timer
  EventKillTimer();
  SendMail("grailsEA stops ","magic="+StringFormat("%d",magic_number));
// order.ClosePending();

}

bool isProfitWeekOverMax() {

  if (getProfitPipOfThisWeek(true) >= max_pip_of_week ) {
    Print("Ignore Trading - getProfitPipOfThisWeek() > ", max_pip_of_week);
    return true;
  }
  return false;

}


int getProfitPipOfThisWeek(bool fcomment = true) {

  double  profitPip = 0;
  datetime thisMonday = weektime.findThisMonday();
  double allProfit = 0;

  for (int i = OrdersHistoryTotal() - 1; i >= 0; i--) {

    if (OrderSelect(i, SELECT_BY_POS, MODE_HISTORY)) {

      if (OrderOpenTime() > thisMonday && OrderType() != 6 /*type balance*/ ) {


        double actualProfit = RoundDown2Digit(OrderProfit()) + RoundDown2Digit(OrderCommission()) + RoundDown2Digit(OrderSwap());
        allProfit = allProfit + actualProfit;
        continue;
      }

      break;
    }
  }
  profitPip = allProfit / g_trade_vol;
  if (fcomment) Print("getProfitPipOfThisWeek() = ", MathCeil(profitPip) , ",thisMonday is =", thisMonday);
  return (int) MathCeil(profitPip);
}







void OnTick() {

  vpamon.collectOnTick();
  vpamon.CommentInfo(magic_number);
  PreformOrderWhenTesting();
  workWithOrder();

}



bool BollingerBandSingal(double &main_price) {

  if (!bollinger_enable) return true;

  double upper_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_UPPER, 0);
  double lower_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_LOWER, 0);
  main_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_MAIN, 0);

  // start - check price not go too far from divation

  double upper_price01 = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_UPPER, 1);
  double lower_price01 = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_LOWER, 1);
  double main_price01 = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_MAIN, 1);

  // if ( Bid >= (upper_price + (upper_price01 - main_price01) * 0.8) ) { // make sure price not go too far
  //   return false;
  // }

  // //if (lower_price > Ask && Ask > (lower_price - (main_price01 - lower_price01) * 0.8) ) { // make sure price not go too far

  // if (Ask <= (lower_price - (main_price01 - lower_price01) * 0.8) ) { // make sure price not go too far
  //   return false;
  // }
  // end -  check price not go too far from divation

  if (upper_price >= Bid   && Ask >= lower_price)
    return false;

  // add mvamon.getEMA(200) on BollingerBand Range
  //if (upper_price >= mvamon.getEMA(200) && mvamon.getEMA(200) >= lower_price )
  //  return false;

  return true;


}


bool isUpperTrend() {
  return ( Bid > mvamon.getEMA(25) && mvamon.getEMA(25) > mvamon.getEMA(50) && mvamon.getEMA(50) > mvamon.getEMA(200));
}
bool isLowerTrend() {
  return (Bid < mvamon.getEMA(25) && mvamon.getEMA(25) < mvamon.getEMA(50) && mvamon.getEMA(50) < mvamon.getEMA(200));
}

int diffEMA200() {
  double price = Ask;

  if (isUpperTrend()) price = Bid; // for sell order
  if (isLowerTrend()) price = Ask; // for Buy order

  int pip = (int) (NormalizeDouble(price - mvamon.getEMA(200), _Digits) / _Point);
  return pip;
}
bool CondTradeAllowed() {



  return (
           IsMarketOpened() && !SingleOrder::HalfHighSpread()
           //&& ( vpamon.getAvgTick1M() >= 2.50)
           && (isUpperTrend()   || isLowerTrend())
           && BollingerBandSingal(g_main_price)
           && !weektime.isWeekEndGap()
           && !weektime.isDayEndGap()
           && (MathAbs(diffEMA200()) > 150)
         );
}


//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
int sec = 0;

double calVol() {

  if (user_lot_trade != 0 ) {
    Print("calVol():lot trade as input lot_trade =", user_lot_trade);
    return RoundDown2Digit(user_lot_trade);
  }


  double usd_margin_usd4_1_miro_lot = Ask * 1000 / AccountLeverage();
  Print("calVol():usd_margin_usd4_1_miro_lot = ", usd_margin_usd4_1_miro_lot, ", plus ", buffer_pip, " pip/ ", buffer_pip / 100, " USD=", usd_margin_usd4_1_miro_lot + buffer_pip / 100);

  if ( AccountBalance() < (usd_margin_usd4_1_miro_lot  + buffer_pip / 100) ) {
    Print("calVol():Not Enough Money to trade with this EA", ",Balance < ", usd_margin_usd4_1_miro_lot  + 2);
    ExpertRemove();
  }
  if ( AccountBalance() < ( usd_margin_usd4_1_miro_lot + buffer_pip / 100 )) { // && AccountBalance() > ( usd_margin_usd4_1_miro_lot + 5 ) ) {
    Print("calVol():AccountBalance very low , force to traingVol = 0.01");
    return 0.01;
  }

  double tradingVol = AccountBalance() / (usd_margin_usd4_1_miro_lot + buffer_pip / 100) * 0.01;

  //tradingVol = RoundDown2Digit(tradingVol, 2);

  Print("calVol():Trading Volume = ", tradingVol, ",NormalLot to ", RoundDown2Digit(tradingVol));
  if (tradingVol > max_lot ) {
    tradingVol = RoundDown2Digit(max_lot);
    Print("calVol():Force Trading Volume = max_lot", max_lot);
  }

  return RoundDown2Digit(tradingVol);

}

string getComment(void) {

  int direction = (api.getHttpStatus() - 200) % 10;

  /* direction
   *  1 - Price is rasing up trend - so buy
   *  2 - Price is going down trend - so sell
   */

  string comment  = StringFormat("D%dV%.2fB%dF%dT%d", direction, vpamon.getAvgTick1M(), vpamon.getLastABSPip(), vpamon.getLastDiff(), vpamon.getLastTickTotal());
  return comment;
}



void PrintVPAOrderDetail() {
  if (api.getHttpStatus() > 200 && api.getHttpStatus() <= 299) {
    Print("pip=", order.profitPip() , ",http=", api.getHttpStatus(), ",Bid =", Bid, ",Ask=", Ask, "-", getComment());
  }

}

bool CondTradeWithPressureOnly() {

  g_avg_tick_close_order = 2 ;

  if (vpamon.getAvgTick1M() < 2.50 && vpamon.hasLowerAvg50PerCenterBefore()) {
    g_avg_tick_close_order = vpamon.getAvgTick1M() * 0.8;
  }

  return ( vpamon.getAvgTick1M() >= 2.50 || ( vpamon.hasLowerAvg50PerCenterBefore() &&  vpamon.getAvgTick1M() > 1));

}
void monitorSleep(){

  if (order.isNoneOrder() && g_sleep_sec > 0) {
    g_sleep_sec--;
    if (g_sleep_sec % 60 == 0) {
      Print("Sleep for ", g_sleep_sec / 60, " min.");
    }
    return ;
  }
  
}
bool isSleepMode(){
  return g_sleep_sec > 0;
}
void OnTimer() {

  vpamon.processTickDataWhenTimer();
  vpamon.CommentInfo(magic_number);

  if (weektime.isNewWeek()) {
    g_trade_vol = calVol();
  }


  g9alog.monitor();
  monitorSleep();
  
  if (!CondTradeWithPressureOnly())
    return;

  api.get("http://localhost/EA/signal?magic=" + IntegerToString(magic_number));

  if (api.getHttpStatus() <= 200 || api.getHttpStatus() > 299) return;

  // it's has signal
  PrintVPAOrderDetail();

  //================================

  

  if (!CondTradeAllowed())
    return;

  if (order.isNoneOrder()) {

    if (isSleepMode()) {
      Print("#addSignal() Ignored ,because isSleepMode() == true ,  g_sleep_sec=", g_sleep_sec);
      return;
    }

    g9alog.addSignal(api.getHttpStatus());
    workWithOrder();

  }



}

void workWithOrder() {

  if (order.isNoneOrder() && g9alog.getCount() >= g9_signal_count) {
    // OpenPendOrder Here
    order.setVolume(g_trade_vol);
    order.setComment(g9alog.getComment());
    if (isLowerTrend()) order.BuyPending(g9_pip_range, -100, 120);
    if (isUpperTrend()) order.SellPending(-g9_pip_range, -100, 120);
    g9alog.reset();
    SendMail("grailsEA just open pending order ","magic="+StringFormat("%d",magic_number));
    return;
  }

  if (order.isPendingOrder() && order.getBarPos() > timout_pending_min) {
    Print("ClosePending() ,order_min  > ", timout_pending_min, " min");
    order.ClosePending();
    SendMail("grailsEA just delete pending order ","magic="+StringFormat("%d",magic_number));
    return;

  }
  if (order.isOpenedOrder()){
    
    int pip = order.profitPip() ;
    // sleep 30 min = 30 * 60 sec
    g_sleep_sec = pip < 0 ? (sleep_after_loss_min * 60)+1 : 0;
    return;
  }


  if (order.isBuyStopOrder()) {

    int diff = order.getDistancePipPending()  - g9_pip_range;
    if (diff > 0 )
      order.modifyPendingOrder(-diff , -100 , 120);


  }

  if (order.isSellStopOrder()) {

    int diff = order.getDistancePipPending()  + g9_pip_range;
    if (diff < 0)
      order.modifyPendingOrder(-diff, -100, 120);
  }

}

void PreformOrderWhenTesting() {

  if (IsTesting()) {
    g9alog.fake();
  }

}

//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester() {
  //---
  double ret = 0.0;
  //---


  return (ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long & lparam, const double & dparam,
                  const string & sparam) {
  //---
}
//+------------------------------------------------------------------+
