/*
22-MAR-2017 - start grails EA after failed with 4plus1 and grails1 ,grails2 EA
version 3.02 - ABS pip cond
version 3.05 - ABS pip cond +  bollinger band + Pip Pressure stop
version 3.05 - ABS pip cond +  bollinger band(Enable/Disable Input)
  EURUSD , cool + No Bollinger Band + Pip Pressure stop
  GUBUSD , cool + Bollinger Band + Pip Pressure stop
version 3.06 - success with win rate , + reduce stoploss from -25 to -20
             - Add ABS(Diff) < TickTotal
             - Add Diff != 0
version 3.07 - add support buy3/sell3 algo
             - add close when signalLast3SecHasZeroTick();
version 3.08 - Change ABS(Diff) > TickTotal
version 3.09 - adding Dual Order
             - add Auto Resize Lot
        3.09.1 - Fix bug volume always 0.01
        3.09.2 - Add Handle when T/P was reached
        3.09.3 - if (count > 3) return / no open order
version 3.10
   - grails add return 22X
   - input Enable_AUTO_resize_lot
=== BIG CHANGE
version 4.01 move to new stategy
version 4.02
   +not care server directly , care only Bid price
   +add close when profit > 0 , avg_tick < 2

2017-06-20 - version 4.03
    + implement direction condition and pip between 2 price signal
2017-07-04 - version 4.03.02
    + add vpa.lastDiff() as condition too
    + add auto lot
2017-07-13 - version 4.03.03
    + add ABS{vpamon.getLastDiff()) > 1

2017-07-19 - version 4.03.04
    + check sig 2 previous sig time , rather than 1 previous sig time
2017-07-204 - version 4.03.05
    + When order is open if sig , then close order
2017-07-31 - version 4.03.06
    + time between order = 8 hours

---------------------------------------------
Big change
---------------------------------------------
2017-08-21 - version 5.01.01
   + change new stategy
   + 1) using Hedge Order Manager
   + 2) using BollingerBandSingal

2017-08-30 - done
2017-08-30 - version 50102
  +apply hedge trailing
2017-09-02 - version 50103
   + apply callVol();
2017-09-05 - version 50103
  + add HedgeOrderManager::isTooDifferenceUpperLower() , not test yet !
2017-09-13 - version 50103
   + Improve cal lot
 ========= Start 5.02
2017-08-21 - version 5.02.01 // seems go
  + re comment and message
  + add low AvergePip , >= 1  and cond < 2.5


2017-10-16 - version 5.02.02
   + when reach max order , close when profit = -5%
2017-10-17 - version 5.02.03
   + swap direction
2017-10-18 - version 5.02.04
   + g_max_loss_pip


==== Version 6.01
2017-10-23 - verison  6.01.01
2017-10-25 - version 6.01.02
+ no open order when ( LastABS != 0 && LastDiff != 0)
2017-10-25 - version 6.0.1.03
+ add sleep 10 min ,if previous order get loss
2017-10-30 - version 6.0.1.04
+ order.profitPip() > ((-1)*stoploss_pip/3)
+ set g_sleep_sec as twice =  60*10*2


==== Version 6.02
2017-11-02 - version 6.02.01
Buy & Sell consider with diffPip too

2017-11-06 - version 6.02.02
StopLoss same as get profit
Sleep 5 min if get loss

2017-11-07 - version 6.02.03
add close order when trailingActivated= true and avgTick < g_avg_tick_close_order

2017-11-07 - version 6.02.04
add CalVol() and Weekly Timer condition
2017-11-07 - version 6.02.05
not all to sell when F == 0
x4/3 of volumn when buy
x1 of volumn when sell


2017-11-07 - version 6.02.06
Sleep 1 hour when loss

2017-11-14 - version 6.03.01
sleep as 5 min when loss
add change open order mode when consec_loss >=3 

2017-11-14 - version 6.03.01 (same version ) 
fix cal Rt_stop_loss from profit_pip
2017-11-15 - version 6.03.02
close order ,when HighSpread , actualProfit > 0
2017-11-16 - version 6.03.03
 + input g_open_order_mode
 + improve comment bug

2017-11-16 - version 6.04.01
 + getProfitPipOfThisWeek
 2017-11-19 - version 6.04.02
 + NormalLot()
 + AutoModeSelect Based on last 7 orders

*/

//+------------------------------------------------------------------+..
//|                                                    grailsEA3.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Grails EA , Agility FX Trading."
#property link "http://www.grailsEA.com"
#property version "6.0402"
#property strict

#include ".\Include\Account.mqh"
#include ".\Include\HedgeOrderManager.mqh"
#include ".\Include\DenFxUtil.mqh"
#include ".\Include\WebAPI.mqh"
#include ".\Include\VPAMonitor.mqh"
#include ".\Include\WeekTimer.mqh"
#include ".\Include\GrailsAutoLot.mqh"




enum enum_open_order_mode{
   open_reverse,
   open_forward,
};




WebAPI api;
VPAMonitor vpamon;
WeekTimer weektime;
//GrailsAutoLot autolot;
//HedgeOrderManager hMan;
SingleOrder order;

input int magic_number = 60402;
input int profit_pip = 50;
input int stoploss_pip = -50;
input int ABS_pip_per_sec = 12;
input bool bollinger_enable = true;
input int trailingstep = 10;
input int lot_time = 16;
input int max_pip_of_week = 500;
input enum_open_order_mode open_order_mode;


int test_value = 0;
int g_max_loss_pip = 0;

//BollingerBandSingal
double g_main_price;
double g_max_order_count ;
// Avg Tick
double g_avg_tick_close_order = 2;
double g_trade_vol = 0;
bool trailingActivated = false;
int g_sleep_sec = 0;

//  varialble of open_order_mode
int g_consec_loss_count = 0;
enum_open_order_mode g_open_order_mode = open_order_mode;

//--------add pre_buy,pre_sell state for version 6.03

void resetOpenMode(){
   g_consec_loss_count = 0;
   g_open_order_mode = open_reverse;
   
}

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+


int OnInit() {

  // Show Logo


  ChartSetInteger(0, CHART_COLOR_CHART_UP, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CHART_DOWN, 0, 255);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, 0, 255);
  ChartSetInteger(0, CHART_SHOW_VOLUMES, 0, 1);
  ChartSetInteger(0, CHART_MODE, 0, 1);
  ChartSetInteger(0, CHART_SCALE, 0);
  ChartSetSymbolPeriod(0, NULL, PERIOD_M1);

  EventSetTimer(1);


  if (AccountCompany() == "Trading Point Of Financial Instruments Ltd") {
    // add 13 pip
    SingleOrder::s_high_spread += 13;

  } else if (AccountCompany() != "Tickmill Ltd") {
    // Do not thing
    MessageBox("Unknown Broker name " + AccountCompany(), "Error", MB_ICONINFORMATION);
    //ExpertRemove();

  }

  SingleOrder::setMagicNo(magic_number);
  
  if (Symbol() != "EURUSD") {
    Print("Error : Symbol is not EURUSD");
    ExpertRemove();
  }
  
  g_trade_vol = calVol();
  Print("g_trade_vol =", g_trade_vol);
  getProfitPipOfThisWeek();
   autoModeLast7Order();
   ExpertRemove();
  
  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
  //--- destroy timer
  EventKillTimer();
// order.ClosePending();

}

int getProfitPipOfThisWeek(){
   
   double  profitPip = 0;
   datetime thisMonday = weektime.findThisMonday();
   double allProfit = 0; 
   
   for(int i = OrdersHistoryTotal()-1; i >= 0; i--){
   
      if (OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)){
      
         if(OrderOpenTime() > thisMonday){

            double actualProfit = OrderProfit() + OrderCommission();
            allProfit = allProfit +actualProfit;
            continue;
         }
         
         break;
      }
   }
   profitPip = allProfit / g_trade_vol;
   Print("getProfitPipOfThisWeek() = ",MathCeil(profitPip) ,",thisMonday is =",thisMonday);
   return (int) MathCeil(profitPip);
}


enum_open_order_mode autoModeLast7Order(){
  
   datetime thisMonday = weektime.findThisMonday();
   int countRwin = 0;
   int countFwin = 0;
   
    for(int i = OrdersHistoryTotal()-1; i >= OrdersHistoryTotal()-8; i--){
      if (OrderSelect(i,SELECT_BY_POS,MODE_HISTORY)){
         if(true || OrderOpenTime() > thisMonday){
            if (OrderOpenTime() > thisMonday)
               break;
            double actualProfit = OrderProfit() + OrderCommission();
            if(StringFind(OrderComment(),"MR") != -1 && actualProfit > 0){
               countRwin++;
            }
            if(StringFind(OrderComment(),"MF") != -1 && actualProfit > 0){
               countFwin++;
            }
            if(StringFind(OrderComment(),"MR") != -1 && actualProfit < 0){
               countFwin++;
            }
            if(StringFind(OrderComment(),"MR") != -1 && actualProfit > 0){
               countRwin++;
            }
           
         }
      }
   }
   
   return (countRwin>= countFwin ? open_reverse: open_forward);

}

void swapOpenOrderMode(){

   Print("swapOpenOrderMode:: g_consec_loss_count == ",g_consec_loss_count);
   Print("swapOpenOrderMode:: g_open_order_mode == ", g_open_order_mode == open_forward ? "open_forward" : "open_reverse");
   if (g_consec_loss_count < 3 ) return ;
    g_consec_loss_count = 0;
    
   if (g_open_order_mode == open_forward )
      g_open_order_mode = open_reverse;
   else if (g_open_order_mode == open_reverse )
      g_open_order_mode = open_forward;
      
   Print("Swap mode to ", g_open_order_mode == open_forward ? "open_forward" : "open_reverse");
}
void PreformOrderWhenTesting() {
  if (IsTesting()) {



  }

}




void OnTick() {

  vpamon.collectOnTick();
  vpamon.CommentInfo(magic_number);

  PreformOrderWhenTesting();





  if (order.isOpenedOrder()) {
    int pip = order.profitPip() ;
    // set sleep = 1 hour
    g_sleep_sec = pip < 0 ? 60 * 5 : 0;


    if (order.isProfitOverRtTakeProfit()) {
      order.trailingRtStopLoss();
      trailingActivated = true;
      Print("trailing stop loss is activated , pip = ", pip);
    }

    if ( trailingActivated && order.isProfitLowerRtStopLoss()) {
      g_consec_loss_count = 0;
      order.CloseNow();
      return;
    }

    if (order.profitPip() < stoploss_pip) {
      Print("Close order due to profit=", pip, " < stoploss_pip=", stoploss_pip);
      order.CloseNow();
      g_consec_loss_count++;
      return;
    }
    return;

  }



}

bool IsMarketOpened() {

  return MarketInfo(Symbol(), MODE_TRADEALLOWED) == 1;

}

bool BollingerBandSingal(double &main_price) {

  if (!bollinger_enable) return true;

  double upper_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_UPPER, 0);
  double lower_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_LOWER, 0);
  main_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_MAIN, 0);

  if (Bid > upper_price  || lower_price > Ask  ) {
    return true;
  }
  return false;
}

bool CondTradeAllowed() {

  if (!order.isNoneOrder()) return  false;

  //## if order is close() , then add sleep if previous order have negative profit

  if (g_sleep_sec > 0) {
    if (g_sleep_sec % 60 == 0) Print("Sleep for ", g_sleep_sec/60, " min.");
    g_sleep_sec--;
    return false;
  }

  g_avg_tick_close_order = 2 ;

  if (vpamon.getAvgTick1M() < 2.50 && vpamon.hasLowerAvg50PerCenterBefore()) {
    g_avg_tick_close_order = vpamon.getAvgTick1M() * 0.8;
  }

  return (
           IsMarketOpened() && !SingleOrder::HalfHighSpread()

           && ( vpamon.getAvgTick1M() >= 2.50 || ( vpamon.hasLowerAvg50PerCenterBefore() &&  vpamon.getAvgTick1M() > 1))
           && BollingerBandSingal(g_main_price)
           && ( vpamon.getLastABSPip() != 0 ||  vpamon.getLastDiff() != 0)
           && !weektime.isWeekEndGap()
           //&& (TimeCurrent() - last_opened_order_time ) > 60 * 60 * 8 // 8 hours
         );
}


//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
int sec = 0;
void MonitorOpenedOrder() {


  if (order.isNoneOrder()) return;

  //if ( trailingActivated && order.profitPip() > ((-1)*stoploss_pip/3) && order.getActualProfit() > 0 && vpamon.getAvgTick1M() < g_avg_tick_close_order) {
  if ( trailingActivated &&  vpamon.getAvgTick1M() < g_avg_tick_close_order) {
    Print("Close order due to trailingActivated=true > 0, AvgTick < ", g_avg_tick_close_order);
    order.CloseNow();
    g_consec_loss_count =0;
  }

  if (SingleOrder::HighSpread() && order.getActualProfit() > 0){
    order.CloseNow();
    //g_consec_loss_count =0;
  }

}

double calVol() {
  

  double usd_margin_usd4_1_miro_lot = Ask * 1000 / AccountLeverage();
  Print("usd_margin_usd4_1_miro_lot = ", usd_margin_usd4_1_miro_lot, ", plus 1000 pip/ 10 USD=", usd_margin_usd4_1_miro_lot + 10);
  
  if ( AccountBalance() < ( usd_margin_usd4_1_miro_lot + 10 ) &&AccountBalance() < ( usd_margin_usd4_1_miro_lot + 5 ) ){
    Print("AccountBalance very low , force to traingVol = 0.01");
    return 0.01;
  }
  double tradingVol = AccountBalance() / (usd_margin_usd4_1_miro_lot + 10) * 0.01;

  tradingVol = NormalizeDouble(tradingVol, 2);
  if (tradingVol < 0.01 ) {
    Print("Not Enough Money to trade with this EA");
  }
  Print("Trading Volume = ", tradingVol, ",NormalLot to ",NormalLot(tradingVol));
  if (tradingVol > 10 ) {
    tradingVol = 10;
    Print("Force Trading Volume = 10");
  }



  return NormalLot(tradingVol);

}

string getComment(){

  
  int direction = (api.getHttpStatus() - 200) % 10;
  /* direction
   *  1 - Price is rasing up trend - so buy
   *  2 - Price is going down trend - so sell
   */
  string str_open_mode = g_open_order_mode == open_forward ? "F" : "R";
  string comment  = StringFormat("M%s,D%dV%.2fB%dF%dT%d",str_open_mode , direction, vpamon.getAvgTick1M(), vpamon.getLastABSPip(), vpamon.getLastDiff(), vpamon.getLastTickTotal());

  return comment;
}
void OnTimer() {

  vpamon.processTickDataWhenTimer();
  vpamon.CommentInfo(magic_number);

  MonitorOpenedOrder();

  if (!CondTradeAllowed())
    return;

  api.get("http://localhost/EA/signal?magic=" + IntegerToString(magic_number));

  if (api.getHttpStatus() > 200 && api.getHttpStatus() <= 299 ) {
    Print( "http=", api.getHttpStatus(), ",Bid =", Bid, ",Ask=", Ask);
    
    if (api.getHttpStatus() > 200) {
    
      if (getProfitPipOfThisWeek() >=max_pip_of_week ){
         Print("Igore Trading - getProfitPipOfThisWeek() > ",max_pip_of_week);
         return;
      }
      
      int count = (api.getHttpStatus() - 200) / 10;   

      // Set traingVolume
      if (weektime.isNewWeek()) {
        g_trade_vol = calVol();
        resetOpenMode();  
      }

      if (Bid > g_main_price) {
      
        if ( vpamon.getLastDiff() < 0) {//|| (vpamon.getLastDiff() == 0 &&  vpamon.getLastTickTotal() > vpamon.getAvgTick1M()))
          swapOpenOrderMode();
          order.setVolume(g_trade_vol);
          if (g_open_order_mode == open_reverse )
            order.SellNow(getComment());
          else 
            order.BuyNow(getComment());           
        }

      }
      else {
        if ( vpamon.getLastDiff() > 0 || (vpamon.getLastDiff() == 0 &&  vpamon.getLastTickTotal() > vpamon.getAvgTick1M())) {
          swapOpenOrderMode();
          order.setVolume(g_trade_vol);
          if (g_open_order_mode == open_reverse )
            order.BuyNow(getComment());
          else 
            order.SellNow(getComment());
        }
      }


      if (order.isOpenedOrder()) {

        int l_profit_pip = profit_pip + (int)(4*Ask);
        order.modifyOpenedOrder(stoploss_pip - 5, 1000);
        trailingActivated = false;
        order.setRtStopLossPip(l_profit_pip - trailingstep * 2);
        order.setRtTakeProfitPip(l_profit_pip);
        order.setRtTrailingStepPip(trailingstep);
      }


      return;

    }
  }




  // Must set Pending order here


}
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester() {
  //---
  double ret = 0.0;
  //---


  return (ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long& lparam, const double& dparam,
                  const string& sparam) {
  //---
}
//+------------------------------------------------------------------+
