/*
26-NOV-2016 - first version

*/

//+------------------------------------------------------------------+..
//|                                             DenAutoFXTrading.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, ManowUK EA , Agility FX Trading."
#property link "http://www.4plus1ea.com"
#property version "1.00"
#property strict

#include ".\Include\Account.mqh"
#include ".\Include\SingleOrder.mqh"
#include ".\Include\DenFxUtil.mqh"
#include ".\Include\DualOrderManager.mqh"
#include ".\Include\WebAPI.mqh"
#include ".\Include\VPAMonitor.mqh"

input uint EA_magic_number = 10001;
input bool EA_trade_on_17_18_hr = false;
input bool EA_auto_volume_trade = false; 



DualOrderManager dualman;
WebAPI api;
VPAMonitor vpamon;


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {

   // Show Logo


  ChartSetInteger(0, CHART_COLOR_CHART_UP, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CHART_DOWN, 0, 255);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, 0, 255);
  ChartSetInteger(0, CHART_SHOW_VOLUMES, 0, 1);
  ChartSetInteger(0, CHART_MODE, 0, 1);
  ChartSetInteger(0, CHART_SCALE, 3);
  ChartSetSymbolPeriod(0,NULL, PERIOD_M1);
  
  EventSetTimer(1);



  // Checking Broker

  //"Trading Point Of Financial Instruments Ltd" = XM
  //"Tickmill Ltd" == TICK MILL



  if (AccountCompany() == "Trading Point Of Financial Instruments Ltd") {
      // add 13 pip
    SingleOrder::s_high_spread += 13;
    
  } else if (AccountCompany() == "Tickmill Ltd") {
      // Do not thing
  
  } else {
    MessageBox("Unknown Broker name " + AccountCompany(), "Error",MB_ICONINFORMATION);
    ExpertRemove();
  }
  SingleOrder::setMagicNo(EA_magic_number);
  
  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
  //--- destroy timer
  EventKillTimer();

}

double calVolume(){
   
   if (!EA_auto_volume_trade) return 0.01;
   
   // 100 USD , -> 1 , 90 pip
   // 100 USD , -> 0.5 , 45 pip
   
   // 10 USD , -> 0.05
   
   int mul =(int) (AccountBalance()/10);
   double vol =  mul*0.05;
   if (vol > 100 )
      return 100;
   return vol;
}

int http_status = 0;
void SetDaulPendingOrder(){
    
    if(dualman.areBothNoneOrder()){
         string comment  = StringFormat("%d,V=%.2f,B=%d,F=%d,T=%d",http_status-200,vpamon.getAvgTick1M(),vpamon.getLastABSPip(),vpamon.getLastDiff(),vpamon.getLastTickTotal());
         dualman.setComment(comment);
         dualman.setVolume(calVolume());
         dualman.openBothPending();         
   }
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+

void OnTick() {

   vpamon.collectOnTick();
   vpamon.CommentInfo();

  //Monitor Orders Here
   dualman.monitor(SignalCancel());
   
   
   //For Testing
   
   if(IsTesting()){
      SetDaulPendingOrder();
   }
  
}

bool IsMarketOpened(){
   return MarketInfo(Symbol(),MODE_TRADEALLOWED)== 1;
}

bool Is17And18Hr(){

    if (!EA_trade_on_17_18_hr) 
      return true;
    datetime current = TimeCurrent();
    int hour = TimeHour(current);
    if (hour == 17 || hour == 18)
      return true;
    return false;
    
}

bool CondTradeAllowed(){

   return ( Is17And18Hr() && IsMarketOpened() && !SingleOrder::HighSpread() && vpamon.getAvgTick1M() > 3  && vpamon.getLastTickTotal() > vpamon.getAvgTick1M() );
}

bool SignalCancel(){

   return false;
   if (IsTesting())return false;
   //bool currentTickAvgHigher = vpamon.getPreviousTickTotal() < vpamon.getAvgTick1M();
   //return false;
  return (SingleOrder::HighSpread()|| vpamon.signalLast3SecHasZeroTick()|| vpamon.signalPressureHighAndNoMove(false)); ;
   
}
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
int sec=0;


void OnTimer() {

   vpamon.processTickDataWhenTimer();
   vpamon.CommentInfo();

   if (!CondTradeAllowed())
      return;
   if(!dualman.areBothNoneOrder())
      return;
   
   
   string fullURL ="http://localhost/EA/signal";
   string output;
   int res = api.get(fullURL,output);
   
   if(res> 200 && res <= 299 ){
      http_status = res;
      Print( "http status return =",res ,",output=",output);   
      SetDaulPendingOrder();
   }
  

   
   
   // Must set Pending order here
  
   
}
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester() {
  //---
  double ret = 0.0;
  //---

 
  return (ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long& lparam, const double& dparam,
                  const string& sparam) {
  //---
}
//+------------------------------------------------------------------+
