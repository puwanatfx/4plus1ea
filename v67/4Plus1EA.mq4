/*
26-NOV-2016 - first version

*/

//+------------------------------------------------------------------+..
//|                                             DenAutoFXTrading.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+


/*
Version History

1.11 - add Hedging  USD 
*/

#define software_version "1.18"

#property copyright "Copyright 2016, 4Plus1 EA, Agility FX Trading."
#property link "http://www.4plus1ea.com"
#property version software_version
#property strict

#include ".\Include\Account.mqh"
#include ".\Include\OrderManager.mqh"
#include ".\Include\SignalManager.mqh"
#include ".\Include\LicenseManager.mqh"
#include ".\Include\USDCounterHedger.mqh"


input uint EA_MAX_tradable_amount = 50000;
input uint EA_magic_number = 505050;
input bool EA_close_order_when_exit = true;

OrderManager orderman;
SignalManager signalman;
datetime startdate;
datetime expired_date = D'2018.01.01';



//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
bool checkInitialParam(){
   LicenseManager licman;
   //## license verify account
   
   if(!licman.verify(IsDemo(),AccountNumber())){
      MessageBox("Your account is not allowed to trade with this EA .\n Contact support@4plus1ea.com","Error",MB_ICONINFORMATION);
      return false;
   }
   Print("License Verified!");
    if(EA_MAX_tradable_amount < 500 ){
      MessageBox("EA_MAX_tradable_amount is at least 500 USD","Error",MB_ICONINFORMATION);
      return false;
   }
    if(EA_MAX_tradable_amount > 50000 ){
      MessageBox("EA_MAX_tradable_amount is not more than 50000 USD","Error",MB_ICONINFORMATION);
      return false;
   }
   if ( EA_MAX_tradable_amount%500 > 0){
      MessageBox("EA_MAX_tradable_amount must be multiple of 500 \n example 500 ,1000 ,1500 ...till 50000","Error",MB_ICONINFORMATION);
      return false;
   }
   if(AccountCurrency() != "USD"){
      MessageBox("Your account currency is not USD","Error",MB_ICONINFORMATION);
      return false;
   }
   if (USDCounterHedger::currencyType(Symbol()) == no_usd ){
      MessageBox("Your Chart Symbol is not XXXUSD nor USDXXX ","Error",MB_ICONINFORMATION);
      return false;
   }
   if(TimeCurrent() > expired_date){
      MessageBox("Your License is expired , please contact admin ","Error",MB_ICONINFORMATION);
      return false;
   }
   if(Symbol()=="USDJPY"){
      MessageBox("Not Allow to trade with USDJPY , too dangerous ","Error",MB_ICONINFORMATION);
      return false;
   }
   if(Symbol()=="USDCAD"){
      MessageBox("Not Allow to trade with USDCAD , too dangerous ","Error",MB_ICONINFORMATION);
      return false;
   }
   
   return true;
}
int OnInit() {
   
   
   if(!checkInitialParam()) return INIT_FAILED;
   
  

  ChartSetInteger(0, CHART_COLOR_CHART_UP, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CHART_DOWN, 0, 255);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, 0, 255);
  ChartSetInteger(0, CHART_SHOW_VOLUMES, 0, 1);
  ChartSetInteger(0, CHART_MODE, 0, 1);
  ChartSetInteger(0, CHART_SCALE, 0);
  ChartSetSymbolPeriod(0,NULL, PERIOD_M1);
  startdate = Time[0];
  EventSetTimer(1);

  if(orderman.countMarketOrder() >0){
      Alert("Error : there is opened or pending order of ",Symbol());
      Print("Error : there is opened or pending order of",Symbol());
      ExpertRemove();
  }


  // Checking Broker

  //"Trading Point Of Financial Instruments Ltd" = XM
  //"Tickmill Ltd" == TICK MILL
   
  if (AccountCompany() == "Trading Point Of Financial Instruments Ltd") {
    // add 13 pip
    SingleOrder::s_high_spread += 13;
  } else if (AccountCompany() == "Tickmill Ltd") {
  } else {
    MessageBox("Unknown Broker name " + AccountCompany(), "Error",MB_ICONINFORMATION);
    ExpertRemove();
  }
  
  SingleOrder::setSlippage(50);
  SingleOrder::setMagicNo(EA_magic_number);
  orderman.setMaxTradableAmount(EA_MAX_tradable_amount);
  orderman.setCloseOrderWhenExit(EA_close_order_when_exit);
  MessageBox("4plus1 EA could have risk to lose 100% of your money\n Click OK to continue","Warning",MB_ICONWARNING);

  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
  //--- destroy timer
  EventKillTimer();
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+

void OnTick() {


  //ExpertRemove();
  signalman.collectOnTick();
  if (!orderman.hasOrder()) {
    if ( orderman.countMarketOrder() > 0)
      return;
    if (signalman.singalBuy()) {
      orderman.setOpBuy();
      orderman.openFirstOrder();
      return;
    }

    if (signalman.singalSell() ) {
      orderman.setOpSell();
      orderman.openFirstOrder();
      return;
    }

  } else {
    
    orderman.monitor(signalman.isMarketCrazy());
  }
}

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer() {

  

  signalman.processTickDataWhenTimer();
  //Make sure not market order before start new first order;
  if (!orderman.hasOrder())
    while (!orderman.clearMarketOrder()) {
      Sleep(50);
    }
}
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester() {
  //---
  double ret = 0.0;
  //---

  /*---
  Print("WinCount=", orderman.getWin());
  Print("LossCount=", orderman.getLose());
  Print("StartDate = ", startdate);
  Print("EndDate =", Time[0]);
  Print("MAX Profit (ForwardMode)=", orderman.getMaxForwardProfit());
  Print("MAX Profit (RevertMode)=", orderman.getMaxRevertProfit());
  */
  return (ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long& lparam, const double& dparam,
                  const string& sparam) {
  //---
}
//+------------------------------------------------------------------+
