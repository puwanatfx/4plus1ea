/*
26-NOV-2016 - first version

*/

//+------------------------------------------------------------------+..
//|                                             DenAutoFXTrading.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, ManowUK EA , Agility FX Trading."
#property link "http://www.4plus1ea.com"
#property version "1.00"
#property strict

#include ".\Include\Account.mqh"
#include ".\Include\SingleOrder.mqh"
#include ".\Include\DenFxUtil.mqh"
#include ".\Include\DualOrderManager.mqh"
#include ".\Include\WebAPI.mqh"
#include ".\Include\VPAMonitor.mqh"

// Success version 100 pip a day on 16-FEB-2017



DualOrderManager dualman;
WebAPI api;
VPAMonitor vpamon;


//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {

   // Show Logo


  ChartSetInteger(0, CHART_COLOR_CHART_UP, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CHART_DOWN, 0, 255);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, 0, 255);
  ChartSetInteger(0, CHART_SHOW_VOLUMES, 0, 1);
  ChartSetInteger(0, CHART_MODE, 0, 1);
  ChartSetInteger(0, CHART_SCALE, 3);
  ChartSetSymbolPeriod(0,NULL, PERIOD_M1);
  
  EventSetTimer(1);



  // Checking Broker

  //"Trading Point Of Financial Instruments Ltd" = XM
  //"Tickmill Ltd" == TICK MILL



  if (AccountCompany() == "Trading Point Of Financial Instruments Ltd") {
      // add 13 pip
    SingleOrder::s_high_spread += 13;
    
  } else if (AccountCompany() == "Tickmill Ltd") {
      // Do not thing
  
  } else {
    MessageBox("Unknown Broker name " + AccountCompany(), "Error",MB_ICONINFORMATION);
    ExpertRemove();
  }
  SingleOrder::setMagicNo(101);
  
  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
  //--- destroy timer
  EventKillTimer();

}
int http_status = 0;
void SetDaulPendingOrder(){
    
    if(dualman.areBothNoneOrder()){
     // if (SingnalAlert()){
         Print("[SIGNAL-ALERT] Open Pending Orders");
         string comment  = StringFormat("S1:%d,AV=%.2f,AB=%d,TK=%d",http_status,vpamon.getAvgTick1M(),vpamon.getLastABSPip(),vpamon.getLastTickTotal());
         //string comment  = StringFormat("GRAILS,HTTP STATUS=%d",http_status);
         dualman.setComment(comment);
         dualman.openBothPending(false);         
     // }
   }
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+

void OnTick() {
   vpamon.collectOnTick();
   vpamon.CommentInfo();

  //Monitor Orders Here
  dualman.monitor(SignalCancel());
  
}

bool IsMarketOpened(){
   return MarketInfo(Symbol(),MODE_TRADEALLOWED)== 1;
}

bool SingnalAlert(){

   return (IsMarketOpened() && !SingleOrder::HighSpread() && vpamon.getAvgTick1M() > 3  && vpamon.getLastTickTotal() > vpamon.getAvgTick1M() );
}

bool SignalCancel(){

   return false;
   if (IsTesting())return false;
   //bool currentTickAvgHigher = vpamon.getPreviousTickTotal() < vpamon.getAvgTick1M();
   //return false;
  return (SingleOrder::HighSpread()|| vpamon.signalLast3SecHasZeroTick()|| vpamon.signalPressureHighAndNoMove(false)); ;
   
}
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
int sec=0;

void OnTimer() {



   vpamon.processTickDataWhenTimer();
   vpamon.CommentInfo();

   if (!SingnalAlert())
      return;
   if(!dualman.areBothNoneOrder())
      return;
   string fullURL ="http://localhost/EA/signal";
   string output;
   int res = api.get(fullURL,output);
   
   if(res> 200 && res <= 299 ){
      http_status = res;
      Print( "http status return =",res ,",output=",output);
      SetDaulPendingOrder();
   }
  

   
   
   // Must set Pending order here
  
   
}
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester() {
  //---
  double ret = 0.0;
  //---

 
  return (ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long& lparam, const double& dparam,
                  const string& sparam) {
  //---
}
//+------------------------------------------------------------------+
