/*
22-MAR-2017 - start grails EA after failed with 4plus1 and grails1 ,grails2 EA
version 3.02 - ABS pip cond
version 3.05 - ABS pip cond +  bollinger band + Pip Pressure stop
version 3.05 - ABS pip cond +  bollinger band(Enable/Disable Input)
  EURUSD , cool + No Bollinger Band + Pip Pressure stop
  GUBUSD , cool + Bollinger Band + Pip Pressure stop
version 3.06 - success with win rate , + reduce stoploss from -25 to -20
             - Add ABS(Diff) < TickTotal
             - Add Diff != 0
version 3.07 - add support buy3/sell3 algo
             - add close when signalLast3SecHasZeroTick();
version 3.08 - Change ABS(Diff) > TickTotal
version 3.09 - adding Dual Order
             - add Auto Resize Lot
        3.09.1 - Fix bug volume always 0.01
        3.09.2 - Add Handle when T/P was reached
        3.09.3 - if (count > 3) return / no open order
version 3.10
   - grails add return 22X
   - input Enable_AUTO_resize_lot
=== BIG CHANGE
version 4.01 move to new stategy
version 4.02
   +not care server directly , care only Bid price
   +add close when profit > 0 , avg_tick < 2

2017-06-20 - version 4.03
    + implement direction condition and pip between 2 price signal
2017-07-04 - version 4.03.02
    + add vpa.lastDiff() as condition too
    + add auto lot
2017-07-13 - version 4.03.03
    + add ABS{vpamon.getLastDiff()) > 1

2017-07-19 - version 4.03.04
    + check sig 2 previous sig time , rather than 1 previous sig time
2017-07-204 - version 4.03.05
    + When order is open if sig , then close order
2017-07-31 - version 4.03.06
    + time between order = 8 hours

---------------------------------------------
Big change
---------------------------------------------
2017-08-21 - version 5.01.01
   + change new stategy
   + 1) using Hedge Order Manager
   + 2) using BollingerBandSingal
   
2017-08-30 - done 
2017-08-30 - version 50102
  +apply hedge trailing 
2017-09-02 - version 50103
   + apply callVol();
2017-09-05 - version 50103
  + add HedgeOrderManager::isTooDifferenceUpperLower() , not test yet !
2017-09-13 - version 50103
   + Improve cal lot 
 ========= Start 5.02
2017-08-21 - version 5.02.01 // seems go
  + re comment and message
  + add low AvergePip , >= 1  and cond < 2.5
  
  
2017-10-16 - version 5.02.02
   + when reach max order , close when profit = -5%
2017-10-17 - version 5.02.03
   + swap direction 
2017-10-18 - version 5.02.04
   + g_max_loss_pip

*/

//+------------------------------------------------------------------+..
//|                                                    grailsEA3.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Grails EA , Agility FX Trading."
#property link "http://www.grailsEA.com"
#property version "5.0204"
#property strict

#include ".\Include\Account.mqh"
#include ".\Include\HedgeOrderManager.mqh"
#include ".\Include\DenFxUtil.mqh"
#include ".\Include\WebAPI.mqh"
#include ".\Include\VPAMonitor.mqh"
#include ".\Include\WeekTimer.mqh"
#include ".\Include\GrailsAutoLot.mqh"






WebAPI api;
VPAMonitor vpamon;
WeekTimer weektime;
//GrailsAutoLot autolot;
HedgeOrderManager hMan;
//SingleOrder order;

input int magic_number = 50204;
input int profit_pip = 100;
input int stoploss_pip = -80;
input int ABS_pip_per_sec = 12;
input bool bollinger_enable = true;
input int trailingstep = 10;
input int lot_time = 16;



int test_value = 0;

int g_max_loss_pip = 0;
//BollingerBandSingal
double g_main_price;
double g_max_order_count ;
// Avg Tick

double g_avg_tick_close_order = 2;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+

int getMaxCountOrder(){
   double temp_value = lot_time;
   int count = 0;
   
   while((temp_value=temp_value/2) >= 1 ){
      count++; 
   }
   Print("getMaxCountOrder() =",count);
   if( MathPow(2,count) != lot_time){
      Print("lot_time is 2^n , n = 1 , 2, 3 , 4..");
      ExpertRemove();
      
   }
   
   return count;
   
}
int OnInit() {

  // Show Logo


  ChartSetInteger(0, CHART_COLOR_CHART_UP, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CHART_DOWN, 0, 255);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, 0, 255);
  ChartSetInteger(0, CHART_SHOW_VOLUMES, 0, 1);
  ChartSetInteger(0, CHART_MODE, 0, 1);
  ChartSetInteger(0, CHART_SCALE, 0);
  ChartSetSymbolPeriod(0, NULL, PERIOD_M1);

  EventSetTimer(1);


  if (AccountCompany() == "Trading Point Of Financial Instruments Ltd") {
    // add 13 pip
    SingleOrder::s_high_spread += 13;

  } else if (AccountCompany() != "Tickmill Ltd") {
    // Do not thing
    MessageBox("Unknown Broker name " + AccountCompany(), "Error", MB_ICONINFORMATION);
    //ExpertRemove();

  }
  g_max_order_count = getMaxCountOrder()+2;



    
  
  SingleOrder::setMagicNo(magic_number);
  if(Symbol() != "EURUSD"){
   Print("Error : Symbol is not EURUSD");
   ExpertRemove();
  }
  calVol();
  
  //Print("Auto Volume Lot =",autolot.getVolume());
  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
  //--- destroy timer
  EventKillTimer();
// order.ClosePending();

}


//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+

void PreformOrderWhenTesting() {
  if (IsTesting()) {
  
   if(hMan.count() > 0 ) return;
   //hMan.buyUpper("test");
   //return;
   hMan.setVolume(calVol());
   if (test_value%2 == 0)
      hMan.buyUpper("test");
   else
      hMan.sellLower("test");
    
   test_value++;

  }

}




void OnTick() {

  vpamon.collectOnTick();
  vpamon.CommentInfo(magic_number);

  PreformOrderWhenTesting();

  //------------Hedging Algorithms ------------------
  hMan.monitor();
  
 

}

bool IsMarketOpened() {

  return MarketInfo(Symbol(), MODE_TRADEALLOWED) == 1;

}

bool BollingerBandSingal(double &main_price) {

  if (!bollinger_enable) return true;

  double upper_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_UPPER, 0);
  double lower_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_LOWER, 0);
  main_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_MAIN, 0);

  if (Bid > upper_price  || lower_price > Ask  ) {
    return true;
  }
  return false;
}

bool CondTradeAllowed() {

   if (hMan.count() > 0) return  false;
   
   g_avg_tick_close_order = 2 ;
   if (vpamon.getAvgTick1M() < 2.50 && vpamon.hasLowerAvg50PerCenterBefore()){
      g_avg_tick_close_order = vpamon.getAvgTick1M()*0.8;
   }

  return (
           IsMarketOpened() && !SingleOrder::HalfHighSpread()
           
           && ( vpamon.getAvgTick1M() >= 2.50 || ( vpamon.hasLowerAvg50PerCenterBefore() &&  vpamon.getAvgTick1M() > 1))
           //&& vpamon.getLastTickTotal() > vpamon.getAvgTick1M()
           //&& vpamon.getLastABSPip() >=  ABS_pip_per_sec
           && BollingerBandSingal(g_main_price)
           //&& vpamon.getLastDiff() != 0
           //&& MathAbs(vpamon.getLastDiff()) != vpamon.getLastTickTotal()
           && hMan.count() == 0
           //&& !weektime.isWeekEndGap()
           //&& (TimeCurrent() - last_opened_order_time ) > 60 * 60 * 8 // 8 hours
         );
}


//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
int sec = 0;
void MonitorOpenedOrder() {

   if(hMan.count() == 0) return;

   if ( hMan.count() == g_max_order_count && hMan.getActualProfit() > AccountBalance()*(-0.05)) {
    Print("Close order due to g_max_order_count = ",g_max_order_count,",and Profit > -5%");
    hMan.closeAll();
  }

  if ( !hMan.isTrailingActivated() && hMan.getActualProfit() > 0 && vpamon.getAvgTick1M() < g_avg_tick_close_order) {
   
    //## Saving Max_Loss_Pip of single Order 
    if (hMan.count() == 1 && (hMan.getMaxLossPip() < g_max_loss_pip)  ){
      g_max_loss_pip = hMan.getMaxLossPip();
      Print("Got New g_max_loss_pip",g_max_loss_pip);
    }
    Print("g_max_loss_pip=",g_max_loss_pip);
    Print("Close order due to ActualProfit() > 0, AvgTick < ",g_avg_tick_close_order);
    hMan.closeAll();
    
  }
  
  

}

double calVol(){
   
   double USD_Used_Margin_per_0_01_lot = Ask * 1000 / ( AccountLeverage());
   
   Print("USD_Used_Margin_per_0_01_lot=", USD_Used_Margin_per_0_01_lot,", leverage = ", AccountLeverage());
   Print(" xTime = ",lot_time,", x USD_Used_Margin_per_0_01_lot = ",lot_time*USD_Used_Margin_per_0_01_lot);
   
   double allCommission = (lot_time*0.01*3) * 4 * Ask;
   Print("All Commission = ",allCommission);
   
   
   // All of Margin =  USD_Used_Margin_per_0_01_lot x xTime + margin_when_floating + all_commission
   
   double All_Of_Margin_per_xTime_lot  = (USD_Used_Margin_per_0_01_lot*lot_time + (hMan.getRangePip()*1.2*lot_time*0.01) +allCommission );
   Print("All_Of_Margin_per_xTime_lot = ", All_Of_Margin_per_xTime_lot,", leverage = ",AccountLeverage());
   
   double start_trading_lot = 0.01* (int)AccountBalance()/All_Of_Margin_per_xTime_lot;
   Print("start_trading_lot = ",start_trading_lot);
   double maxlot = 95/(lot_time*3/2);
   if (start_trading_lot > maxlot){ // 4 = maxLot 100 / 24 
    start_trading_lot = maxlot;
    Print("Force Max start_trading_lot = ",start_trading_lot);
   }
   start_trading_lot = NormalizeDouble(start_trading_lot,2);
   
   return start_trading_lot;
  
}

void OnTimer() {

  vpamon.processTickDataWhenTimer();
  vpamon.CommentInfo(magic_number);

  MonitorOpenedOrder();
  
  
  if (!CondTradeAllowed())
    return;
   

  api.get("http://localhost/EA/signal?magic=" + IntegerToString(magic_number));

  if (api.getHttpStatus() > 200 && api.getHttpStatus() <= 299 ) {
    Print( "http=", api.getHttpStatus(), ",Bid =", Bid, ",Ask=", Ask);


    if (api.getHttpStatus() > 200) {

      int count = (api.getHttpStatus() - 200) / 10;
      /* direction
      *  1 - Price is rasing up trend - so buy
      *  2 - Price is going down trend - so sell
      */
      int direction = (api.getHttpStatus() - 200) % 10;
      string comment  = StringFormat("N1,MXP%d,D%d,V%.2f,B%d,F%d,T%d",g_max_loss_pip , direction,vpamon.getAvgTick1M(), vpamon.getLastABSPip(), vpamon.getLastDiff(), vpamon.getLastTickTotal());
      hMan.setVolume(calVol());
      if(Bid > g_main_price){
         //hMan.buyUpper(comment);
         hMan.sellLower(comment);
      }else{
         //hMan.sellLower(comment);
         hMan.buyUpper(comment);
      }
      // save last value    
      return;

    }
  }




  // Must set Pending order here


}
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester() {
  //---
  double ret = 0.0;
  //---


  return (ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long& lparam, const double& dparam,
                  const string& sparam) {
  //---
}
//+------------------------------------------------------------------+
