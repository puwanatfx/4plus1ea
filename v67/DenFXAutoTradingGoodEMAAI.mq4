//+------------------------------------------------------------------+
//|                                             DenAutoFXTrading.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, Agility FX Trading."
#property link      ""
#property version   "1.00"
#property strict



#include "..\Include\SingleOrder.mqh"
#include "..\Include\VPAMonitor.mqh"
#include "..\Include\MVAMonitor.mqh"
#include "..\Include\ResistanceSupport.mqh"


//---- Gobal ZONE
// test
SingleOrder order;
VPAMonitor monitor;
MVAMonitor  mva_monitor;
ResistanceSupport rs_floor;
int loss_count = 0;
int win_count =0;
datetime startdate;

int spread ;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit()
{
//--- create timer
 EventSetTimer(1);
 

//--- setting chart 

 ChartSetInteger(0,CHART_COLOR_CHART_UP,0,16711680);
 ChartSetInteger(0,CHART_COLOR_CHART_DOWN,0,255);
 ChartSetInteger(0,CHART_COLOR_CANDLE_BULL,0,16711680);
 ChartSetInteger(0,CHART_COLOR_CANDLE_BEAR,0,255);
 ChartSetInteger(0,CHART_SHOW_VOLUMES,0,1);
 ChartSetInteger(0,CHART_MODE,0,1);
 ChartSetInteger(0,CHART_SCALE,4);
 ChartSetSymbolPeriod(0,"USDJPY",PERIOD_M1);

 startdate = Time[0];
 spread = SingleOrder::spreadPip();




 Print("HelloDen");
 return(INIT_SUCCEEDED);




}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason)
{
//--- destroy timer
 EventKillTimer();
 

}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+


bool check_ReverseTrendWithSupportOrResistance(){


         int limitBarSum = 7; // very good both USD/EUR
         int limitPipFromFloor = 20;
         
         int support_resistan_hr = 24 ; // hr
         
         int bar0 = mva_monitor.diff10EMAPip(0,1);
         int bar1 = mva_monitor.diff10EMAPip(1,2);
         int bar2 = mva_monitor.diff10EMAPip(2,4);
         int bar3 = mva_monitor.diff10EMAPip(2,4);
         int barSum = bar1 + bar2 + bar3;
         
         if(bar0 < 0 ){
          if(bar1 >0 && bar2 > 0 && bar3 >0 && barSum > limitBarSum){
           double rs_price = rs_floor.getResistance(support_resistan_hr);
         
           int pip_from_floor =(int) (NormalizeDouble(MathAbs(Bid-rs_price),_Digits)/_Point);
           if( pip_from_floor < limitPipFromFloor) {
               order.SellNow();
               return true;
           }
         }
         
       }


       bar0 = mva_monitor.diff10EMAPip(0,1);
       bar1 = mva_monitor.diff10EMAPip(1,2);
       bar2 = mva_monitor.diff10EMAPip(2,4);
       bar3 = mva_monitor.diff10EMAPip(2,4);
       barSum = bar1 + bar2 + bar3;
       if(bar0 > 0 ){
        if(bar1 <0 && bar2 < 0 && bar3 <0 && barSum < -limitBarSum ){
         double rs_price = rs_floor.getSupport(support_resistan_hr);
         int pip_from_floor =(int) (NormalizeDouble(MathAbs(Bid-rs_price),_Digits)/_Point);
         if( pip_from_floor < limitPipFromFloor){
            order.BuyNow();
            return true;
         }
       }

     }
     return false;

}
  
bool check200EMA_With10EMA(){

   int volume = 0; // volume is not factor
   int slopePip10_200 = 50;
   int slopePip200 = 5; // 5 is good 
   
   int diffPos0 = mva_monitor.diff10EMAand200EMAPip(0);
   int diffPos1 = mva_monitor.diff10EMAand200EMAPip(1);
   int diffPos2 = mva_monitor.diff10EMAand200EMAPip(2);
   
   if (diffPos0 > slopePip10_200  && diffPos1 < slopePip10_200*0.8 && Volume[1] > volume && mva_monitor.diff200EMAPip(0,1) > slopePip200) {
      order.BuyNow();
      return true;
   }
   
   if (diffPos0 < -slopePip10_200 &&  diffPos1 > -slopePip10_200*0.8 && Volume[1] > volume &&mva_monitor.diff200EMAPip(0,1) < -slopePip200 ){
     
      order.SellNow();
      return true;
   }
      
   return false;
}


void OnTick() {  

   




  if(order.isNoneOrder()){
     
     
     order.setRtTakeProfitPip(spread*2);
     order.setRtStopLossPip(-(spread*3)); // good is 10
     order.setRtTrailingStepPip(spread*2);

     //if(check_ReverseTrendWithSupportOrResistance())
     // return;
     if(check200EMA_With10EMA())
      return;
     

  }
  
  if(order.isBuyOrder() || order.isSellOrder()){


    int profitPip = order.profitPip();
    
    if (order.isProfitOverRtPTakeProfit()){ 
       order.trailingRtStopLoss();
       Print("order.trailingRtStopLoss()");
       return;
     }
     
     if(order.isProfitLowerRtStopLoss()){ 
       if(profitPip > 0 ){
        Print("Close with Profit=",profitPip);
        win_count++;
      }else{
        Print("Close with Loss=",profitPip);
        loss_count++;
      }

      order.CloseNow();

      return;
    }


    if(order.isOverStopLoss() || order.isOverTakeProfit()) {
         // force close agian;
     loss_count++;
     order.CloseNow();
     Print("Close cause isOverStoploss() or isOverTakeProfit()" , profitPip);

   }

  // if(order.OrderDoneCount() > 200   )
  //  ExpertRemove();

  
}








}
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer()
{
//---
    //  monitor.processPressure();
}
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester()
{
//---
 double ret=0.0;
//---

//---
 Print("WinCount=",win_count);
 Print("LossCount=",loss_count);
 Print("StartDate = " ,startdate);
 Print("EndDate =",Time[0]);
 return(ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id,
  const long &lparam,
  const double &dparam,
  const string &sparam)
{
//---

}
//+------------------------------------------------------------------+
