/*
22-MAR-2017 - start grails EA after failed with 4plus1 and grails1 ,grails2 EA
version 3.02 - ABS pip cond
version 3.05 - ABS pip cond +  bollinger band + Pip Pressure stop
version 3.05 - ABS pip cond +  bollinger band(Enable/Disable Input)
  EURUSD , cool + No Bollinger Band + Pip Pressure stop
  GUBUSD , cool + Bollinger Band + Pip Pressure stop
version 3.06 - success with win rate , + reduce stoploss from -25 to -20
             - Add ABS(Diff) < TickTotal
             - Add Diff != 0
version 3.07 - add support buy3/sell3 algo
             - add close when signalLast3SecHasZeroTick();
version 3.08 - Change ABS(Diff) > TickTotal
version 3.09 - adding Dual Order
             - add Auto Resize Lot
        3.09.1 - Fix bug volume always 0.01
        3.09.2 - Add Handle when T/P was reached
        3.09.3 - if (count > 3) return / no open order
version 3.10
   - grails add return 22X
   - input Enable_AUTO_resize_lot
=== BIG CHANGE
version 4.01 move to new stategy
version 4.02
   +not care server directly , care only Bid price
   +add close when profit > 0 , avg_tick < 2

2017-06-20 - version 4.03
    + implement direction condition and pip between 2 price signal
2017-07-04 - version 4.03.02
    + add vpa.lastDiff() as condition too
    + add auto lot
2017-07-13 - version 4.03.03
    + add ABS{vpamon.getLastDiff()) > 1

2017-07-19 - version 4.03.04
    + check sig 2 previous sig time , rather than 1 previous sig time
2017-07-204 - version 4.03.05
    + When order is open if sig , then close order
2017-07-31 - version 4.03.06
    + time between order = 8 hours

---------------------------------------------
Big change
---------------------------------------------
2017-08-21 - version 5.01.01
   + change new stategy
   + 1) using Hedge Order Manager
   + 2) using BollingerBandSingal
   
2017-08-30 - done 
2017-08-30 - version 50102
  +apply hedge trailing 
2017-09-02 - version 50103
   + apply callVol();
2017-09-05 - version 50103
  + add HedgeOrderManager::isTooDifferenceUpperLower() , not test yet !
2017-09-13 - version 50103
   + Improve cal lot 
  

*/

//+------------------------------------------------------------------+..
//|                                                    grailsEA3.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Grails EA , Agility FX Trading."
#property link "http://www.grailsEA.com"
#property version "5.0102"
#property strict

#include ".\Include\Account.mqh"
#include ".\Include\HedgeOrderManager.mqh"
#include ".\Include\DenFxUtil.mqh"
#include ".\Include\WebAPI.mqh"
#include ".\Include\VPAMonitor.mqh"
#include ".\Include\WeekTimer.mqh"
#include ".\Include\GrailsAutoLot.mqh"






WebAPI api;
VPAMonitor vpamon;
WeekTimer weektime;
//GrailsAutoLot autolot;
HedgeOrderManager hMan;
//SingleOrder order;

input int magic_number = 50103;
input int profit_pip = 100;
input int stoploss_pip = -80;
input int ABS_pip_per_sec = 12;
input bool bollinger_enable = true;
input int trailingstep = 10;
input int lot_time = 16;





// Signal Timing
datetime last_sig_time = 0;
datetime last_sig_time2 = 0;
datetime last_opened_order_time = 0;
double last_bid_price = Bid;
double last_bid_price2 = Bid;
int last_direction = 0;

int test_value =0;

//BollingerBandSingal
double g_main_price;



//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {

  // Show Logo


  ChartSetInteger(0, CHART_COLOR_CHART_UP, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CHART_DOWN, 0, 255);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, 0, 255);
  ChartSetInteger(0, CHART_SHOW_VOLUMES, 0, 1);
  ChartSetInteger(0, CHART_MODE, 0, 1);
  ChartSetInteger(0, CHART_SCALE, 0);
  ChartSetSymbolPeriod(0, NULL, PERIOD_M1);

  EventSetTimer(1);


  if (AccountCompany() == "Trading Point Of Financial Instruments Ltd") {
    // add 13 pip
    SingleOrder::s_high_spread += 13;

  } else if (AccountCompany() == "Tickmill Ltd") {
    // Do not thing

  } else {
    MessageBox("Unknown Broker name " + AccountCompany(), "Error", MB_ICONINFORMATION);
    ExpertRemove();
  }
  SingleOrder::setMagicNo(magic_number);
  if(Symbol() != "EURUSD"){
   Print("Error : Symbol is not EURUSD");
   ExpertRemove();
  }
  calVol();
  
  //Print("Auto Volume Lot =",autolot.getVolume());
  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
  //--- destroy timer
  EventKillTimer();
// order.ClosePending();

}


//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+

void PreformOrderWhenTesting() {
  if (IsTesting()) {
  
   if(hMan.count() > 0 ) return;
   //hMan.buyUpper("test");
   //return;
   hMan.setVolume(calVol());
   if (test_value%2 == 0)
      hMan.buyUpper("test");
   else
      hMan.sellLower("test");
    
   test_value++;

  }

}




void OnTick() {

  vpamon.collectOnTick();
  vpamon.CommentInfo(magic_number);

  PreformOrderWhenTesting();

  //------------Hedging Algorithms ------------------
  hMan.monitor();

 

}

bool IsMarketOpened() {

  return MarketInfo(Symbol(), MODE_TRADEALLOWED) == 1;

}

bool BollingerBandSingal(double &main_price) {

  if (!bollinger_enable) return true;

  double upper_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_UPPER, 0);
  double lower_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_LOWER, 0);
  main_price = iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_MAIN, 0);

  if (Bid > upper_price  || lower_price > Ask  ) {
    return true;
  }
  return false;
}

bool CondTradeAllowed() {

  return (
           IsMarketOpened() && !SingleOrder::HalfHighSpread()
           && vpamon.getAvgTick1M() > 2.50
           && vpamon.getLastTickTotal() > vpamon.getAvgTick1M()
           && vpamon.getLastABSPip() >=  ABS_pip_per_sec
           && BollingerBandSingal(g_main_price)
           && vpamon.getLastDiff() != 0
           && MathAbs(vpamon.getLastDiff()) != vpamon.getLastTickTotal()
           && hMan.count() == 0
           && !weektime.isWeekEndGap()
           //&& (TimeCurrent() - last_opened_order_time ) > 60 * 60 * 8 // 8 hours
         );
}


//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
int sec = 0;
void MonitorOpenedOrder() {

   if(hMan.count() == 0) return;


  if ( !hMan.isTrailingActivated() && hMan.getActualProfit() > 0 && vpamon.getAvgTick1M() < 2) {
    Print("Close order due to ActualProfit() > 0, AvgTick < 2");
    hMan.closeAll();
  }

}

double calVol(){
   
   double USD_Used_Margin_per_0_01_lot = Ask * 1000 / ( AccountLeverage());
   
   Print("USD_Used_Margin_per_0_01_lot=", USD_Used_Margin_per_0_01_lot,", leverage = ", AccountLeverage());
   Print(" xTime = ",lot_time,", USD_Margin = ",lot_time*USD_Used_Margin_per_0_01_lot);
   double USD_Used_Margin_per_xTime_lot  = (USD_Used_Margin_per_0_01_lot*lot_time + hMan.getRangePip()*1.2*lot_time*0.01);
   Print("USD_Used_Margin_per_xTime_lot = ", USD_Used_Margin_per_xTime_lot,", leverage = ",AccountLeverage());
   double start_trading_lot = 0.01* (int)AccountBalance()/USD_Used_Margin_per_xTime_lot;
   Print("start_trading_lot = ",start_trading_lot);
   
   start_trading_lot = NormalizeDouble(start_trading_lot,2);
   if (start_trading_lot > 4){ // 4 = maxLot 100 / 24 
    start_trading_lot = 4;
    Print("Force Max start_trading_lot = ",start_trading_lot);
   }
   
   return start_trading_lot;
  
}

void OnTimer() {

  vpamon.processTickDataWhenTimer();
  vpamon.CommentInfo(magic_number);

  MonitorOpenedOrder();
  
  
  if (!CondTradeAllowed())
    return;


  api.get("http://localhost/EA/signal?magic=" + IntegerToString(magic_number));

  if (api.getHttpStatus() > 200 && api.getHttpStatus() <= 299 ) {
    Print( "http=", api.getHttpStatus(), ",Bid =", Bid, ",Ask=", Ask);


    if (api.getHttpStatus() > 200) {

      int count = (api.getHttpStatus() - 200) / 10;
      /* direction
      *  1 - Price is rasing up trend - so buy
      *  2 - Price is going down trend - so sell
      */
      int direction = (api.getHttpStatus() - 200) % 10;
      string comment  = StringFormat("D=%d,V=%.2f,B=%d,F=%d,T=%d",direction,vpamon.getAvgTick1M(), vpamon.getLastABSPip(), vpamon.getLastDiff(), vpamon.getLastTickTotal());
      hMan.setVolume(calVol());
      if(Bid > g_main_price){
         hMan.buyUpper(comment);
      }else{
         hMan.sellLower(comment);
      }
      // save last value    
      return;

    }
  }




  // Must set Pending order here


}
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester() {
  //---
  double ret = 0.0;
  //---


  return (ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long& lparam, const double& dparam,
                  const string& sparam) {
  //---
}
//+------------------------------------------------------------------+
