/*
22-MAR-2017 - start grails EA after failed with 4plus1 and grails1 ,grails2 EA
version 3.02 - ABS pip cond
version 3.05 - ABS pip cond +  bollinger band + Pip Pressure stop
version 3.05 - ABS pip cond +  bollinger band(Enable/Disable Input)
  EURUSD , cool + No Bollinger Band + Pip Pressure stop
  GUBUSD , cool + Bollinger Band + Pip Pressure stop
version 3.06 - success with win rate , + reduce stoploss from -25 to -20
             - Add ABS(Diff) < TickTotal
             - Add Diff != 0
version 3.07 - add support buy3/sell3 algo
             - add close when signalLast3SecHasZeroTick();
version 3.08 - Change ABS(Diff) > TickTotal
version 3.09 - adding Dual Order 
             - add Auto Resize Lot
        3.09.1 - Fix bug volume always 0.01
        3.09.2 - Add Handle when T/P was reached 
        3.09.3 - if (count > 3) return / no open order
version 3.10 
   - grails add return 22X
   - input Enable_AUTO_resize_lot
=== BIG CHANGE
version 4.01 move to new stategy
               
  
*/

//+------------------------------------------------------------------+..
//|                                                    grailsEA3.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, Grails EA , Agility FX Trading."
#property link "http://www.grailsEA.com"
#property version "4.01"
#property strict

#include ".\Include\Account.mqh"
#include ".\Include\SingleOrder.mqh"
#include ".\Include\DenFxUtil.mqh"
#include ".\Include\DualOrderManager.mqh"
#include ".\Include\WebAPI.mqh"
#include ".\Include\VPAMonitor.mqh"






WebAPI api;
VPAMonitor vpamon;
//SingleOrder order;
int http_status = 0;
input int magic_number=40101;
input int profit_pip = 100;
input int stoploss_pip = -100;
input int ABS_pip_per_sec = 12;
input bool bollinger_enable = false;
input bool auto_resize_lot_enable = true;
input int trailingstep = 10;
 


SingleOrder order;
bool trailingActivated = false;

// Signal Timing 
datetime last_sig_time = 0;
datetime last_opened_order_time =0;
double last_bid_price = Bid;
int last_direction = 0;



//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {

   // Show Logo


  ChartSetInteger(0, CHART_COLOR_CHART_UP, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CHART_DOWN, 0, 255);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, 0, 255);
  ChartSetInteger(0, CHART_SHOW_VOLUMES, 0, 1);
  ChartSetInteger(0, CHART_MODE, 0, 1);
  ChartSetInteger(0, CHART_SCALE, 3);
  ChartSetSymbolPeriod(0,NULL, PERIOD_M1);
  
  EventSetTimer(1);


  if (AccountCompany() == "Trading Point Of Financial Instruments Ltd") {
      // add 13 pip
    SingleOrder::s_high_spread += 13;
    
  } else if (AccountCompany() == "Tickmill Ltd") {
      // Do not thing
  
  } else {
    MessageBox("Unknown Broker name " + AccountCompany(), "Error",MB_ICONINFORMATION);
    ExpertRemove();
  }
  SingleOrder::setMagicNo(magic_number);
  Print("Test=",(TimeCurrent()- last_opened_order_time )> 15*60);
  
  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
  //--- destroy timer
  EventKillTimer();
 // order.ClosePending();

}


//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+

void OnTick() {
  
  
  
    
  if (IsTesting()){
   
      if (order.isNoneOrder()) {
         order.SellNow("TESTMODE");
         order.modifyOpenedOrder(stoploss_pip-5,1000);
         //Print("ActualProfit",order.getActualProfit());
         //order.CloseNow();
         //ExpertRemove();
         trailingActivated = false;
         order.setRtStopLossPip(profit_pip-trailingstep*2);
         order.setRtTakeProfitPip(profit_pip);
         order.setRtTrailingStepPip(trailingstep);
         return;
      }
      
    
   
      
  }

   vpamon.collectOnTick();
   vpamon.CommentInfo(magic_number);
   
   
   if(order.isOpenedOrder()){
      int pip = order.profitPip() ;
      //Print("PIP =",pip);
      
      if(order.isProfitOverRtTakeProfit()){
         order.trailingRtStopLoss();
         trailingActivated = true;
         Print("trailing stop loss is activated , pip = ",pip);
      }
      if( trailingActivated && order.isProfitLowerRtStopLoss()){
        order.CloseNow();
        return;
      }
      
      if(order.profitPip() < stoploss_pip){
         Print("Close order due to profit=",pip," < stoploss_pip=",stoploss_pip);
         order.CloseNow();
         return;
      }
      return;

   }

}

bool IsMarketOpened(){
   return MarketInfo(Symbol(),MODE_TRADEALLOWED)== 1;
}

bool BollingerBandSingal(){

   if (!bollinger_enable) return true;
  
   double upper_price = iBands(NULL,PERIOD_M1,4,1,0,PRICE_MEDIAN,MODE_UPPER,0);
   double lower_price = iBands(NULL,PERIOD_M1,4,1,0,PRICE_MEDIAN,MODE_LOWER,0);
   
   if (Bid > upper_price  || Ask  < lower_price ){
      return true;
   }
   return false;
}

bool CondTradeAllowed(){

   return (  
            IsMarketOpened() && !SingleOrder::HighSpread() 
            && vpamon.getAvgTick1M() > 2.50  
            && vpamon.getLastTickTotal() > vpamon.getAvgTick1M() 
            && vpamon.getLastABSPip() >=  ABS_pip_per_sec
            && BollingerBandSingal()
            && vpamon.getLastDiff() != 0
            && MathAbs(vpamon.getLastDiff()) != vpamon.getLastTickTotal()
            && order.isNoneOrder()
            && (TimeCurrent()- last_opened_order_time )> 15*60
         );
}


//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
int sec=0;



void OnTimer() {
   
   vpamon.processTickDataWhenTimer();
   vpamon.CommentInfo(magic_number);
   
   if (!CondTradeAllowed())
      return;   

   string output;
   int res = api.get("http://localhost/EA/signal?magic="+IntegerToString(magic_number),output);
   
   if(res> 200 && res <= 299 ){
      http_status = res;
      //Print( "http status return =",res ,",output=",output);
      Print( "http status return =",res ,",output=",output,",Bid =",Bid,",Ask=",Ask);
      
      if(res > 200){ 
      
         int count = (res-200)/10;
         if (count >3 ) return; 
         int direction = (res-200)%10; 
         //Timing Condition
         
         datetime sig_time = TimeCurrent();
         
         if ( (sig_time - last_sig_time )  < 180 
            && ( last_direction == direction )){
            string comment  = StringFormat("S=%d.%d,V=%.2f,B=%d,F=%d,T=%d",count,direction,vpamon.getAvgTick1M(),vpamon.getLastABSPip(),vpamon.getLastDiff(),vpamon.getLastTickTotal());
            
            
            if (direction == 1 && Bid > last_bid_price) order.BuyNow(comment);
            if (direction == 2 && Bid < last_bid_price) order.SellNow(comment);
            
            if(order.isOpenedOrder()){
               last_opened_order_time = TimeCurrent();
               order.modifyOpenedOrder(stoploss_pip-5,1000);
               trailingActivated = false;
               order.setRtStopLossPip(profit_pip-trailingstep*2);
               order.setRtTakeProfitPip(profit_pip);
               order.setRtTrailingStepPip(trailingstep);  
             }
            
         }
         // save last value
         last_bid_price = Bid;
         last_sig_time = sig_time;
         last_direction = direction;
         return;
         
      }      
   }
  

   
   
   // Must set Pending order here
  
   
}
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester() {
  //---
  double ret = 0.0;
  //---

 
  return (ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long& lparam, const double& dparam,
                  const string& sparam) {
  //---
}
//+------------------------------------------------------------------+
