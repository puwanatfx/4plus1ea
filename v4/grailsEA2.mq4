/*
26-NOV-2016 - first version

*/

//+------------------------------------------------------------------+..
//|                                             DenAutoFXTrading.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, ManowUK EA , Agility FX Trading."
#property link "http://www.4plus1ea.com"
#property version "1.00"
#property strict

#include ".\Include\Account.mqh"
#include ".\Include\SingleOrder.mqh"
#include ".\Include\DenFxUtil.mqh"
#include ".\Include\DualOrderManager.mqh"
#include ".\Include\WebAPI.mqh"
#include ".\Include\VPAMonitor.mqh"






WebAPI api;
VPAMonitor vpamon;
SingleOrder order;
input int ABS_pip_per_sec = 15;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+
int OnInit() {

   // Show Logo


  ChartSetInteger(0, CHART_COLOR_CHART_UP, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CHART_DOWN, 0, 255);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, 0, 255);
  ChartSetInteger(0, CHART_SHOW_VOLUMES, 0, 1);
  ChartSetInteger(0, CHART_MODE, 0, 1);
  ChartSetInteger(0, CHART_SCALE, 3);
  ChartSetSymbolPeriod(0,NULL, PERIOD_M1);
  
  EventSetTimer(1);



  // Checking Broker

  //"Trading Point Of Financial Instruments Ltd" = XM
  //"Tickmill Ltd" == TICK MILL



  if (AccountCompany() == "Trading Point Of Financial Instruments Ltd") {
      // add 13 pip
    SingleOrder::s_high_spread += 13;
    
  } else if (AccountCompany() == "Tickmill Ltd") {
      // Do not thing
  
  } else {
    MessageBox("Unknown Broker name " + AccountCompany(), "Error",MB_ICONINFORMATION);
    ExpertRemove();
  }
  SingleOrder::setMagicNo(20001);
  
  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
  //--- destroy timer
  EventKillTimer();
 // order.ClosePending();

}
int http_status = 0;

//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+
bool set_new_sl_pf= false;

void OnTick() {

   vpamon.collectOnTick();
   vpamon.CommentInfo();
      
   if (order.isPendingOrder()){
   
      int distance_pip = order.getDistancePipPending();   
      
      if (order.isBuyStopOrder()){
         int diff  = distance_pip - 5;
         if(diff > 0)
            order.modifyPendingOrder(-diff,-5,0);   
         return;
      }
      
      if(order.isSellStopOrder()){
         int diff = distance_pip + 5;
         if(diff< 0)
            order.modifyPendingOrder(-diff,-5,0);   
      }
      
   }
   
   if(order.isOpenedOrder() && !set_new_sl_pf ){
      order.setSVStopLossPip(-5);
      order.setSVTakeProfitPip(5);
      order.setSVTrailingSetpPip(5);   
      set_new_sl_pf = true;
   }
   
   if(order.isOpenedOrder() && set_new_sl_pf){
      if (order.isProfitOverSVTakeProfit()) {
      order.trailingSVStopLoss();
      Print("[ACTIVE].trailingSVStopLoss() executed");
      return;
      }
   }
   

}

bool IsMarketOpened(){
   return MarketInfo(Symbol(),MODE_TRADEALLOWED)== 1;
}

bool SingnalAlert(){

   return (IsMarketOpened() && !SingleOrder::HighSpread() && vpamon.getAvgTick1M() > 3  && vpamon.getLastTickTotal() > vpamon.getAvgTick1M() );
}
bool CondTradeAllowed(){

   return (  IsMarketOpened() && !SingleOrder::HighSpread() && vpamon.getAvgTick1M() > 3  
   && vpamon.getLastTickTotal() > vpamon.getAvgTick1M() && vpamon.getLastABSPip() >=  ABS_pip_per_sec);
}

bool SignalCancel(){

   return false;
   if (IsTesting())return false;
   //bool currentTickAvgHigher = vpamon.getPreviousTickTotal() < vpamon.getAvgTick1M();
   //return false;
  return (SingleOrder::HighSpread()|| vpamon.signalLast3SecHasZeroTick()|| vpamon.signalPressureHighAndNoMove(false)); ;
   
}
//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
int sec=0;

void OnTimer() {


   
   vpamon.processTickDataWhenTimer();
   vpamon.CommentInfo();
    // TEST   
   //SetDaulPendingOrder();
   // End TesT
   if (!CondTradeAllowed())
      return;
   
   if (!order.isNoneOrder())
      return;
  
   string fullURL ="http://localhost/EA/signal";
   string output;
   int res = api.get(fullURL,output);
   
   if(res> 200 && res <= 299 ){
      http_status = res;
      Print( "http status return =",res ,",output=",output);
      
      
      if (order.isNoneOrder() && res > 200){

         string comment  = StringFormat("V2:%d,V=%.2f,B=%d,F=%d,T=%d",http_status-200,vpamon.getAvgTick1M(),vpamon.getLastABSPip(),vpamon.getLastDiff(),vpamon.getLastTickTotal());
         order.setComment(comment);
         if (res == 202) order.BuyPending(5,-5,200);
         if (res == 201) order.SellPending(-5,-5,200);
         set_new_sl_pf = false;
         return;
      }
 
      
   }
  

   
   
   // Must set Pending order here
  
   
}
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester() {
  //---
  double ret = 0.0;
  //---

 
  return (ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long& lparam, const double& dparam,
                  const string& sparam) {
  //---
}
//+------------------------------------------------------------------+
