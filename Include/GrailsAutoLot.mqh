//+------------------------------------------------------------------+
//|                                                GrailsAutoLot.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class GrailsAutoLot
{
private:
  double m_volume;
  int m_balance_per_centi_lot; // 1000 USD per 1.00 lot , or 10 per 0.01 lot

public:
  GrailsAutoLot();
  ~GrailsAutoLot();
  void calVolume();
  double getVolume();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
GrailsAutoLot::GrailsAutoLot()
{
  m_balance_per_centi_lot = 10;
  calVolume();

}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
GrailsAutoLot::~GrailsAutoLot()
{
}

void GrailsAutoLot::calVolume() {
  int factor  = (int)(AccountBalance() / m_balance_per_centi_lot);
  m_volume = 0.01 * factor;
  Print("GrailsAutoLot::calVolume() == ", m_volume);
  if(m_volume > 100){
    m_volume = 100;
    Print("GrailsAutoLot::reset calVolume() == ", m_volume);
  } 
  
}

double GrailsAutoLot::getVolume() {
  return m_volume;

}

//+------------------------------------------------------------------+
