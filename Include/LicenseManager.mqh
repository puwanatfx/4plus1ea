//+------------------------------------------------------------------+
//|                                               LicenseManager.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link "https://www.mql5.com"
#property version "1.00"
#property strict
#include "DenFXUtil.mqh"

#define LICENSE_SIZE 100
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class LicenseManager {
  struct license_struct {
    bool real;
    int account_number;
    string email;
    datetime created_dt;
  };

 private:
  license_struct m_acc[LICENSE_SIZE];
  int m_pos;
  void addAccount(bool, int, string, datetime);
  void addRealAccount(int, string, datetime);
  void addDemoAccount(int, string, datetime);
  // Test

 public:
  LicenseManager();
  ~LicenseManager();
  bool verify(bool demo, int acc_no);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
LicenseManager::LicenseManager() {
  m_pos = 0;
  //#DEMO TEST ACCOUNT

  addDemoAccount(2089264658,"puwanat.fx@gmail.com", D'2017/01/5 00:00:00');
  addDemoAccount(2089300630,"puwanat.fx@gmail.com",D'2017/01/07 00:00:00');
  //#DEMO ACCOUNT SEASION 1
  addDemoAccount(2089320325,"puwanat.fx@gmail.com", D'2017/01/5 00:00:00');

  //#CUSTOMER REAL ACCOUNT

  //#CUSTOMER DEMO ACCOUNT
  
  //#customer betuboxem@yahoo.com 
  addDemoAccount(2089328734,"betuboxem@yahoo.com", D'2017/02/03 00:00:00');
}

//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
LicenseManager::~LicenseManager() {}
bool LicenseManager::verify(bool demo, int acc_no) {
  for (int i = 0; i < m_pos; i++) {
    bool check_type = (m_acc[i].real == !demo);
    bool check_acc_no = (m_acc[i].account_number == acc_no);
    if (check_type && check_acc_no) return true;
  }
  return false;
}
void LicenseManager::addAccount(bool real, int acc_no, string email,
                                datetime created_dt) {
  ASSERT(m_pos < LICENSE_SIZE, "m_pos excess LICENSE_SIZE");
  m_acc[m_pos].real = real;
  m_acc[m_pos].account_number = acc_no;
  m_acc[m_pos].email = email;
  m_acc[m_pos].created_dt = created_dt;
  m_pos++;
}

void LicenseManager::addRealAccount(int acc_no, string email,
                                    datetime created_dt) {
  addAccount(true, acc_no, email, created_dt);
}

void LicenseManager::addDemoAccount(int acc_no, string email,
                                    datetime created_dt) {
  addAccount(false, acc_no, email, created_dt);
}
//+------------------------------------------------------------------+
