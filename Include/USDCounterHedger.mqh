//+------------------------------------------------------------------+
//|                                             USDCounterHedger.mqh |
//|                        Copyright 2017, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2017, MetaQuotes Software Corp."
#property link "https://www.mql5.com"
#property version "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class USDCounterHedger {
  enum currency_type_enum {
    no_usd,
    usd_base,
    usd_counter,
  };

 private:
  void load();
  int m_count_buy_negative_order;
  int m_count_sell_negative_order;
  int m_count_buy_RW_order;
  int m_count_sell_RW_order;
  int m_count_buy;
  int m_count_sell;

 public:
  USDCounterHedger();
  ~USDCounterHedger();
  int buyNegativeOrderCount() { return m_count_buy_negative_order; }
  int sellNegativeOrderCount() { return m_count_sell_negative_order; }
  int buyCount(){ return m_count_buy;}
  int sellCount() { return m_count_sell;}
  int buyRWCount() { return m_count_buy_RW_order;}
  int sellRWCount() { return m_count_sell_RW_order;}
  
  bool hasRW(string);
  static currency_type_enum currencyType(string);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
USDCounterHedger::USDCounterHedger() {
  
  m_count_buy_negative_order = 0;
  m_count_sell_negative_order = 0;
  m_count_buy_RW_order = 0;
  m_count_sell_RW_order = 0;
  m_count_buy = 0;
  m_count_sell= 0;

  load();
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
USDCounterHedger::~USDCounterHedger() {}
//+------------------------------------------------------------------+

currency_type_enum USDCounterHedger::currencyType(string currencySymbol) {
  if (StringFind(currencySymbol, "USD") == 0) return usd_base;
  if (StringFind(currencySymbol, "USD") == 3) return usd_counter;
  return no_usd;
}

bool USDCounterHedger::hasRW(string comment) {
  return (StringFind(comment, "RW") >= 0);
}
void USDCounterHedger::load() {

  currency_type_enum symbol_currency_type = currencyType(Symbol());
  if (symbol_currency_type == no_usd) return;
  int total = OrdersTotal();

  for (int pos = 0; pos < total; pos++) {
    if (OrderSelect(pos, SELECT_BY_POS)) {
      currency_type_enum order_currency_type = currencyType(OrderSymbol());

      if (OrderType() == OP_BUY &&
          symbol_currency_type == order_currency_type) {
        m_count_buy++;
        if (OrderProfit() < 0) {
          m_count_buy_negative_order++;
          if (hasRW(OrderComment())) m_count_buy_RW_order++;
        }
      }
      if (OrderType() == OP_BUY &&
          symbol_currency_type != order_currency_type) {
        m_count_sell++;
        if (OrderProfit() < 0) {
          m_count_sell_negative_order++;
          if (hasRW(OrderComment())) m_count_sell_RW_order++;
        }
      }
      if (OrderType() == OP_SELL &&
          symbol_currency_type == order_currency_type) {
        m_count_sell++;
        if (OrderProfit() < 0) {
          m_count_sell_negative_order++;
          if (hasRW(OrderComment())) m_count_sell_RW_order++;
        }
      }
      if (OrderType() == OP_SELL &&
          symbol_currency_type != order_currency_type) {
        m_count_buy++;
        if (OrderProfit() < 0) {
          m_count_buy_negative_order++;
          if (hasRW(OrderComment())) m_count_buy_RW_order++;
        }
      }
    }
  }
}