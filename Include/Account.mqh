//+------------------------------------------------------------------+
//|                                                      Account.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link "https://www.mql5.com"
#property version "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Account {
 private:
 public:
  Account();
  ~Account();
  static bool FreeMarginLowerBalance(int percent);
  static double TradableBalance();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Account::Account() {}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Account::~Account() {}
//+------------------------------------------------------------------+
bool Account::FreeMarginLowerBalance(int percent){
   return (AccountFreeMargin() < (AccountBalance()*percent/100));

}

double Account::TradableBalance(){
	if ( AccountBalance() <= 1500 )
		return AccountBalance();
	return MathMin(AccountFreeMargin(),AccountBalance()-AccountMargin());
}