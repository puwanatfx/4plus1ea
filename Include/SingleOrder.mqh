//+------------------------------------------------------------------+
//|                                                  SingleOrder.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link "https://www.mql5.com"
#property strict
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+
#include <stdlib.mqh>
#include "DenFxUtil.mqh"
bool RetryOnError(int pErrorCode) {
  // Retry on these error codes
  switch (pErrorCode) {
  case ERR_BROKER_BUSY:
  case ERR_COMMON_ERROR:
  case ERR_NO_ERROR:
  case ERR_NO_CONNECTION:
  case ERR_NO_RESULT:
  case ERR_SERVER_BUSY:
  case ERR_NOT_ENOUGH_RIGHTS:
  case ERR_MALFUNCTIONAL_TRADE:
  case ERR_TRADE_CONTEXT_BUSY:
  case ERR_TRADE_TIMEOUT:
  case ERR_REQUOTE:
  case ERR_TOO_MANY_REQUESTS:
  case ERR_OFF_QUOTES:
  case ERR_PRICE_CHANGED:
  case ERR_TOO_FREQUENT_REQUESTS:
  case ERR_MARKET_CLOSED:

    return (true);
  }

  return (false);
}

string OrderTypeToString(int pType) {
  string orderType;
  if (pType == OP_BUY)
    orderType = "Buy";
  else if (pType == OP_SELL)
    orderType = "Sell";
  else if (pType == OP_BUYSTOP)
    orderType = "Buy stop";
  else if (pType == OP_BUYLIMIT)
    orderType = "Buy limit";
  else if (pType == OP_SELLSTOP)
    orderType = "Sell stop";
  else if (pType == OP_SELLLIMIT)
    orderType = "Sell limit";
  else
    orderType = "Invalid order type";
  return (orderType);
}
//+--------------------TRADE
//CLASS----------------------------------------------+

enum enum_order_type {
  none,
  buy,
  sell,
  buystop,
  buylimit,
  sellstop,
  selllimit,
  closed
};
#define MAX_RETRIES 3
#define RETRY_DELAY 100  // ms

class SingleOrder {
public:
  SingleOrder();
  ~SingleOrder();
  static int spreadPip();
  static void setSlippage(int s) {s_slippage = s;};
  static void setMagicNo(int m) { s_magic_number = m;};
  static bool HighSpread();
  static bool HalfHighSpread();
  static int diffPip(double price1, double price2);
  static int s_magic_number;
  static int s_high_spread;


  int getRtStopLoss() { return m_rt_stoploss_pip; };
  int getRtTakProfit() { return m_rt_takeprofit_pip; };
  int getRtTrailingCount(){ return m_rt_trailing_count;};

private:
  static int s_slippage;
  int m_order_done;
  int m_stoploss_pip;
  int m_takeprofit_pip;
  double m_openPrice;
  double m_stoplossPrice;
  double m_takeprofitPrice;
  datetime m_order_iTime;
  datetime m_order_openTime;
  datetime m_order_pendingTime;

  // Real Time (Internal EA) Stoploss and Take profit

  int m_rt_stoploss_pip;    // realtime stop loss
  int m_rt_takeprofit_pip;  // realtime take profit
  int m_rt_trailing_step_pip;
  int m_rt_adaptive_stoloss_pip;
  int m_rt_trailing_count;

  int m_previous_pip;
  int m_current_pip;

  // Server (SV) Stoploss and Take profit

  int m_sv_stoploss_pip;
  int m_sv_takeprofit_pip;
  int m_sv_trailing_step_pip;


  string m_time_stamp;
  int m_ticket;
  double m_volume;
  string m_comment;

  enum_order_type m_order_type;
  void reset();
  void resetRT();
  void resetSV();
  int OpenMarketOrder(string pSymbol, int pType, double pVolume,
                      string pComment, color pArrow);
  bool CloseMarketOrder(int pTicket, double pVolume = 0.000000,
                        color pArrow = 255);

  void OpenNow(int op);
  int OpenPendingOrder(string pSymbol, int pType, double pVolume, double pPrice,
                       double pStop, double pProfit, string pComment,
                       datetime pExpiration, color pArrow);
  bool DeletePendingOrder(int pTicket, color pArrow = 255);
  void OpenPending(int op, int diff, int sl, int tp);




public:
  bool modifyOpenedOrder(int sl_pip , int tp_pip);



  void setVolume(double volume) { m_volume = volume; }
  double getVolume() { return m_volume; };
  int getTicket() { return m_ticket;};
  int getOpenTimeSec();
  int getPendingTimeSec();
  void BuyNow(string comment);
  void SellNow(string comment);
  void CloseNow();
  void BuyPending(int, int, int);
  void SellPending(int, int, int);
  void ClosePending();

  bool isSellOrder();
  bool isBuyOrder();
  bool isBuyStopOrder();
  bool isBuyLimitOrder();
  bool isSellStopOrder();
  bool isSellLimitOrder();
  bool isNoneOrder();
  bool isPendingOrder();
  bool isOpenedOrder();
  void setComment(string cm) { m_comment = cm;}
  double openPrice();
  int profitPip();
  int OrderDoneCount();

  // Legacy  Stoploss and takeprofit
  bool isOverStopLoss();
  bool isOverTakeProfit();

  // RealTime (Internal EA) stoploss and takeprofit

  void setRtTakeProfitPip(int pip);
  void setRtStopLossPip(int pip);
  void setRtTrailingStepPip(int pip);
  int trailingRtStopLoss();
  bool isProfitLowerRtStopLoss();
  bool isProfitOverRtTakeProfit();

  void increaseAdaptiveStoploss(int inc);

  // Server(SV) (Broker EA) Stoploss and take profit

  void trailingSVStopLoss();
  bool setSVStopLossPip(int pip);
  void setSVTrailingSetpPip(int pip);
  void setSVTakeProfitPip(int pip);
  bool isProfitOverSVTakeProfit(void);

  // Support Pending Order /Grails EA-2

  int getDistancePipPending();
  bool modifyPendingOrder(int pip_price , int sl_pip, int tp_pip);

  // -----
  double getProfit();
  double getCommission();
  double getActualProfit();
  int getBarPos();


  void updateOrderType();


};
int SingleOrder::s_magic_number = 505050;
int SingleOrder::s_slippage = 10;
int SingleOrder::s_high_spread = 20;

SingleOrder::SingleOrder(void) {
  m_time_stamp = TimeToStr(TimeCurrent(), TIME_DATE | TIME_SECONDS);

  m_order_done = 0;
  reset();
}
SingleOrder::~SingleOrder(void) {}
int SingleOrder::spreadPip(void) {
  return (int)(NormalizeDouble(Ask - Bid, _Digits) / _Point);
}
bool SingleOrder::HighSpread(void) {
  if (Symbol() == "GBPUSD")
    return spreadPip() > (s_high_spread + 5);
  else
    return spreadPip() > s_high_spread;
}
bool SingleOrder::HalfHighSpread(void) {
  if (Symbol() == "GBPUSD")
    return spreadPip() > (double)(s_high_spread + 5) / 2;
  else
    return spreadPip() > (double)(s_high_spread) / 2;
}

int SingleOrder::getOpenTimeSec() {

  if (m_order_openTime == 0 && (this.isOpenedOrder() ) ) {

    //possible pending order , then convert to open order

    bool selected = OrderSelect(m_ticket, SELECT_BY_TICKET);

    if (!selected) {
      Alert("Can not OrderSelect on SingleOrder::getOpenTimeSec() with");
      reset();
      return 0 ;
    }

    m_order_openTime = OrderOpenTime();
  }
  return (int)(TimeCurrent() - m_order_openTime);
};

int SingleOrder::getPendingTimeSec() {

  if (m_order_pendingTime == 0 && ( this.isPendingOrder() ) ) {

    //possible pending order , then convert to open order

    bool selected = OrderSelect(m_ticket, SELECT_BY_TICKET);

    if (!selected) {
      Alert("Can not OrderSelect on SingleOrder::getPendingTimeSec() with");
      reset();
      return 0 ;
    }

    m_order_pendingTime = OrderOpenTime();
  }

  return (int)(TimeCurrent() - m_order_pendingTime);
};


int SingleOrder::diffPip(double price1, double price2) {

  double diffprice = price1 - price2 ;
  int res = (int)(NormalizeDouble(diffprice, _Digits) / _Point);
  return res;

}
int SingleOrder::getBarPos(void) {
  if (m_order_iTime == D'1999.01.01') return -1;

  for (int i = 0; i <= 10000; i++)
    if (m_order_iTime == iTime(Symbol(), PERIOD_CURRENT, i)) return i;

  return 999;
}
double SingleOrder::getProfit(void) {

  if (m_ticket == -1 || m_order_type == none) return 0;

  bool selected = OrderSelect(m_ticket, SELECT_BY_TICKET);

  if (!selected) {
    Print("Error in SingleOrder::getProfit(),", GetLastError());
    return 0;
  }

  return OrderProfit();
}

double SingleOrder::getCommission(void) {

  if (m_ticket == -1 || m_order_type == none) return 0;

  bool selected = OrderSelect(m_ticket, SELECT_BY_TICKET);

  if (!selected) {
    Print("Error in SingleOrder::getCommission(),", GetLastError());
    return 0;
  }

  return OrderCommission();

}

double SingleOrder::getActualProfit(void) {
  if (m_ticket == -1 || m_order_type == none) return 0;
  bool selected = OrderSelect(m_ticket, SELECT_BY_TICKET);

  if (!selected) {
    Print("Error in SingleOrder::getactualProfit(),", GetLastError());
    return 0;
  }
  //Print("OrderCommission==",OrderCommission());
  // OrderCommission is nagative value
  double actualProfit = OrderProfit() + OrderCommission();

  if (OrderSwap() < 0)
    actualProfit = actualProfit + OrderSwap();

  return actualProfit;
}

void SingleOrder::updateOrderType(void) {

  if (m_ticket == -1) {
    reset();
    return;
  }

  bool selected = OrderSelect(m_ticket, SELECT_BY_TICKET);
  if (!selected || OrderCloseTime() != 0) {
    //possible order was clsoed
    reset();
    return;
  }

  int order_type = OrderType();
  m_openPrice = OrderOpenPrice();

  if (order_type == OP_BUY) {
    m_order_type = buy;
    return ;
  }

  if (order_type == OP_SELL) {
    m_order_type = sell;
    return ;
  }

  if (order_type == OP_SELLLIMIT) {
    m_order_type = selllimit;
    return;
  }
  if (order_type == OP_SELLSTOP) {
    m_order_type = sellstop;
    return;
  }
  if (order_type == OP_BUYLIMIT) {
    m_order_type = buylimit;
    return;
  }
  if (order_type == OP_BUYSTOP) {
    m_order_type = buystop;
    return;
  }


}
bool SingleOrder::isOpenedOrder(void) {

  bool res = this.isBuyOrder() || this.isSellOrder();
  return res;
}

bool SingleOrder::isPendingOrder(void) {

  bool res = this.isBuyLimitOrder() || this.isBuyStopOrder() || this.isSellLimitOrder() || this.isSellStopOrder();
  return res;
}
int SingleOrder::OrderDoneCount() { return m_order_done; }

void SingleOrder::resetRT(void) {
  // RealTime StopLoss and Take Profit
  m_rt_stoploss_pip = 0;
  m_rt_takeprofit_pip = 0;
  m_rt_trailing_step_pip = 0;
  m_rt_adaptive_stoloss_pip = 0;
  m_rt_trailing_count = 0;
}
void SingleOrder::resetSV(void) {
  m_sv_stoploss_pip = 0;
}
void SingleOrder::reset(void) {
  m_ticket = -1;
  m_stoploss_pip = 0;    // min == 40;
  m_takeprofit_pip = 0;  // min == 40;
  m_order_type = none;
  m_volume = 0.01;
  m_openPrice = 0;
  m_comment = "";
  m_order_iTime = D'1999.01.01';
  m_order_openTime = 0;
  m_order_pendingTime = 0;
  m_previous_pip = 0;
  m_current_pip = 0;
  resetRT();
  resetSV();
}

bool SingleOrder::isProfitOverSVTakeProfit(void) {
  return (profitPip() > m_sv_takeprofit_pip);
}
void SingleOrder::trailingSVStopLoss() {


  // no need to increase m_sv_stoploss_pip , because using current bid/ask price  , not open price
  if (!setSVStopLossPip( m_sv_stoploss_pip))
    return;

  m_sv_takeprofit_pip = m_sv_takeprofit_pip + m_sv_trailing_step_pip;
}
void SingleOrder::setSVTakeProfitPip(int pip) {
  m_sv_takeprofit_pip = pip;
}
bool SingleOrder::setSVStopLossPip(int pip) {

  //ASSERT(m_order_type == buy || m_order_type == sell,"Order Type is not opended type");
  m_sv_stoploss_pip = pip;
  return modifyOpenedOrder(m_sv_stoploss_pip, 0);


}
void SingleOrder::setSVTrailingSetpPip(int pip) {
  m_sv_trailing_step_pip = pip;
}

bool SingleOrder::modifyOpenedOrder( int sl_pip , int tp_pip) {

  //ASSERT( sl_pip <= 0 ," STOPLOSS PIP NOT < 0");
  //ASSERT( tp_pip >= 0 ," STOPLOSS PIP NOT < 0");

  //m_openPrice = m_openPrice + (price_pip * _Point);
  //OrderSelect(m_ticket);

  double bidPrice = MarketInfo(Symbol(), MODE_BID);
  double askPrice = MarketInfo(Symbol(), MODE_ASK);
  //Print("SL PIP =",sl_pip);

  if (m_order_type == buy || m_order_type == buystop || m_order_type == buylimit) {
    //    BUY OPEN  => ASK PRICE
    //==> BUY CLOSE => BID PRICE
    //Print("Cal as BUY");
    m_stoplossPrice = sl_pip == 0 ? 0 : NormalizeDouble(bidPrice + (sl_pip * _Point), _Digits);
    m_takeprofitPrice = tp_pip == 0 ? 0 : NormalizeDouble(bidPrice + (tp_pip * _Point), _Digits);
    //Print("modifyOpenedOrder() : bidPrice =  ",bidPrice);
  }

  if (m_order_type == sell || m_order_type == sellstop || m_order_type == selllimit) {
    //    SELL OPEN  => BID PRICE
    //==> SELL CLOSE => ASK PRICE
    //  Print("modifyOpenedOrder() : askPrice =  ",askPrice);

    m_stoplossPrice =  sl_pip == 0 ? 0 : NormalizeDouble(askPrice  - (sl_pip * _Point), _Digits);
    m_takeprofitPrice = tp_pip == 0 ? 0 : NormalizeDouble(askPrice - (tp_pip * _Point), _Digits);
  }
  double open_price = m_openPrice;

  if ( m_order_type == buy || m_order_type == sell)
    open_price = 0;

  bool res = OrderModify(m_ticket, open_price, m_stoplossPrice, m_takeprofitPrice, NULL, clrGreen);

  if (!res) {

    int errorCode = GetLastError();
    string errorDesc = ErrorDescription(errorCode);

    // no eorror , trading condition not changed
    if (errorCode == 1)
      return true;
    // possible order was closed before modify
    if (this.isNoneOrder()) {
      Print("Warning:modifyOpenedOrder(), Order was closed before");
      return false;
    }
    Print("Error on modifyOpenedOrder():", errorCode, ",", errorDesc);
    //ExpertRemove();
  }
  return res;
}

bool SingleOrder::modifyPendingOrder(int price_pip , int sl_pip, int tp_pip) {

  //ASSERT( sl_pip <= 0 ," STOPLOSS PIP NOT < 0");
  //ASSERT( tp_pip >= 0 ," STOPLOSS PIP NOT < 0");

  double new_price =  m_openPrice + (price_pip * _Point);



  double bidPrice = MarketInfo(Symbol(), MODE_BID);
  double askPrice = MarketInfo(Symbol(), MODE_ASK);
  //Print("SL PIP =",sl_pip);

  if (m_order_type == buy || m_order_type == buystop || m_order_type == buylimit) {
    //    BUY OPEN  => ASK PRICE
    //==> BUY CLOSE => BID PRICE
    //Print("Cal as BUY");
    m_stoplossPrice = sl_pip == 0 ? 0 : NormalizeDouble(new_price + (sl_pip * _Point), _Digits);
    m_takeprofitPrice = tp_pip == 0 ? 0 : NormalizeDouble(new_price + (tp_pip * _Point), _Digits);
    //Print("modifyOpenedOrder() : bidPrice =  ",bidPrice);
  }

  if (m_order_type == sell || m_order_type == sellstop || m_order_type == selllimit) {
    //    SELL OPEN  => BID PRICE
    //==> SELL CLOSE => ASK PRICE
    //  Print("modifyOpenedOrder() : askPrice =  ",askPrice);

    m_stoplossPrice =  sl_pip == 0 ? 0 : NormalizeDouble(new_price  - (sl_pip * _Point), _Digits);
    m_takeprofitPrice = tp_pip == 0 ? 0 : NormalizeDouble(new_price - (tp_pip * _Point), _Digits);
  }
  //double open_price = m_openPrice;

  if ( m_order_type == buy || m_order_type == sell)
    new_price = 0;

  bool res = OrderModify(m_ticket, new_price, m_stoplossPrice, m_takeprofitPrice, NULL, clrGreen);

  if (!res) {

    int errorCode = GetLastError();
    string errorDesc = ErrorDescription(errorCode);

    // no eorror , trading condition not changed
    if (errorCode == 1)
      return true;
    // possible order was closed before modify
    if (this.isNoneOrder()) {
      Print("Warning:modifyPendingOrder(), Order was closed before");
      return false;
    }
    Print("Error on modifyPendingOrder():", errorCode, ",", errorDesc);
    //ExpertRemove();
  }
  this.updateOrderType();
  return res;
}
int SingleOrder::getDistancePipPending(void) {

  double bidPrice = MarketInfo(Symbol(), MODE_BID);
  double askPrice = MarketInfo(Symbol(), MODE_ASK);

  int distance_pip = 0;
  if (m_order_type == buystop || m_order_type == buylimit) {
    //==> BUY OPEN  => ASK PRICE
    //    BUY CLOSE => BID PRICE
    distance_pip = (int)(NormalizeDouble(m_openPrice - askPrice, _Digits) / _Point);
  }

  if (m_order_type == sell || m_order_type == sellstop || m_order_type == selllimit) {
    //    SELL CLOSE => ASK PRICE
    //==> SELL OPEN  => BID PRICE
    distance_pip = (int)(NormalizeDouble(m_openPrice - bidPrice, _Digits) / _Point);

  }
  return distance_pip;

}
bool SingleOrder::isProfitLowerRtStopLoss(void) {
  bool res = profitPip() < ( m_rt_stoploss_pip +m_rt_adaptive_stoloss_pip);

  if (res)
    Print("SingleOrder::isProfitLowerRtStopLoss(void), profit_pip =",
          profitPip(), " < stoploss_pip + adaptive_sl=", m_rt_stoploss_pip, "+ ", m_rt_adaptive_stoloss_pip,"=",m_rt_stoploss_pip+m_rt_adaptive_stoloss_pip);
  return res;
}

bool SingleOrder::isProfitOverRtTakeProfit(void) {
  return (profitPip() > m_rt_takeprofit_pip);
}
void SingleOrder::setRtStopLossPip(int pip) {
  m_rt_stoploss_pip = pip;
  m_rt_adaptive_stoloss_pip = 0;
  m_rt_trailing_count = 0;
}

int SingleOrder::trailingRtStopLoss() {

  m_rt_trailing_count++;
  m_rt_takeprofit_pip = m_rt_takeprofit_pip + m_rt_trailing_step_pip;
  m_rt_stoploss_pip = m_rt_stoploss_pip + m_rt_trailing_step_pip;
  m_rt_adaptive_stoloss_pip = 0;
  Print("trailingRtStopLoss(), T/P =" , m_rt_takeprofit_pip ,",S/L=",m_rt_stoploss_pip);
  return m_rt_takeprofit_pip;
  
}
void SingleOrder::setRtTakeProfitPip(int pip) { m_rt_takeprofit_pip = pip; }
void SingleOrder::setRtTrailingStepPip(int pip) {
  m_rt_trailing_step_pip = pip;
}

void SingleOrder::increaseAdaptiveStoploss(int inc) {
  
  if (inc <= 0) return;
  m_rt_adaptive_stoloss_pip = m_rt_adaptive_stoloss_pip + inc;
  Print("inc=",inc," , m_rt_adaptive_stoloss_pip = ", m_rt_adaptive_stoloss_pip);

}

double SingleOrder::openPrice(void) { return m_openPrice; }
bool SingleOrder::isBuyOrder(void) {
  this.updateOrderType();
  return (m_order_type == buy);
}
bool SingleOrder::isSellOrder(void) {
  this.updateOrderType();
  return (m_order_type == sell);
}
bool SingleOrder::isBuyStopOrder(void) {
  this.updateOrderType();
  return (m_order_type == buystop);
}
bool SingleOrder::isBuyLimitOrder(void) {
  this.updateOrderType();
  return (m_order_type == buylimit);
}
bool SingleOrder::isSellStopOrder(void) {
  this.updateOrderType();
  return (m_order_type == sellstop);
}
bool SingleOrder::isSellLimitOrder(void) {
  this.updateOrderType();
  return (m_order_type == selllimit);
}
bool SingleOrder::isNoneOrder(void) {
  this.updateOrderType();
  return (m_order_type == none);
}
int SingleOrder::profitPip(void) {

  double diffprice = 0;

  if (m_order_type == buy)
    diffprice = Bid - m_openPrice;  // close buy at Bid price ,checked passed

  if (m_order_type == sell)
    diffprice = m_openPrice - Ask;  // close sell at Ask Price ,checked passed

  m_previous_pip = m_current_pip;
  m_current_pip = (int)(NormalizeDouble(diffprice, _Digits) / _Point);

  return m_current_pip;
}

bool SingleOrder::isOverStopLoss() {
  if (m_stoploss_pip == 0) return false;

  if (m_order_type == buy)
    return (Bid < m_stoplossPrice);
  else
    return (Ask > m_stoplossPrice);
}
bool SingleOrder::isOverTakeProfit() {

  if (m_takeprofit_pip == 0) return false;
  if (m_order_type == buy)
    return (Bid > m_takeprofitPrice);
  else
    return (Ask < m_takeprofitPrice);
}

void SingleOrder::OpenPending(int op, int diff, int sl_pip , int tp_pip) {
  m_order_iTime = iTime(Symbol(), PERIOD_CURRENT, 0);

  if (op != OP_BUYLIMIT && op != OP_BUYSTOP && op != OP_SELLLIMIT &&
      op != OP_SELLSTOP) {
    Print("OrderType is not ( OP_BUYLIMIT,OP_BUYSTOP,OP_SELLLIMIT,OP_SELLSTOP");
    ExpertRemove();
  }
  // OpenPendingOrder(string pSymbol,int pType,double pVolume,double
  // pPrice,double pStop,double pProfit,string pComment,datetime
  // pExpiration,color pArrow)

//Print("SL P=",sl_pip);
//  Print("TP_P=",tp_pip);
  double askPrice = MarketInfo(Symbol(), MODE_ASK);
  double bidPrice = MarketInfo(Symbol(), MODE_BID);
  if (op == OP_BUYLIMIT || op == OP_BUYSTOP) {
    // Open Buy at Ask Price

    m_openPrice = askPrice + (diff * Point);
    //Print(" OPEN BUY PEDING use ASK Price") ;
    //Print(">>Ask Price =", Ask);
    //Print("  Bid Price = ", Bid);
    m_stoplossPrice = sl_pip == 0 ? 0 : bidPrice + ((sl_pip + diff) * _Point);
    m_takeprofitPrice = tp_pip == 0 ? 0 : bidPrice + ((tp_pip + diff) * _Point);

  } else {  // OP_SELLLIMIT , OP_SELLSTOP

    // Open Sell At Bid Price
    //Print(" OPEN SELL PEDING use BID Price") ;

    m_openPrice = bidPrice + (diff * Point);
    m_stoplossPrice = sl_pip == 0 ? 0 : askPrice + ((diff - sl_pip) * _Point);
    m_takeprofitPrice = tp_pip == 0 ? 0 : askPrice + ((diff - tp_pip) * _Point);
  }




// Print("OpenPending , m_openPrice = ", m_openPrice ,"Type=",OrderTypeToString(op));
  m_ticket = OpenPendingOrder(Symbol(), op, m_volume, m_openPrice, m_stoplossPrice, m_takeprofitPrice,
                              m_comment , 0,
                              clrGreen);

  if (m_ticket == -1) {
    Print("Can not OpenPendingOrder()");
    reset();
    return;
  }
  //---
  /*
  bool selected = OrderSelect(m_ticket,SELECT_BY_TICKET);
  if(!selected) {
     Print("Can not OrderSelect on SingleOrder::openPending() with "
  ,OrderTypeToString(op));
     ExpertRemove();
     return;
  }
  */
}


void SingleOrder::OpenNow(int op) {
  m_order_iTime = iTime(Symbol(), PERIOD_CURRENT, 0);

  if (op != OP_BUY && op != OP_SELL) {
    Alert(" Order Type is not both OP_BUY and OP_SELL");
    ExpertRemove();
  }

  m_ticket = OpenMarketOrder(Symbol(), op, m_volume,
                             m_comment, clrGreen);
  if (m_ticket == -1) {
    Print("Warning:Can not openMarkeorder()");
    reset();
    return;
  }

  bool selected = OrderSelect(m_ticket, SELECT_BY_TICKET);

  if (!selected) {
    Alert("Can not OrderSelect on SingleOrder::OpenNow() with ",
          OrderTypeToString(op));
    reset();
    return;
  }

  m_openPrice = OrderOpenPrice();
  m_order_openTime = OrderOpenTime();
  bool modified = false;
  int retry = 0;

  //Print("Open Price",m_openPrice);
  // Print("Spread Pip =",SingleOrder::spreadPip());
  // Print("S/L pip    =", m_stoploss_pip);
  // Print("T/P pip    =", m_takeprofit_pip);
  // TODO : May add retry for stop loss & take profit here;
  do {
    if (op == OP_SELL) {
      // Open Sell at Bid Price
      double bidPrice = MarketInfo(Symbol(), MODE_BID);
      Print("Bid Price = ", bidPrice, ",Open Sell at ", m_openPrice);
      m_stoplossPrice = bidPrice - (m_stoploss_pip * _Point);
      m_takeprofitPrice = bidPrice + (m_takeprofit_pip * _Point);
    } else {
      // Open Buy at Ask Price
      double askPrice = MarketInfo(Symbol(), MODE_ASK);
      Print("Ask Price = ", askPrice, ",Open Sell at ", m_openPrice);
      m_stoplossPrice = askPrice + (m_stoploss_pip * _Point);
      m_takeprofitPrice = askPrice - (m_takeprofit_pip * _Point);
    }

    modified = true;

    if (m_stoploss_pip > 0 || m_takeprofit_pip > 0) {
      Print("set S/L = ", m_stoplossPrice);
      Print("set T/P = ", m_takeprofitPrice);
      modified = OrderModify(m_ticket, 0, m_stoplossPrice, m_takeprofitPrice,
                             clrGreen);
    }
    retry++;

  } while (!modified && retry < 3);

  if (!modified) {
    Alert(" Can not OrderModify on SingleOrder::OpenNow() with " +
          OrderTypeToString(op) +
          ",max rety > 3 , so Close Order to protect loss");
    CloseMarketOrder(m_ticket);
    ExpertRemove();
    return;
  }
}
void SingleOrder::BuyNow(string comment) {
  m_comment = comment;
  m_order_type = buy;
  OpenNow(OP_BUY);
}
void SingleOrder::SellNow(string comment) {
  m_comment = comment;
  m_order_type = sell;
  OpenNow(OP_SELL);
}
void SingleOrder::CloseNow() {
  if (isNoneOrder()) return;
  CloseMarketOrder(m_ticket);
  reset();
  m_order_done++;
}
void SingleOrder::BuyPending(int pip, int sl_pip , int tp_pip) {
  if (pip > 0) {
    m_order_type = buystop;
    OpenPending(OP_BUYSTOP, pip, sl_pip, tp_pip);
  } else {
    m_order_type = buylimit;
    OpenPending(OP_BUYLIMIT, pip, sl_pip, tp_pip);
  }
}

void SingleOrder::SellPending(int pip, int sl_pip, int tp_pip) {
  if (pip > 0) {
    OpenPending(OP_SELLLIMIT, pip , sl_pip , tp_pip);
    m_order_type = selllimit;
  } else {
    OpenPending(OP_SELLSTOP, pip, sl_pip , tp_pip);
    m_order_type = sellstop;
  }
}
void SingleOrder::ClosePending(void) {

  if (isNoneOrder()) return;

  bool result = false ;

  if (!result && this.isPendingOrder()){
    result = DeletePendingOrder(m_ticket);
  }

  if (!result && this.isOpenedOrder()){
    result = CloseMarketOrder(m_ticket);
  }

  if ( result ) {
    reset();
    m_order_done++;
  }

}

int SingleOrder::OpenMarketOrder(string pSymbol, int pType, double pVolume,
                                 string pComment, color pArrow) {
  int retryCount = 0;
  int ticket = 0;
  int errorCode = 0;

  double orderPrice = 0;

  string orderType;
  string errDesc;

  // Order retry loop
  while (retryCount <= MAX_RETRIES) {
    while (IsTradeContextBusy()) Sleep(10);

    // Get current bid/ask price
    if (pType == OP_BUY)
      orderPrice = MarketInfo(pSymbol, MODE_ASK);
    else if (pType == OP_SELL)
      orderPrice = MarketInfo(pSymbol, MODE_BID);

    // Place market order
    ticket = OrderSend(pSymbol, pType, pVolume, orderPrice, s_slippage, 0, 0,
                       pComment, s_magic_number, 0, pArrow);

    // Error handling
    if (ticket == -1) {
      errorCode = GetLastError();
      errDesc = ErrorDescription(errorCode);
      bool checkError = RetryOnError(errorCode);
      orderType = OrderTypeToString(pType);

      // Unrecoverable error
      if (checkError == false) {
        Print("Warning:Open ", orderType, " order: Error ", errorCode, " - ", errDesc);
        Print("Warning:Symbol: ", pSymbol, ", Volume: ", pVolume, ", Price: ",
              orderPrice);
        break;
      }

      // Retry on error
      else {
        Print("Server error detected, retrying...");
        Sleep(1000);
        retryCount++;
      }
    }

    // Order successful
    else {
      orderType = OrderTypeToString(pType);
      // Comment(orderType, " order #", ticket, " opened on ", pSymbol);
      //Print(orderType, " order #", ticket, " opened on ", pSymbol);
      break;
    }
  }

  // Failed after retry
  if (retryCount > MAX_RETRIES) {
    Print("Warning:Open ", orderType, " order: Max retries exceeded. Error ", errorCode,
          " - ", errDesc);
    Print("Warning:Symbol: ", pSymbol, ", Volume: ", pVolume, ", Price: ", orderPrice);
  }

  return (ticket);
}
bool SingleOrder::CloseMarketOrder(int pTicket, double pVolume = 0.000000,
                                   color pArrow = 255) {
  int retryCount = 0;
  int errorCode = 0;

  double closePrice = 0;
  double closeVolume = 0;

  bool result;

  string errDesc;

  // Select ticket
  result = OrderSelect(pTicket, SELECT_BY_TICKET);

  // Exit with error if order select fails
  if (result == false) {
    errorCode = GetLastError();
    errDesc = ErrorDescription(errorCode);

    Alert("Close order: Error selecting order #", pTicket, ". Error ",
          errorCode, " - ", errDesc);
    return (result);
  }

  // Close entire order if pVolume not specified, or if pVolume is greater than
  // order volume
  if (pVolume == 0 || pVolume > OrderLots())
    closeVolume = OrderLots();
  else
    closeVolume = pVolume;

  // Order retry loop
  while (retryCount <= MAX_RETRIES) {
    while (IsTradeContextBusy()) Sleep(10);

    // Get current bid/ask price
    if (OrderType() == OP_BUY)
      closePrice = MarketInfo(OrderSymbol(), MODE_BID);
    else if (OrderType() == OP_SELL)
      closePrice = MarketInfo(OrderSymbol(), MODE_ASK);

    result = OrderClose(pTicket, closeVolume, closePrice, s_slippage, pArrow);

    if (result == false) {
      errorCode = GetLastError();
      errDesc = ErrorDescription(errorCode);
      bool checkError = RetryOnError(errorCode);

      // Unrecoverable error
      if (checkError == false) {
        Alert("Close order #", pTicket, ": Error ", errorCode, " - ", errDesc);
        Print(" Warning:Price: ", closePrice, ", Volume: ", closeVolume);
        break;
      }

      // Retry on error
      else {
        Print("Server error detected, retrying...");
        Sleep(RETRY_DELAY);
        retryCount++;
      }
    }

    // Order successful
    else {
      Comment("Order #", pTicket, " closed");
      //Print("Order #", pTicket, " closed");
      break;
    }
  }

  // Failed after retry
  if (retryCount > MAX_RETRIES) {
    Alert("Close order #", pTicket, ": Max retries exceeded. Error ", errorCode,
          " - ", errDesc);
    Print("Price: ", closePrice, ", Volume: ", closeVolume);
  }

  return (result);
}

int SingleOrder::OpenPendingOrder(string pSymbol, int pType, double pVolume,
                                  double pPrice, double pStop, double pProfit,
                                  string pComment, datetime pExpiration,
                                  color pArrow) {
  int retryCount = 0;
  int ticket = 0;
  int errorCode = 0;

  string orderType;
  string errDesc;

  // Order retry loop
  while (retryCount <= MAX_RETRIES) {
    while (IsTradeContextBusy()) Sleep(10);
    ticket = OrderSend(pSymbol, pType, pVolume, pPrice, s_slippage, pStop,
                       pProfit, pComment, s_magic_number, pExpiration, pArrow);

    // Error handling
    if (ticket == -1) {
      errorCode = GetLastError();
      errDesc = ErrorDescription(errorCode);
      bool checkError = RetryOnError(errorCode);
      orderType = OrderTypeToString(pType);

      // Unrecoverable error
      if (checkError == false) {
        Alert("Open ", orderType, " order: Error ", errorCode, " - ", errDesc);
        Print("Symbol: ", pSymbol, ", Volume: ", pVolume, ", Price: ", pPrice,
              ", SL: ", pStop, ", TP: ", pProfit, ", Expiration: ",
              pExpiration);
        break;
      }

      // Retry on error
      else {
        Print("Server error detected, retrying...");
        Sleep(RETRY_DELAY);
        retryCount++;
      }
    }

    // Order successful
    else {
      orderType = OrderTypeToString(pType);
      // Comment(orderType, " order #", ticket, " opened on ", pSymbol);
      // Print(orderType, " order #", ticket, " opened on ", pSymbol);
      break;
    }
  }

  // Failed after retry
  if (retryCount > MAX_RETRIES) {
    Alert("Open ", orderType, " order: Max retries exceeded. Error ", errorCode,
          " - ", errDesc);
    Print("Symbol: ", pSymbol, ", Volume: ", pVolume, ", Price: ", pPrice,
          ", SL: ", pStop, ", TP: ", pProfit, ", Expiration: ", pExpiration);
  }

  return (ticket);
}
bool SingleOrder::DeletePendingOrder(int pTicket, color pArrow = 255) {
  int retryCount = 0;
  int errorCode = 0;

  bool result = false;

  string errDesc;

  // Order retry loop
  while (retryCount <= MAX_RETRIES) {

    while (IsTradeContextBusy()) Sleep(10);

    result = OrderDelete(pTicket, pArrow);

    if (result == false) {
      errorCode = GetLastError();
      errDesc = ErrorDescription(errorCode);
      bool checkError = RetryOnError(errorCode);

      // Unrecoverable error
      if (checkError == false) {
        Alert("Delete pending order #", pTicket, ": Error ", errorCode, " - ", errDesc);
        break;
      }

      // Retry on error
      else {
        Print("Server error detected, retrying...");
        Sleep(RETRY_DELAY);
        retryCount++;
      }
    }

    // Order successful
    else {
      Comment("Pending order #", pTicket, " deleted");
      //Print("Pending order #", pTicket, " deleted");
      break;
    }
  }

  // Failed after retry
  if (retryCount > MAX_RETRIES) {
    Alert("Delete pending order #", pTicket, ": Max retries exceeded. Error ",
          errorCode, " - ", errDesc);
  }

  return (result);
}
