//+------------------------------------------------------------------+
//|                                                   VPAMonitor.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link "https://www.mql5.com"
#property strict
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+
//+---------------------MONITOR
//CLASS---------------------------------------------+

#include "MVAMonitor.mqh"
#include "ResistanceSupport.mqh"
#include "VPAMonitor.mqh"
#include "USDCounterHedger.mqh"



class SignalManager {
 public:
  SignalManager();
  ~SignalManager();

  void collectOnTick();
  void processTickDataWhenTimer();
  bool singalBuy();
  bool singalSell();
  bool isMarketCrazy();
  bool isHedgeCondBuy();
  bool isHedgeCondSell();
  bool isWeekEndGap();

 private:
  
  VPAMonitor vpa_monitor;
  MVAMonitor mva_monitor;
  ResistanceSupport rs_floor;
};

SignalManager::SignalManager(void) {}

SignalManager::~SignalManager(void) {}


bool SignalManager::isHedgeCondBuy(void){
   USDCounterHedger hedge;
   
   return hedge.buyRWCount() <= hedge.sellCount();
   //return (hedge.buyNegativeOrderCount() <= hedge.sellNegativeOrderCount());
}

bool SignalManager::isHedgeCondSell(void){
   USDCounterHedger hedge;
   return hedge.sellRWCount() <= hedge.buyCount();
   //return( hedge.sellNegativeOrderCount() <= hedge.buyNegativeOrderCount());

}
bool SignalManager::singalBuy(){
   
   bool vpa_signal = vpa_monitor.singalBuy();  
   return ( vpa_signal && !isWeekEndGap() && !isMarketCrazy() && isHedgeCondBuy());
}
bool SignalManager::singalSell(){

   bool vpa_signal = vpa_monitor.singalSell();
   return ( vpa_signal && !isWeekEndGap() && !isMarketCrazy() && isHedgeCondSell()); 
}
void SignalManager::collectOnTick(){
   //this method has to run  under Ontick of Main program
   vpa_monitor.collectOnTick();
}
void SignalManager::processTickDataWhenTimer(){
   // run undertimer
   vpa_monitor.processTickDataWhenTimer();
}
bool SignalManager::isMarketCrazy(){

  return SingleOrder::HighSpread()|| Account::FreeMarginLowerBalance(60);

}
bool SignalManager::isWeekEndGap(){

    datetime current = TimeCurrent();
    int day = TimeDayOfWeek(current); // 0 - sunday ..
    int hour = TimeHour(current);
    bool isLateFriday = (day ==  5 && hour >= 12);
    bool isEarlyMonday = (day == 1 && hour <= 1);
    return ( isLateFriday || isEarlyMonday);

}