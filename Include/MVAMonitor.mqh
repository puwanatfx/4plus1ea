//+------------------------------------------------------------------+
//|                                                   MVAMonitor.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+


class MVAMonitor {
   
   public:
      MVAMonitor(void);
      ~MVAMonitor(void);
      double getCurrent();
      double get10EMA(int);
      double get200EMA(int);
      int diff10EMAPip(int,int);
      int diff200EMAPip(int,int);
      int diff10EMAand200EMAPip(int);

      // for grailsEA
      double getEMA(int period,int shift = 0);
   private:
   double m_current_ma;
};

MVAMonitor::MVAMonitor(void){
   
}

MVAMonitor::~MVAMonitor(void){


}
int MVAMonitor::diff10EMAPip(int pos1, int pos2){
   double diff = get10EMA(pos1)- get10EMA(pos2);
   return (int)(diff/_Point);

}

int MVAMonitor::diff200EMAPip(int pos1, int pos2){
   double diff = get200EMA(pos1)- get200EMA(pos2);
   return (int)(diff/_Point);

}
int MVAMonitor::diff10EMAand200EMAPip(int pos){
   double diff = get10EMA(pos)- get200EMA(pos);
   return (int)(diff/_Point);

}
double MVAMonitor::getCurrent(){
   double ma = iMA(Symbol(),PERIOD_M1,10,0,MODE_EMA,PRICE_CLOSE,0);
   m_current_ma =  (NormalizeDouble(ma,_Digits));
   return m_current_ma;
}
double MVAMonitor::get10EMA(int shift){
   double ma = iMA(Symbol(),PERIOD_M1,10,0,MODE_EMA,PRICE_CLOSE,shift);
   return  (NormalizeDouble(ma,_Digits));
}

double MVAMonitor::get200EMA(int shift){
   double ma = iMA(Symbol(),PERIOD_M1,200,0,MODE_EMA,PRICE_CLOSE,shift);
   return  (NormalizeDouble(ma,_Digits));
}

// for grails EA 

double MVAMonitor::getEMA(int period, int shift){
   double ma = iMA(Symbol(),PERIOD_M1,period,0,MODE_EMA,PRICE_MEDIAN,shift);
   return ma;
}