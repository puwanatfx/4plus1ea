//+------------------------------------------------------------------+
//|                                                  ImageObject.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
#include "DenFxUtil.mqh"
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class ImageObject
  {
private:
   string m_object_name;
   string m_url;
   string m_filename;
   void load();

public:
                     ImageObject(string,string);
                    ~ImageObject();
                    
                    //http://4plus1ea.com/image/41logo.bmp
  };
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ImageObject::ImageObject(string name ,string image)
  {
   ASSERT(name != "" , "ImageObject Name is empty");

   m_object_name = name;
   m_url = "http://4plus1ea.com/image/"+image;
   load();

   ObjectCreate(0, m_object_name, OBJ_BITMAP_LABEL, 0, 0, 0);
   if(!ObjectSetString(0, "logo", OBJPROP_BMPFILE,0, "\\Files\\test.bmp"))
      Alert("Image Object can not load with URL =",image);
   
  }
void ImageObject::load(void){
   string cookie=NULL,headers;
   char post[],result[];
   int res;
   
   
   //string google_url="https://www.google.com/finance";
//--- Reset the last error code
   ResetLastError();
//--- Loading a html page from Google Finance
   int timeout=5000; //--- Timeout below 1000 (1 sec.) is not enough for slow Internet connection
   res=WebRequest("GET",m_url+"?testEEERR77333",cookie,NULL,timeout,post,0,result,headers);
//--- Checking errors
   if(res==-1)
     {
      Print("Error in WebRequest. Error code  =",GetLastError());
      //--- Perhaps the URL is not listed, display a message about the necessity to add the address
      MessageBox("Add the address '"+m_url+"' in the list of allowed URLs on tab 'Expert Advisors'","Error",MB_ICONINFORMATION);
     }
   else
     {
      //--- Load successfully
      PrintFormat("The file has been successfully loaded, File size =%d bytes.",ArraySize(result));
      //--- Save the data to a file
      int filehandle=FileOpen("test.bmp",FILE_WRITE|FILE_BIN);
      //--- Checking errors
      if(filehandle!=INVALID_HANDLE)
        {
         //--- Save the contents of the result[] array to a file
         FileWriteArray(filehandle,result,0,ArraySize(result));
         //--- Close the file
         FileClose(filehandle);
        }
      else Print("Error in FileOpen. Error code=",GetLastError());
     }

}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
ImageObject::~ImageObject()
 {
      ObjectDelete(m_object_name);
 }
//+------------------------------------------------------------------+
