//+------------------------------------------------------------------+
//|                                             DualOrderManager.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link "https://www.mql5.com"
#property version "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
#include "SingleOrder.mqh"

enum enum_dual_man_state {

  both_none,
  upper_pending,
  lower_pending,
  upper_opened,
  lower_opened,
  both_opened,
  upper_closed,
  lower_closed,
  both_closed

};
class DualOrderManager {
 private:
  SingleOrder upper;
  SingleOrder lower;
  SingleOrder* active_order;
  SingleOrder* idle_order;
  
  //Dynamic SL/PF
  bool m_narrow_sl_exected;
  bool m_wider_sl_exected;
  bool m_dynamic_sl_enabled;
  
  void updateOrderType();
  void resetPointer();
  bool pointerAreAssigned() {
    return (active_order != NULL && idle_order != NULL);
  }
  bool pointerAreNull() { return (active_order == NULL && idle_order == NULL); }
  void setStoplossToAcive();
  

 public:
  DualOrderManager();
  ~DualOrderManager();
  void setVolume(double vol){ upper.setVolume(vol);lower.setVolume(vol);}
  void openBothPending(bool dynamic_sl = true);
  void closeBothPending();
  bool areBothStillPending();
  bool areBothNoneOrder();
  bool upperIsOpened();
  bool lowerIsOpened();
  bool hasOpenedOrder();
  void setComment(string);
  void monitor(bool);

  void Test();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
DualOrderManager::DualOrderManager() { resetPointer(); }
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
DualOrderManager::~DualOrderManager() { closeBothPending(); }

void DualOrderManager::resetPointer() {
    active_order = NULL;
    idle_order = NULL;
    m_narrow_sl_exected = false;
    m_wider_sl_exected = false;
    m_dynamic_sl_enabled = true;
    
}
void DualOrderManager::closeBothPending() {
  upper.ClosePending();
  lower.ClosePending();
}

bool DualOrderManager::hasOpenedOrder() {
  return (upperIsOpened() || lowerIsOpened());
}

void DualOrderManager::setComment(string comment) {
  upper.setComment(comment);
  lower.setComment(comment);
}

bool DualOrderManager::areBothStillPending() {
  return (upper.isPendingOrder() && lower.isPendingOrder());
}
bool DualOrderManager::areBothNoneOrder() {
  return (upper.isNoneOrder() && lower.isNoneOrder());
}
bool DualOrderManager::upperIsOpened() {
  return (upper.isBuyOrder() || upper.isSellOrder());
}
bool DualOrderManager::lowerIsOpened() {
  return (lower.isBuyOrder() || lower.isSellOrder());
}
void DualOrderManager::updateOrderType() {
  upper.updateOrderType();
  lower.updateOrderType();
}

void DualOrderManager::openBothPending(bool dynamicSLFeature) {
  resetPointer();
  upper.BuyPending(5, -5, 200);
  lower.SellPending(-5, -5, 200);
  m_dynamic_sl_enabled = dynamicSLFeature;
}

void DualOrderManager::setStoplossToAcive() {
  ASSERT(pointerAreAssigned(), "pointerAreAssigned()");

  idle_order.ClosePending();

  bool setSL;
  int retry = 0;
  while (!(setSL = active_order.setSVStopLossPip(-5)) && retry < 3) {
    Sleep(10);

    if (active_order.isNoneOrder()) break;
    retry++;
    Print("Retry setSVStopLossPip()");
  }
  if (!setSL || retry >= 3) {
    active_order.CloseNow();
    return;
  }
  active_order.setSVTakeProfitPip(5);
  active_order.setSVTrailingSetpPip(5);
}
void DualOrderManager::monitor(bool sigCloseOpenedOrder) {
  // if both orders are not set , then do nothing
  if (areBothNoneOrder()) return;

  if (areBothStillPending() && sigCloseOpenedOrder) {
    Print("[SIGNAL-CANCEL] Close Pending Order");
    closeBothPending();
  }

  if (pointerAreNull()) {
    if (upper.isBuyOrder() && lower.isSellStopOrder()) {
      Print("[UPPER] ORDER IS OPENED.");
      active_order = &upper;
      idle_order = &lower;
      setStoplossToAcive();
    }
    if (lower.isSellOrder() && upper.isBuyStopOrder()) {
      Print("[LOWER] ORDER IS OPENED.");
      active_order = &lower;
      idle_order = &upper;
      setStoplossToAcive();
    }

    // Something worng can not set Both as pending order;
    if (upper.isBuyStopOrder() && lower.isNoneOrder()) {
      Print("Warning : Can not set dual pending order ,delete pending UPPER");
      upper.ClosePending();
      return;
    }
    if (upper.isNoneOrder() && lower.isSellStopOrder()) {
      Print("Warning : Can not set dual pending order, delete pending LOWER");
      lower.ClosePending();
      return;
    }

  }  // pointerAreNull()

  if (pointerAreAssigned()) {
    if (active_order.getActualProfit() > 0 && sigCloseOpenedOrder) {
      Print("[ACTIVE] Close because of Signal");
      active_order.CloseNow();
      resetPointer();
      return;
    }
    int profitPip = active_order.profitPip() ;
    
    //## Dynamic SL SIZE / 
    
    // Narrow stop loss and profit 
    if (m_dynamic_sl_enabled && profitPip >= 4 && !m_narrow_sl_exected ){
    
      active_order.setSVTakeProfitPip(profitPip+2);
      active_order.setSVStopLossPip(-2);
      active_order.setSVTrailingSetpPip(2);
      m_narrow_sl_exected = true;
      return;
    }
    // wider stop loss and profit
    if (m_dynamic_sl_enabled && profitPip >= 10  && !m_wider_sl_exected){
      
      active_order.setSVTakeProfitPip(profitPip+4);
      active_order.setSVStopLossPip(-4);
      active_order.setSVTrailingSetpPip(4);
      m_wider_sl_exected = true;
      return;
    }
    
    
    if (active_order.isProfitOverSVTakeProfit()) {
      active_order.trailingSVStopLoss();
      Print("[ACTIVE].trailingSVStopLoss() executed");
      return;
    }
  }  // pointerAreAssigned
}
//+------------------------------------------------------------------+
