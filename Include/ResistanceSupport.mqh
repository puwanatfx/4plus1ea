//+------------------------------------------------------------------+
//|                                            ResistanceSupport.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+
class ResistanceSupport {
   public:
      
      double getResistance(int );
      double getResistanceByMinute(int min, bool& is_currentbar);
      double getSupport(int);
      double getSupportByMinute(int min, bool& is_currentbar);
   private:
   
   
   
   
  

};

double ResistanceSupport::getResistance(int bar_count){
   int index = iHighest(NULL,PERIOD_H1,MODE_HIGH,bar_count,0);
   //Print("High index =",index);
   if (index>=0)
      return iHigh(NULL,PERIOD_H1,index);
   return 0;
}
double ResistanceSupport::getSupport(int bar_count){

    int index=iLowest(NULL,PERIOD_H1,MODE_LOW,bar_count,0);
   //  Print("Low index =",index);
    if (index>=0)
      return  iLow(NULL,PERIOD_H1,index); 
     
    return 0; 
}
double ResistanceSupport::getResistanceByMinute(int min,bool& is_crrentbar){
   int index = iHighest(NULL,PERIOD_M1,MODE_HIGH,min,0);
   is_crrentbar =  (index == 0);
 
   
   return iHigh(NULL,PERIOD_M1,index);
   
}

double ResistanceSupport::getSupportByMinute(int min,bool& is_crrentbar){

    int index=iLowest(NULL,PERIOD_M1,MODE_LOW,min,0);
  
   is_crrentbar = (index == 0);
    
   return  iLow(NULL,PERIOD_M1,index); 
     
}