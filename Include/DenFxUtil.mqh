//+------------------------------------------------------------------+
//|                                                         Util.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property strict
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+

void ASSERT(bool cond,string message){
   if(cond)
      return;
   Print("ASSET!!,"+message);
   Alert("ASSET!!,"+message);
   ExpertRemove();
   
}

string pad(string in , int len , string ch){
   string res ;
   for(int i =0 ; i < len - StringLen(in) ; i ++){
      res = res + ch;
   }
   return res+in;

}

double RoundDown2Digit(double lot){
   lot = MathFloor(lot * 100)/100;
   
   return lot;
}

bool IsMarketOpened() {

  return MarketInfo(Symbol(), MODE_TRADEALLOWED) == 1;

}

void ShowLastOrderInfo() {
  if (HistoryTotal() > 0) {
    if (OrderSelect(HistoryTotal() - 1, SELECT_BY_POS, MODE_HISTORY) == true)
    {
      double actualProfit = RoundDown2Digit(OrderProfit()) + RoundDown2Digit(OrderCommission());
      string result = actualProfit > 0 ? "win" : "loss";
      if (actualProfit == 0) result = "draw";
      Print(result, ",#", OrderTicket(), "Net profit=", actualProfit);
    }
  }

}
int DiffPip(double p1 , double p2){
   return (int)(NormalizeDouble(p1-p2, _Digits) / _Point);
}

/// BBBand Util
double getBollingerBandUpperPrice(){
  return iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_UPPER, 0);
}

double getBollingerBandLowerPrice(){
  return iBands(NULL, PERIOD_M1, 25, 1, 0, PRICE_MEDIAN, MODE_LOWER, 0);
}


double getUpperZonePrice(){
  return getBollingerBandUpperPrice()  + 2* (getBollingerBandUpperPrice() - getBollingerBandLowerPrice());
}  

double getLowerZonePrice(){
  return getBollingerBandLowerPrice() - 2* (getBollingerBandUpperPrice() - getBollingerBandLowerPrice());
}

double getMaxHigh() {
  double high = MathMax(High[0],High[1]);
  high =MathMax(high,High[2]);
  return high ;
  
}
double getMaxLow(){
  double low = MathMin(Low[0],Low[1]);
  low =MathMin(low,Low[2]);
  return low ;
}
