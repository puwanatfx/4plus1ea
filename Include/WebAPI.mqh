//+------------------------------------------------------------------+
//|                                                       WebAPI.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class WebAPI
{
private:
  string m_output;
  int m_http_status;

public:
  WebAPI();
  ~WebAPI();
  string getOutput(){return m_output;};
  int getHttpStatus(){ return m_http_status; };
  void setHttpStatus(int status){  m_http_status = status; };
  int get(string url);
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
WebAPI::WebAPI()
{
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
WebAPI::~WebAPI()
{
}
//+------------------------------------------------------------------+
int WebAPI::get(string url) {
  string cookie = NULL;
  string result_headers;
  char data[], result[];
  m_http_status = WebRequest("GET", url, cookie, NULL, 50, data, 0, result, result_headers);
  //Print(ErrorDescription(GetLastError()));
  m_output = CharArrayToString(result);
  return m_http_status;
}