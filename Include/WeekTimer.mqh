//+------------------------------------------------------------------+
//|                                                    WeekTimer.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class WeekTimer
{
private:
  datetime m_nextMonday;



public:

  WeekTimer();
  ~WeekTimer();
  void findNextMonday();
  datetime findThisMonday();
  bool isNewWeek();
  bool isWeekEndGap();
  bool isDayEndGap();
  bool isLateDay();
};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
WeekTimer::WeekTimer()
{
  findNextMonday();
}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
WeekTimer::~WeekTimer()
{
}

//+------------------------------------------------------------------+
void WeekTimer::findNextMonday() {
  datetime current = TimeCurrent();
  current = current - TimeHour(current) * 60 * 60 - TimeMinute(current) * 60 - TimeSeconds(current);
  int day = TimeDayOfWeek(current) + 1; // sunday is 0
  int nDay2Monday = day >= 2 ? 7 - day + 2 : 2 - day;
  m_nextMonday = current + (nDay2Monday * 24 * 60 * 60);
  Print("WeekTimer::Next Monday=", m_nextMonday);

}

datetime WeekTimer::findThisMonday() {
  datetime thisMonday;
  datetime current = TimeCurrent();
  current = current - TimeHour(current) * 60 * 60 - TimeMinute(current) * 60 - TimeSeconds(current);
  int day = TimeDayOfWeek(current); // sunday is 0
  thisMonday = current  - (TimeDayOfWeek(current)-1)*24*60*60;
  //Print("WeekTimer::This Monday=", thisMonday);
  return thisMonday;

}


bool WeekTimer::isNewWeek(void) {
  if (TimeCurrent() > m_nextMonday) {
    findNextMonday();
    return true;
  }
    return false;
}


bool WeekTimer::isLateDay(){
  // Convert to Thailand Hour and Min

  int hour =  TimeHour(TimeGMT()+7*60*60);
  int min = TimeMinute(TimeGMT()+7*60*60);

  bool before_endtime = (hour < 5);
  bool after_starttime = (hour >= 23 && min >=0);

  return  (before_endtime || after_starttime);
}

bool WeekTimer::isWeekEndGap() {

  datetime current = TimeCurrent();
  int day = TimeDayOfWeek(current); // 0 - sunday ..
  int hour = TimeHour(current);
  bool isLateFriday = (day ==  5 && hour >= 18);
  bool isEarlyMonday = (day == 1 && hour <= 1);
  return ( isLateFriday || isEarlyMonday);


}

bool WeekTimer::isDayEndGap() {

  datetime current = TimeCurrent();
  int hour = TimeHour(current);
  bool res = (hour >= 22 || hour <= 1);
  return ( res);


}