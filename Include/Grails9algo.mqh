//+------------------------------------------------------------------+
//|                                                    g9algo.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
class Grails9algo
{
private:
  int m_signal_count;

  bool m_time_enable;
  int m_time_count;
  int m_max_sec;
  int m_signal_status[10];


public:

  Grails9algo(int);
  ~Grails9algo();


  void monitor();
  void addSignal(int status);
  void addComment(string comment);
  void reset();
  int getCount();
  void fake();
  string getComment();
  bool isSameDirection();


};
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Grails9algo::Grails9algo(int max_sec)
{
  m_max_sec = max_sec;
  Print("Grails9algo.m_max_sec = ", m_max_sec);
  reset();
  //addSignal();

}
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+
Grails9algo::~Grails9algo()
{

} 

void Grails9algo::reset() {
  Print("Grails9algo::reset()");
  m_signal_count = 0;
  m_time_count = 0;
  m_time_enable = false; 
  
}


void Grails9algo::addSignal(int status) {
  Print("Grails9algo::addSignal(",status,")");
  m_signal_status[m_signal_count] = status;
  m_signal_count++;
  m_time_enable = true;

}

void Grails9algo::monitor() {

  if ( !m_time_enable) {
    return;
  }

  if (m_time_enable &&  m_time_count < m_max_sec) {
    if ( m_time_count % 10 == 0 ) {
      Print("m_time_count++", m_time_count ," to m_max_sec = ", m_max_sec);
    }
    m_time_count++;
    return;
  }
 
  if (m_time_enable && m_time_count >= m_max_sec) {
    
    Print("Grails9algo::reset() when m_time_enable && m_time_count > ", m_max_sec, " sec");
    reset();
  }


}
int Grails9algo::getCount() {

  return m_signal_count;

}

string Grails9algo::getComment(){
  string statusComment = "";
  for(int i =0 ; i < m_signal_count;i++){
    statusComment =statusComment + StringFormat("%d",m_signal_status[i]);
  }
    
 //string return_cmnt = StringFormat("C%d-",m_signal_count)+statusComment;
  string return_cmnt = statusComment;
  return return_cmnt;
}
bool Grails9algo::isSameDirection(){

  if (m_signal_count <= 1) return false;

  for(int i = 1 ; i < m_signal_count ; i ++){
    if(m_signal_status[0] != m_signal_status[i]) return false;
  }
  return true;

}
void Grails9algo::fake() {
  if (m_signal_count > 0) return;
  addSignal(222);
  addSignal(222);
  addSignal(222);

}