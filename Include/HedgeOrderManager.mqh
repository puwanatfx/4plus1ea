//+------------------------------------------------------------------+
//|                                            HedgeOrderManager.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link      "https://www.mql5.com"
#property version   "1.00"
#property strict
//+------------------------------------------------------------------+
//|                                                                  |
//+------------------------------------------------------------------+

#include "SingleOrder.mqh"

#define MAX_ORDERS 50
#define H_RANGE 100

enum hedge_state { h_none , h_upper , h_upper_profit , h_lower , h_lower_profit };

class HedgeOrderManager
{

private:
	SingleOrder orders[MAX_ORDERS];
	int m_range_pip ;

	hedge_state m_state;
	int m_orderIdx ;
	double m_start_volume;
	double m_volume;
	double m_UpperPrice;
	double m_lowerPrice;
	bool m_trailing_actived;

	// hedge trailing feature
	int m_takeprofit_pip;
	int m_stoploss_pip;
	int m_trailing_step_pip;
   int m_max_loss_pip;
	void reset();



public:
	HedgeOrderManager();
	~HedgeOrderManager();

	void monitor();
	void buyUpper(string);
	void sellLower(string);
	int count();
	double getActualProfit();
	bool isTrailingActivated() { return m_trailing_actived;}
	void closeAll();
	void setVolume( double vol) { m_start_volume = vol;}
	int getRangePip(){ return m_range_pip;}
	int getMaxLossPip() { return m_max_loss_pip; }

private:



	bool isUpperState() { return (m_state == h_upper);}
	bool isLowerState() { return (m_state == h_lower);}
	bool isNoneState() { return (m_state == h_none);}
	bool isUpperProfitState() { return (m_state == h_upper_profit);}
	bool isLowerProfitState() { return (m_state == h_lower_profit);}
	bool isTooDifferenceUpperLower();

	void openNow(int op , string);

	double getAllBuyVolume(bool);
	double getAllSellVolume(bool);
	double getProfit();
	double getLowerPrice() { return m_lowerPrice;}
	double getUpperPrice() { return m_UpperPrice;}
	int diffPipOfUpper();
	int diffPipOfLower();
	void setTrailingFirstOrder();

	void trailingRtStopLossFirstOrder();
	void changeToUpperProfit();
	void changeToLowerProfit();
	double hedgeProfitPoint();


};



HedgeOrderManager::HedgeOrderManager()
{

	m_range_pip = H_RANGE;
	m_start_volume = 0.01;
	m_trailing_step_pip = 20;
	reset();

}



HedgeOrderManager::~HedgeOrderManager()
{

}

void HedgeOrderManager::reset() {
	m_state = h_none;
	m_orderIdx = 0;
	m_volume = 0.01;
	m_lowerPrice = 0;
	m_UpperPrice = 0;
	m_takeprofit_pip = 0;
	m_stoploss_pip = 0;
	m_trailing_actived = false;
	m_max_loss_pip = 0;
}

int HedgeOrderManager::count() {

	int count = 0;
	for (int i = 0; i < MAX_ORDERS; i++)
		if (!orders[i].isNoneOrder()) count++;

	// force reset incase order not close by this.closeAll()
	if (count == 0) this.reset();
	return count;

}

double HedgeOrderManager::hedgeProfitPoint() {
	if (this.isUpperState()) {
		double diff_vol = this.getAllBuyVolume(false);
		return diff_vol * m_trailing_step_pip;
	}
	if (this.isLowerState()) {
		double diff_vol = this.getAllSellVolume(false);
		return diff_vol * m_trailing_step_pip;
	}
	return 0;

}
bool HedgeOrderManager::isTooDifferenceUpperLower() {

	if (m_UpperPrice != 0 && m_lowerPrice != 0) {
		// check m_UpperPrice and LowerPrice not too much difference
		double upper_lost_limit_price = NormalizeDouble(m_UpperPrice - (m_range_pip * _Point), _Digits);
		double lower_lost_limit_price = NormalizeDouble(m_lowerPrice + (m_range_pip * _Point), _Digits);
		double diff_price = (lower_lost_limit_price - upper_lost_limit_price);

		int diff_pip_limit = (int)(NormalizeDouble(diff_price, _Digits) / _Point);
		if (diff_pip_limit < (m_range_pip / 2)) {
			Print("!! WARNING - prices isTooDifferenceUpperLower() !!");
			return true;
		}

	}
	return false;

}

void HedgeOrderManager::monitor() {

	if (this.count() == 0) return;


	if (isTooDifferenceUpperLower()) {
		this.closeAll();
		//ExpertRemove();
		return;
	}

	// for max losss pip for single order
	if (this.count() == 1 && (this.isUpperState()|| this.isLowerState()) ) {
	   int diff = orders[0].profitPip();
	   if (diff < m_max_loss_pip)
	      m_max_loss_pip = diff;
	}
	
	if (this.count() > 1) {
	   m_max_loss_pip = 0;
	}
	
	// has opened Order
	if (this.isUpperState()) {

		//Print("diff upper=",this.diffPipOfUpper());

		if (this.count() > 1 && this.getProfit() > this.hedgeProfitPoint() && this.getActualProfit() > 0) {

			this.changeToUpperProfit();
			return;
		}

		int diff = this.diffPipOfUpper() ;

		
		if (diff < -m_range_pip ) {
			Print("diffPipOfUpper=", diff, ",upperPrice=", this.getUpperPrice());
			string comment = StringFormat("N=%d,",m_orderIdx+1);
			this.sellLower(comment+"lower");
			return;
		}

		if (this.count() == 1) {
			this.trailingRtStopLossFirstOrder();
			return;
		}
		return;
	}


	if (this.isLowerState() ) {

		if (this.count() > 1 && this.getProfit() > this.hedgeProfitPoint() && this.getActualProfit() > 0) {
			this.changeToLowerProfit();
			return;
		}
		int diff = this.diffPipOfLower();
		if (diff < -m_range_pip) {
			Print("diffPipOfLower=", diff, ",lowerPrice=", this.getLowerPrice());
			string comment = StringFormat("N=%d,",m_orderIdx+1);
			this.buyUpper(comment+"upper");
			return;
		}

		if (this.count() == 1) {
			this.trailingRtStopLossFirstOrder();
			return;
		}
		return;

	}
	if (this.isUpperProfitState() || this.isLowerProfitState()) {

		int diff = this.isUpperProfitState() ?  this.diffPipOfUpper() : this.diffPipOfLower();
		
		if (diff > m_takeprofit_pip) {
		   while(diff > m_takeprofit_pip){
			   Print("HedgeTrailing stop loss is activated , pip = ", diff);
			   m_stoploss_pip = m_takeprofit_pip - m_trailing_step_pip;
			   m_takeprofit_pip = m_takeprofit_pip + m_trailing_step_pip;
			}
			return;
		}
		
		if (diff < m_stoploss_pip) {
		   
			this.closeAll();
			return;
		}
	}


	return;
}


double HedgeOrderManager::getAllBuyVolume(bool print) {
	double res  = 0;
	for (int i = 0; i < MAX_ORDERS; i++) {
		if (orders[i].isBuyOrder())
			res = res + orders[i].getVolume();
	}
	if (print) Print("getAllBuyVolume()=", res);
	return res;
}
double HedgeOrderManager::getAllSellVolume(bool print) {
	double res  = 0;
	for (int i = 0; i < MAX_ORDERS; i++) {
		if (orders[i].isSellOrder())
			res = res + orders[i].getVolume();
	}
	if (print) Print("getAllSellVolume()=", res);
	return res;

}
void HedgeOrderManager::setTrailingFirstOrder() {
	orders[0].setRtStopLossPip(100 - 10 * 2);
	orders[0].setRtTakeProfitPip(100);
	orders[0].setRtTrailingStepPip(10);

}
void HedgeOrderManager::trailingRtStopLossFirstOrder() {

	if (orders[0].isProfitOverRtTakeProfit()) {
		orders[0].trailingRtStopLoss();
		m_trailing_actived = true;
		Print("trailing stop loss is activated , pip = ", orders[0].profitPip());
		return;

	}
	if ( m_trailing_actived && orders[0].isProfitLowerRtStopLoss()) {
	   if(IsTesting()){
	      Print("m_max_loss_pip == ",m_max_loss_pip);
	   }
		this.closeAll();
		return;
	}

}
void HedgeOrderManager::closeAll() {
	Print("Close All Opened Order");
	for (int i = 0; i < MAX_ORDERS; i++) {
		if (!orders[i].isNoneOrder()) {
			orders[i].CloseNow();
		}
	}
	//reset();
}

void HedgeOrderManager::openNow(int op , string comment) {

	ASSERT(op == OP_BUY || op == OP_SELL , "op == OP_BUY || op == OP_SELL");

	double lot = m_start_volume;
	if (m_orderIdx == 0 ) {
		this.setTrailingFirstOrder();
	}

	double xTime = 1;
	if ( m_orderIdx > 0 && op == OP_BUY) {
		lot = (this.getAllSellVolume(true) * 2) - this.getAllBuyVolume(true);
		xTime = (this.getAllBuyVolume(false) + lot - this.getAllSellVolume(false)) / m_start_volume;
	}

	if ( m_orderIdx > 0 && op == OP_SELL) {
		lot = (this.getAllBuyVolume(true) * 2) - this.getAllSellVolume(true);
		xTime = (this.getAllSellVolume(false) + lot - this.getAllBuyVolume(false)) / m_start_volume;
	}
	Print("xTime = " ,xTime ,",Lot = ", lot , ", start_lot = ",m_start_volume);
	if( lot > 100){
		Print("Reset Lot = 100.00");
		lot = 100;
	}

	orders[m_orderIdx].setVolume(lot);

	if (op == OP_BUY) {
		orders[m_orderIdx].BuyNow(comment); // Open Buy at Ask Price
	}
	if (op == OP_SELL) {
		orders[m_orderIdx].SellNow(comment); // Open Sell  at Bid Price
	}
	if ( IsTesting()) {
		Print("FreeMargin=",AccountFreeMargin(),",UsedMargin=",AccountMargin());
		Print("Floating Profit =",this.getActualProfit(),",Equlity=",AccountEquity(),",Balance=",AccountBalance());

		if (orders[m_orderIdx].isNoneOrder()) {
			Print("Error : Can not open order");
			ExpertRemove();
		}
		
		
	}

	m_state = (op == OP_BUY ? h_upper : h_lower );

	if ( (op == OP_BUY && m_UpperPrice == 0) || ( op == OP_SELL && m_lowerPrice == 0) ) {
		//Print("SetUpper =",orders[m_orderIdx].openPrice());

		if (op == OP_BUY)
			m_UpperPrice = Ask;
		if (op == OP_SELL)
			m_lowerPrice = Bid;

		if (this.isTooDifferenceUpperLower()) {
			this.closeAll();
			//ExpertRemove();
			return;
		}
		if (op == OP_BUY)
			m_UpperPrice = orders[m_orderIdx].openPrice();
		if (op == OP_SELL)
			m_lowerPrice = orders[m_orderIdx].openPrice();
	}

	if (orders[m_orderIdx].isOpenedOrder()) m_orderIdx++;

}

void HedgeOrderManager::buyUpper(string comment) {

	Print("buyUpper()");
	this.openNow(OP_BUY, comment);

}



void HedgeOrderManager::sellLower(string comment) {

	Print("sellLower()");
	this.openNow(OP_SELL, comment);

}

void HedgeOrderManager::changeToUpperProfit() {
	Print("changeToUpperProfit()");
	m_state = h_upper_profit;
	int diff = this.diffPipOfUpper();
	m_takeprofit_pip = diff + m_trailing_step_pip;
	m_stoploss_pip = diff - m_trailing_step_pip / 2;

	//close sell order
	for (int i = 0; i < m_orderIdx ; i++) {
		if (orders[i].isSellOrder()) {
			orders[i].CloseNow();
		}
	}

}

void HedgeOrderManager::changeToLowerProfit() {
	Print("changeToLowerProfit()");
	m_state = h_lower_profit;
	int diff = this.diffPipOfLower();
	m_takeprofit_pip = diff + m_trailing_step_pip;
	m_stoploss_pip = diff - m_trailing_step_pip / 2;
	// close
	for (int i = 0; i < m_orderIdx ; i++) {
		if (orders[i].isBuyOrder()) {
			orders[i].CloseNow();
		}
	}
}



double HedgeOrderManager::getProfit() {
	double res = 0;
	for (int i = 0; i < MAX_ORDERS; i++) res = res + orders[i].getProfit();
	return res;
}
double HedgeOrderManager::getActualProfit() {

	double res = 0;
	for (int i = 0; i < MAX_ORDERS; i++) res = res + orders[i].getActualProfit();
	return res;

}

int HedgeOrderManager::diffPipOfUpper() {


	if (m_UpperPrice == 0) return 0;
	double diffprice = 0;
	diffprice = Bid - m_UpperPrice;  // close buy at Bid price ,checked passed
	int res = (int)(NormalizeDouble(diffprice, _Digits) / _Point);
	return res;

}

int HedgeOrderManager::diffPipOfLower() {

	if (m_lowerPrice == 0) return 0;
	double diffprice = 0;
	diffprice = m_lowerPrice - Ask;  // close sell at Ask Price ,checked passed
	int res = (int)(NormalizeDouble(diffprice, _Digits) / _Point);
	return res;

}

//int HedgeOrderManager::getPip4CloseBuy()

