//+------------------------------------------------------------------+
//|                                                 OrderManager.mqh |
//|                        Copyright 2016, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2016, MetaQuotes Software Corp."
#property link "https://www.mql5.com"
#property strict
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+
#include "SingleOrder.mqh"

#define MAX_ORDERS 50
#define MAX_ORDER_POS 49
#define MAX_REVERT_ORDER_POS 4  // 0 , 1 , 2, 3 , 4
#define COUNTER_REVERT_ORDER_POS 5

enum enum_trading_mode { none_trading_mode, forward_mode, revert_mode };

class OrderManager {
 public:
  OrderManager(void);
  ~OrderManager(void);

  bool hasOrder();
  void openFirstOrder();
  void monitor(bool);

  int getWin() { return m_winCount; }

  int getLose() { return m_looseCount; }

  double getMaxRevertProfit() { return m_maxRevertProfit; }
  double getMaxForwardProfit() { return m_maxForwardProfit; }
  void setMaxTradableAmount(double amount){ m_max_tradable_amount= amount;};

  void setOpBuy() { m_operation = buy; }
  void setOpSell() { m_operation = sell; }
  bool clearMarketOrder();
  int countMarketOrder();
  void setCloseOrderWhenExit(bool value ) {
   m_close_order_when_exit = value;
  };
 private:
 
  void closeAll();
  void closeRevertOrder();
  double getProfit();
  double getActualProfit();
  int countOpenedOrder();
  void reset();
  void swapOp();
  void tradeNextOrder(bool swapOp= false);
  int currentOrderProfitPip();

  void processDetectTradingMode();
  void processForwardMode();
  void processRevertMode();
  string getComment();

  SingleOrder orders[MAX_ORDERS];
  enum_order_type m_operation;
  enum_trading_mode m_trading_mode;
  int m_index;
  int m_revertRange;
  int m_forwardRange;

  double m_multiple_reverse;
  double m_multiple_forward;
  double m_start_volume;

  int m_winCount;
  int m_looseCount;
  int m_consecWin;
  double m_maxForwardProfit;
  double m_maxRevertProfit;

  // TailingStopLoss Feature
  double m_stopLossUpper;
  double m_stopLossLower;
  double m_stopLossTrailing;
  

  bool m_marketCrazy;
  
  // input
  double m_max_tradable_amount;
  bool m_close_order_when_exit;

};

OrderManager::OrderManager(void) {
  // Initial Value , no need to reset everytime;
  m_operation = buy;
  m_winCount = 0;
  m_looseCount = 0;
  m_consecWin = 0;
  m_maxForwardProfit = 0;
  m_maxRevertProfit = 0;
  m_marketCrazy = false;
  m_max_tradable_amount = 50000;
  m_index = -1;
  
}

OrderManager::~OrderManager(void) { 
   if (!m_close_order_when_exit) return;
   if (m_index > -1 ) closeAll(); 
 }

void OrderManager::reset(void) {
  // reset is always called on first order

  double tradable_balance = Account::TradableBalance();
  if (tradable_balance > m_max_tradable_amount)
   tradable_balance = m_max_tradable_amount;
   
  m_start_volume = 0;
  // wide spread protect when market is abnormal
  if (SingleOrder::HighSpread()) {
    Print("Warning: HighSpread  = ",SingleOrder::spreadPip(),", skip open new order");
    return;
  }
  if (tradable_balance < 500) {
    Print("Warning: Balance or FreeMargin < 500 USD , skip open new order");
    return;
    // ExpertRemove();
  }

  if (Account::FreeMarginLowerBalance(60)) {
    Print("Warning : FreeMargin < 60 percent of balance, skp open new order");
    return;
  }

  if (tradable_balance > 50000) tradable_balance = 50000;

  m_index = -1;
  m_revertRange = 300;
  m_forwardRange = 100;
  m_multiple_reverse = 2.5;
  m_multiple_forward = 1.5;
  m_start_volume = 0.01 * ((int)(tradable_balance / 500));
  m_trading_mode = none_trading_mode;

  Print("trading  volume=", m_start_volume);
  // m_profit = 1.25 * ((int)(balance / 500));
  // m_stopLoss = 0;

  m_stopLossTrailing = 1.25 * ((int)(tradable_balance / 500));
  m_stopLossUpper = m_stopLossTrailing;
  m_stopLossLower = 0;
}

bool OrderManager::clearMarketOrder() {

  int total = OrdersTotal();
  bool res =true;
  bool orderRes = true; ;
  for (int i = 0; i < total; i++) {
    if (OrderSelect(i, SELECT_BY_POS)) {
      
      if (OrderSymbol() == Symbol() && OrderMagicNumber() == SingleOrder::s_magic_number) {
        if (OrderType() == OP_BUY)
          orderRes = OrderClose(OrderTicket(), OrderLots(),MarketInfo(OrderSymbol(), MODE_BID), 10);

        if (OrderType() == OP_SELL)
          orderRes = OrderClose(OrderTicket(), OrderLots(),MarketInfo(OrderSymbol(), MODE_ASK), 10);
        if (OrderType() > OP_SELL) 
          orderRes = OrderDelete(OrderTicket());
        // makesure all res is true 
        res = res && orderRes;

      }


    }
  }
  return res;
}
int OrderManager::countMarketOrder(){
  int total = OrdersTotal();
  int  res =0;
  for (int i = 0; i < total; i++) {
    if (OrderSelect(i, SELECT_BY_POS)) {
      if (OrderSymbol() == Symbol()) {
         res++;
      }
    }
  }
  return res;

}
bool OrderManager::hasOrder(void) {
  bool res = false;

  for (int i = 0; i < MAX_ORDERS && res == false; i++) {
    res = res || (!orders[i].isNoneOrder());
    if (res) return res;
  }
  return res;
}

int OrderManager::countOpenedOrder(void) {
  int count = 0;
  for (int i = 0; i < MAX_ORDERS; i++)
    if (!orders[i].isNoneOrder()) count++;
  return count;
}

void OrderManager::openFirstOrder(void) {
  reset();
  tradeNextOrder();
}

void OrderManager::tradeNextOrder(bool swapOp) {
  // not open any order if start lot is too low
  if (m_start_volume < 0.01) return;

  double lastestVolume = m_index == -1 ? 0 : orders[m_index].getVolume();
  double tradeVolume  = 0;

  switch(m_trading_mode){
    case none_trading_mode:
      tradeVolume = m_start_volume;
      break;
    case revert_mode:
      tradeVolume = lastestVolume * m_multiple_reverse;
      break;
    case forward_mode:
      tradeVolume = lastestVolume * m_multiple_forward;
      break;
  }

  ASSERT(tradeVolume != 0 , "trading volume != 0 , tradeNextOrder()");

  m_index++;

  ASSERT(orders[m_index].isNoneOrder(),
         "tradeNextOrder(),order[m_index].isNoneOrder()  != true");

  orders[m_index].setVolume(tradeVolume);
  
  // Cal new op if swapop = true;
  
  enum_order_type  op = m_operation;
  if (swapOp == true && m_operation == buy )op = sell;
  if (swapOp == true && m_operation == sell )op = buy;
  if (swapOp == false )op = m_operation;
  
  
  if (op == buy )
    orders[m_index].BuyNow(getComment());
  else
    orders[m_index].SellNow(getComment());

 if(orders[m_index].isNoneOrder())
   m_index--;


}
string OrderManager::getComment() {
  string comment = "V"+software_version+":";

  switch (m_trading_mode) {
    case none_trading_mode:
      comment += "NA";
      break;
    case forward_mode:
      comment += "FW";
      break;
    case revert_mode:
      comment += "RW";
      break;
  }

  comment = comment + IntegerToString(m_index, 2, '0');

  return comment;
}

double OrderManager::getProfit() {
  double res = 0;
  for (int i = 0; i < MAX_ORDERS; i++) res = res + orders[i].getProfit();
  return res;
}
double OrderManager::getActualProfit() {
  double res = 0;
  for (int i = 0; i < MAX_ORDERS; i++) res = res + orders[i].getActualProfit();
  return res;
}

void OrderManager::closeAll(void) {
  Print("Close All Opened Order");
  for (int i = 0; i < MAX_ORDERS; i++) {
    if (!orders[i].isNoneOrder()) {
      orders[i].CloseNow();
    }
  }
  

}

void OrderManager::closeRevertOrder(void) {
  Print("OrderManager.closeRevertOrder()");
  for (int i = 0; i <= MAX_REVERT_ORDER_POS; i++) {
    if (!orders[i].isNoneOrder()) orders[i].CloseNow();
  }
}

void OrderManager::swapOp() {
  if (m_operation == buy)
    m_operation = sell;
  else
    m_operation = buy;
}

int OrderManager::currentOrderProfitPip() {
  ASSERT(m_index < MAX_ORDERS, " m_index < MAX_ORDERS");
  return orders[m_index].profitPip();
}
void OrderManager::processDetectTradingMode() {
  
  int currentProtfitPip = currentOrderProfitPip();

  if (currentProtfitPip > m_forwardRange) {
      
    if (orders[m_index].getBarPos() > 5){
    //too long to continue forward mode
      closeAll();
      return;
    }

    
    m_trading_mode = forward_mode;
    tradeNextOrder();
    return;

    
  }
  if (currentProtfitPip < -m_revertRange) {
    m_trading_mode = revert_mode;
    tradeNextOrder();
    return;
  }

  if (getActualProfit() > 0 && m_marketCrazy) {
    closeAll();
    return;
  }
}

void OrderManager::processForwardMode() {
  int currentProtfitPip = currentOrderProfitPip();
  double actualProfit = getActualProfit();

  if (currentProtfitPip > m_forwardRange) {

    if (m_index < MAX_ORDERS - 1 && orders[m_index].getBarPos() <=5)
      tradeNextOrder();
    else
      closeAll();

    return;
  }
  double max_nagetive_pip = (double)(m_forwardRange) / (1+m_multiple_forward);
  //Print("max_nagetive_pip = ",max_nagetive_pip);
 

  if (currentProtfitPip < (5-max_nagetive_pip) || actualProfit <= 0 ) {
   // No need to close when Market Crazy because forward mode already protect the profit
    closeAll();
    return;
  }
}

void OrderManager::processRevertMode() {

  if(orders[m_index].isNoneOrder()) 
      m_index--;


  if (m_index <= MAX_REVERT_ORDER_POS) {
    double actualProfit = getActualProfit();

    if (actualProfit > m_stopLossUpper) {
      m_stopLossLower = m_stopLossUpper;  //- m_stopLossTrailing;;
      m_stopLossUpper += m_stopLossTrailing;
      return;
    }

    if ((m_stopLossLower != 0 &&
         actualProfit <
             m_stopLossLower) ||  // stop normally as trailing stoploss
        (actualProfit > 0 &&
         m_marketCrazy)  // having some profit but something wrong
        ) {
      closeAll();
      return;
    }
  }

  // 0 ,1 ,2 ,3 , then continue revert mode
  if (m_index < MAX_REVERT_ORDER_POS &&
      orders[m_index].profitPip() < -m_revertRange) {
    tradeNextOrder();
    return;
  }

  // 4 , then buy 5th as revert_mode
  if (m_index == MAX_REVERT_ORDER_POS &&
      orders[m_index].profitPip() < -m_revertRange) {
    tradeNextOrder(true);
  }

  if (m_index == COUNTER_REVERT_ORDER_POS) {
    if (countOpenedOrder() == COUNTER_REVERT_ORDER_POS + 1) {
      if (getActualProfit() > 0) {
        // closeAll();
        closeRevertOrder();
        orders[m_index].setRtStopLossPip(orders[m_index].profitPip() - 50);
        orders[m_index].setRtTakeProfitPip(orders[m_index].profitPip() + 50);
        orders[m_index].setRtTrailingStepPip(50);
        return;
      }

      if (orders[m_index].profitPip() < -80 &&
          (orders[m_index - 1].profitPip() > -m_revertRange)) {
        // close counter order if possible to revertback
        orders[m_index].CloseNow();
        m_index--;
        return;
      }
    }

    if (countOpenedOrder() == 1) {
      if (orders[m_index].isProfitOverRtTakeProfit()) {
        orders[m_index].trailingRtStopLoss();
        return;
      }

      if (orders[m_index].isProfitLowerRtStopLoss()) {
        orders[m_index].CloseNow();
        return;
      }
    }

  }  // if (m_index == COUNTER_REVERT_ORDER_POS )
}

void OrderManager::monitor(bool crazy) {
  m_marketCrazy = crazy;
  if (m_trading_mode == none_trading_mode || m_index == 0) {
    // incase rw order close , and m_index -1 = 0 , then start as none_trading_mode agian
    m_trading_mode = none_trading_mode;
    processDetectTradingMode();
    return;
  }
  if (m_trading_mode == forward_mode) {
    processForwardMode();
    return;
  }
  if (m_trading_mode == revert_mode) {
    
    processRevertMode();

    return;
  }
  return;
}