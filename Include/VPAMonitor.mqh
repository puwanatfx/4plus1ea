//+------------------------------------------------------------------+
//|                                                   VPAMonitor.mqh |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+
#property copyright "Copyright 2015, MetaQuotes Software Corp."
#property link "https://www.mql5.com"
#property strict
//+------------------------------------------------------------------+
//| defines                                                          |
//+------------------------------------------------------------------+
// #define MacrosHello   "Hello, world!"
// #define MacrosYear    2010
//+------------------------------------------------------------------+
//| DLL imports                                                      |
//+------------------------------------------------------------------+
// #import "user32.dll"
//   int      SendMessageA(int hWnd,int Msg,int wParam,int lParam);
// #import "my_expert.dll"
//   int      ExpertRecalculate(int wParam,int lParam);
// #import
//+------------------------------------------------------------------+
//| EX5 imports                                                      |
//+------------------------------------------------------------------+
// #import "stdlib.ex5"
//   string ErrorDescription(int error_code);
// #import
//+------------------------------------------------------------------+
//+---------------------MONITOR
// CLASS---------------------------------------------+

#define TICK_MAX_SLOT 500
#define SIGNAL_TICK 7

enum tick_time_frame { tp_begin, tp500ms, tp1000ms, tp2000ms, tp_end };

struct pressure_st {
  int tick_total[TICK_MAX_SLOT];
  int tick_positive[TICK_MAX_SLOT];
  int tick_negative[TICK_MAX_SLOT];
  int tick_eq[TICK_MAX_SLOT];
  double avgTick[TICK_MAX_SLOT];
  int absPipChanged[TICK_MAX_SLOT];
  int diffPipChanged[TICK_MAX_SLOT];
  bool is_updated[TICK_MAX_SLOT];

};

class VPAMonitor {
private:
  double m_prevAsk;
  double m_preBid;
  int m_pip_changed;
  pressure_st m_presssure[5];

  void reset();
  // Dual Man Algo
  int m_prev_tick_total;

public:
  VPAMonitor();
  ~VPAMonitor();

  void collectOnTick();
  void processTickDataWhenTimer();
  bool singalBuy();
  bool singalSell();
  // Dual Man Algo
  bool signalPressureHighAndNoMove(bool); // signal open pending
  bool signalLast3SecHasZeroTick(); //singal delete pending
  int getPreviousTickTotal();
  double getAvgTick1M();
  int getLastABSPip() {
    return m_presssure[tp1000ms].absPipChanged[1];
  }
  int getLastTickTotal() {
    return m_presssure[tp1000ms].tick_total[1];
  }
  int getLastDiff() {
    return m_presssure[tp1000ms].diffPipChanged[1];
  }
  int getMaxTotalTick1M();
  void CommentInfo(int);
  bool hasLowerAvg50PerCenterBefore();

};

VPAMonitor::VPAMonitor(void) { reset(); }

VPAMonitor::~VPAMonitor(void) {}

void VPAMonitor::reset() {
  m_prevAsk = 0;
  m_preBid = Bid;
  m_pip_changed = 0;
  m_prev_tick_total = 0;


  for (tick_time_frame tp = tp_begin; tp < tp_end; tp++) {
    // Cleann all time frame

    for (int i = 0; i < TICK_MAX_SLOT; i++) {
      m_presssure[tp].tick_total[i] = 0;
      m_presssure[tp].tick_positive[i] = 0;
      m_presssure[tp].tick_negative[i] = 0;
      m_presssure[tp].tick_eq[i] = 0;
      m_presssure[tp].is_updated[i] = false;
      m_presssure[tp].absPipChanged[i] = 0;
      m_presssure[tp].diffPipChanged[i] = 0;

    }
  }
}

bool VPAMonitor::singalBuy(void) {

  int tp_type = tp1000ms;
  int pos_tick = 0;
  for (int i = 0; i <= 3; i++)
    pos_tick = pos_tick + m_presssure[tp_type].tick_positive[i];
  int nag_tick = 0;
  for (int i = 0; i <= 3; i++)
    nag_tick = nag_tick + m_presssure[tp_type].tick_negative[i];

  double ma = iMA(Symbol(), PERIOD_M1, 14, 0, MODE_EMA, PRICE_CLOSE, 0);
  double ma1 = iMA(Symbol(), PERIOD_M1, 14, 0, MODE_EMA, PRICE_CLOSE, 1);
  double ma2 = iMA(Symbol(), PERIOD_M1, 14, 0, MODE_EMA, PRICE_CLOSE, 2);

  bool buyslope = ma - ma1 > 0 && ma1 - ma2 > 0;

  return ((pos_tick - nag_tick) >= SIGNAL_TICK && buyslope);
}

bool VPAMonitor::singalSell(void) {
  int tp_type = tp1000ms;
  int pos_tick = 0;
  for (int i = 0; i <= 3; i++)
    pos_tick = pos_tick + m_presssure[tp_type].tick_positive[i];
  int nag_tick = 0;
  for (int i = 0; i <= 3; i++)
    nag_tick = nag_tick + m_presssure[tp_type].tick_negative[i];

  double ma = iMA(Symbol(), PERIOD_M1, 14, 0, MODE_EMA, PRICE_CLOSE, 0);
  double ma1 = iMA(Symbol(), PERIOD_M1, 14, 0, MODE_EMA, PRICE_CLOSE, 1);
  double ma2 = iMA(Symbol(), PERIOD_M1, 14, 0, MODE_EMA, PRICE_CLOSE, 2);

  bool sellslope = ma - ma1 < 0 && ma1 - ma2 < 0;
  return ((nag_tick - pos_tick) >= SIGNAL_TICK && sellslope);
}
void VPAMonitor::processTickDataWhenTimer() {
  // execute on every 500 ms

  // 500 ms
  int tp_type = tp1000ms;

  m_presssure[tp_type].avgTick[0] = this.getAvgTick1M();

  for (int i = TICK_MAX_SLOT - 1; i > 0; i--) {

    m_presssure[tp_type].tick_total[i] = m_presssure[tp_type].tick_total[i - 1];
    m_presssure[tp_type].tick_positive[i] = m_presssure[tp_type].tick_positive[i - 1];
    m_presssure[tp_type].tick_negative[i] = m_presssure[tp_type].tick_negative[i - 1];
    m_presssure[tp_type].tick_eq[i] = m_presssure[tp_type].tick_eq[i - 1];
    m_presssure[tp_type].avgTick[i] = m_presssure[tp_type].avgTick[i - 1];
    m_presssure[tp_type].is_updated[i] = m_presssure[tp_type].is_updated[i - 1];
    m_presssure[tp_type].absPipChanged[i] = m_presssure[tp_type].absPipChanged[i - 1];
    m_presssure[tp_type].diffPipChanged[i] = m_presssure[tp_type].diffPipChanged[i - 1];

  }

  m_presssure[tp_type].tick_total[0] = 0;
  m_presssure[tp_type].tick_positive[0] = 0;
  m_presssure[tp_type].tick_negative[0] = 0;
  m_presssure[tp_type].tick_eq[0] = 0;
  m_presssure[tp_type].avgTick[0] = 0;
  m_presssure[tp_type].absPipChanged[0] = 0;
  m_presssure[tp_type].diffPipChanged[0] = 0;

  if (m_presssure[tp_type].tick_total[1] > 0 ||
      m_presssure[tp_type].tick_positive[1] > 0 ||
      m_presssure[tp_type].tick_negative[1] > 0) {

    // Print(" Pressure 1000ms  total/eq/positive/nagative[",m_presssure[tp_type].tick_total[1],"/",m_presssure[tp_type].tick_eq[1],"/",m_presssure[tp_type].tick_positive[1],"/",-m_presssure[tp_type].tick_negative[1],"]");
  }


}
void VPAMonitor::collectOnTick() {

  //if(IsTesting()) return;


  m_presssure[tp1000ms].is_updated[0] = true;
  m_presssure[tp1000ms].tick_total[0]++;

  if (Bid > m_preBid) m_presssure[tp1000ms].tick_positive[0]++;
  if (Bid < m_preBid) m_presssure[tp1000ms].tick_negative[0]++;
  if (Bid == m_preBid) m_presssure[tp1000ms].tick_eq[0]++;
  int pip_changed = (int)(NormalizeDouble((Bid - m_preBid), _Digits) / _Point);
  m_presssure[tp1000ms].absPipChanged[0] = m_presssure[tp1000ms].absPipChanged[0] + MathAbs(pip_changed);
  m_presssure[tp1000ms].diffPipChanged[0] = m_presssure[tp1000ms].diffPipChanged[0] + pip_changed;

  m_preBid = Bid;
  m_prevAsk = Ask;



}

double VPAMonitor::getAvgTick1M() {

  int sumTick = 0;
  int countSec = 0;
  for (int i = 1; i <= 31 ; i++) {
    // Not count on pos = 0;

    if (m_presssure[tp1000ms].is_updated[i]) {
      sumTick = sumTick + m_presssure[tp1000ms].tick_total[i];
      countSec = countSec + 1;

    }

  }

  if (countSec == 0 ) return 0;
  double avg = ((double)sumTick) / countSec;
  //avg = NormalizeDouble(avg,4);
  return avg;

}

bool VPAMonitor::hasLowerAvg50PerCenterBefore() {
  for (int i = 2; i <= 31 ; i++) {

    if (m_presssure[tp1000ms].is_updated[i]) {
      if (m_presssure[tp1000ms].avgTick[1] >= (m_presssure[tp1000ms].avgTick[i] * 2))
        return true;

    }
  }
  return false;

}
int VPAMonitor::getMaxTotalTick1M() {
  int max_tick_total = 0;
  for (int i = 1 ; i <= 61 ; i ++) {
    if (m_presssure[tp1000ms].is_updated[i]) {
      max_tick_total = MathMax(m_presssure[tp1000ms].tick_total[i], max_tick_total);
    }
  }
  return max_tick_total;

}

bool VPAMonitor::signalPressureHighAndNoMove(bool showMsg) {
  bool zeroDiffPip = (m_presssure[tp1000ms].diffPipChanged[1] == 0);
  bool absMoreThenTick = ( m_presssure[tp1000ms].absPipChanged[1] > m_presssure[tp1000ms].tick_total[1]);
  bool tickCountHigh = m_presssure[tp1000ms].tick_total[1] > getAvgTick1M();
  if (tickCountHigh)
    m_prev_tick_total = m_presssure[tp1000ms].tick_total[1];
  //bool diffPipHigh = ( m_presssure[tp1000ms].diffPipChanged[0] > getAvgTick1M());
  if (zeroDiffPip && tickCountHigh && showMsg)
    Print("AVG TICK =", getAvgTick1M(), ",DIFPIP=", m_presssure[tp1000ms].diffPipChanged[1], ",TICKS=", m_presssure[tp1000ms].tick_total[1]);
  return zeroDiffPip && tickCountHigh && absMoreThenTick;
}

bool VPAMonitor::signalLast3SecHasZeroTick(void) {
  bool res = (m_presssure[tp1000ms].tick_total[1] + m_presssure[tp1000ms].tick_total[2] + m_presssure[tp1000ms].tick_total[3]) == 0;
  return res;
}
int VPAMonitor::getPreviousTickTotal() {
  return m_prev_tick_total;
}
void VPAMonitor::CommentInfo(int magicNumber) {



  int max_tick_total =  MathMax(m_presssure[tp1000ms].tick_total[0], getMaxTotalTick1M());
  string sumInfo = "Magic=" + IntegerToString(magicNumber) + " / AVG  ===" + DoubleToString(getAvgTick1M()) + "\nTickse =" + IntegerToString(m_presssure[tp1000ms].tick_total[0]) + "\nMax = " + IntegerToString(max_tick_total) + "\n";
  string sumInfo2  = "Pip Change = " + IntegerToString(m_presssure[tp1000ms].diffPipChanged[0]) + "\n" + "Spread = " + IntegerToString(SingleOrder::spreadPip()) + "\n";
  string tickInfo ;
  string absPipInfo;
  string diffPipInfo;

  for (int i = 1 ; i < 61 ; i ++) {
    if (m_presssure[tp1000ms].is_updated[i]) {

      int iTotal = m_presssure[tp1000ms].tick_total[i];
      int iAbsPip = m_presssure[tp1000ms].absPipChanged[i];
      int iDiffPip = m_presssure[tp1000ms].diffPipChanged[i];
      string delim = " ";

      if (iTotal > getAvgTick1M() && iAbsPip > getAvgTick1M()  && iDiffPip == 0)
        delim = "<< ";
      string total = IntegerToString(iTotal);
      tickInfo = tickInfo + pad(total, 3, "0") + delim;

      string pip = IntegerToString(iAbsPip);
      absPipInfo = absPipInfo + pad(pip, 3, "0") + delim;

      string dfPip = IntegerToString(MathAbs(iDiffPip));
      diffPipInfo = diffPipInfo + pad(dfPip, 3, "0") + delim;


    }
  }


  Comment(sumInfo + sumInfo2 + "PIPAB=" + absPipInfo + "\n" + "PIPDF=" + diffPipInfo + "\n" + "TICKS=" + tickInfo);



}