/*
26-NOV-2016 - first version

*/

//+------------------------------------------------------------------+..
//|                                             DenAutoFXTrading.mq4 |
//|                        Copyright 2015, MetaQuotes Software Corp. |
//|                                             https://www.mql5.com |
//+------------------------------------------------------------------+


/*
Version History

1.11 - add Hedging  USD 
*/
#include <stdlib.mqh>
#define software_version "1.00"

#property copyright "Copyright 2016, Grails EA, Agility FX Trading."
#property link "http://www.4plus1ea.com"
#property version software_version
#property strict

#include ".\Include\Account.mqh"
#include ".\Include\WebAPI.mqh"

WebAPI api;

//+------------------------------------------------------------------+
//| Expert initialization function                                   |
//+------------------------------------------------------------------+

int OnInit() {
   
   
  

  ChartSetInteger(0, CHART_COLOR_CHART_UP, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CHART_DOWN, 0, 255);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BULL, 0, 16711680);
  ChartSetInteger(0, CHART_COLOR_CANDLE_BEAR, 0, 255);
  ChartSetInteger(0, CHART_SHOW_VOLUMES, 0, 1);
  ChartSetInteger(0, CHART_MODE, 0, 1);
  ChartSetInteger(0, CHART_SCALE, 0);
  ChartSetSymbolPeriod(0,NULL, PERIOD_M1);
  EventSetTimer(1);

  

  return (INIT_SUCCEEDED);
}
//+------------------------------------------------------------------+
//| Expert deinitialization function                                 |
//+------------------------------------------------------------------+
void OnDeinit(const int reason) {
  //--- destroy timer
  EventKillTimer();
}
//+------------------------------------------------------------------+
//| Expert tick function                                             |
//+------------------------------------------------------------------+


enum serverState { na,suceeded,failed };
serverState state = na;
serverState preState =na;

string formatPriceString(){
   return "%."+IntegerToString(Digits)+"f";
}
string fmt = formatPriceString();
void OnTick() {
  
   string params ="?";
   params += "symbol="+Symbol();
   params += "&bid="+StringFormat(fmt,Bid);
   params += "&ask="+StringFormat(fmt,Ask);
   //Print("Params=",params);
  
   string fullURL ="http://localhost/EA/push"+params;
   int res = api.get(fullURL);
   preState = state;
   if(res != 200){
      state = failed;
   }else{
      state = suceeded;
   }
   if(state != preState ){
      Print("send data : " + (state == suceeded ? "suceeded":"failed"));
   }
   
   
   //Print("Success :",fullURL);

}

//+------------------------------------------------------------------+
//| Timer function                                                   |
//+------------------------------------------------------------------+
void OnTimer() {

  

 
}
//+------------------------------------------------------------------+
//| Tester function                                                  |
//+------------------------------------------------------------------+
double OnTester() {
  //---
  double ret = 0.0;
  //---

  /*---
  Print("WinCount=", orderman.getWin());
  Print("LossCount=", orderman.getLose());
  Print("StartDate = ", startdate);
  Print("EndDate =", Time[0]);
  Print("MAX Profit (ForwardMode)=", orderman.getMaxForwardProfit());
  Print("MAX Profit (RevertMode)=", orderman.getMaxRevertProfit());
  */
  return (ret);
}
//+------------------------------------------------------------------+
//| ChartEvent function                                              |
//+------------------------------------------------------------------+
void OnChartEvent(const int id, const long& lparam, const double& dparam,
                  const string& sparam) {
  //---
}
//+------------------------------------------------------------------+
